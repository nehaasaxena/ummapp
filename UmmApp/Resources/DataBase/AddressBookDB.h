//
//  AddressBookDB.h
//  SocialParty
//
//  Created by pankaj on 06/09/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface AddressBookDB : NSManagedObject

@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * fullName;
@property (nonatomic, retain) NSString * jobTitle;
@property (nonatomic, retain) NSDate * modifiedDate;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * strModifiedDate;
@property (nonatomic, retain) NSNumber * uniqueContactID;
@property (nonatomic, retain) NSNumber * userId;
@property (nonatomic, retain) NSNumber * originalUserId;
@property (nonatomic, retain) NSString * orgAppUsername;
@property (nonatomic, retain) NSString * orgChatUsername;
@property (nonatomic, retain) NSString * profilePic;
@property (nonatomic, retain) NSString * userStatus;
@property (nonatomic, retain) NSNumber * isFriend;
@end
