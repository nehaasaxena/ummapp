//
//  Database.h
//  SocialParty
//
//  Created by chetan shishodia on 14/01/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressBookDB.h"
@interface Database : NSObject

+ (Database *)database;


-(NSArray *)fetchDataFromDatabaseForEntity:(NSString *)entityName;

#pragma mark - AddressBook
-(void)SaveAddressBookDataBase:(NSArray*)array from:(BOOL)updatedArray;
-(NSArray *)fetchDataFromAddressBookDBOfType:(AddressBookContactType)addressBookContactType;
-(void)DeleteAddressBookFromDatabase:(NSString*)idsToBeDelete;
-(AddressBookDB *)fetchDataFromDatabaseForEntity:(NSString *)entityName chatUserName:(NSString*)chatUserName keyName:(NSString *)keyName;
-(NSMutableArray *)fetchDataFromDatabaseForEntityKeyName:(NSString *)keyName;
-(void)SaveAddressBookDataBaseBeforeServerUpdate:(NSDictionary*)aDict;
@end
