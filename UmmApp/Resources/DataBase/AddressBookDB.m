//
//  AddressBookDB.m
//  SocialParty
//
//  Created by pankaj on 06/09/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "AddressBookDB.h"


@implementation AddressBookDB

@dynamic email;
@dynamic fullName;
@dynamic jobTitle;
@dynamic modifiedDate;
@dynamic notes;
@dynamic phone;
@dynamic strModifiedDate;
@dynamic uniqueContactID;
@dynamic userId;
@dynamic originalUserId;
@dynamic orgAppUsername;
@dynamic orgChatUsername;
@dynamic profilePic;
@dynamic userStatus;
@dynamic isFriend;
@end
