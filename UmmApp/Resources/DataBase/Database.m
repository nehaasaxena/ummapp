//
//  Database.m
//  SocialParty
//
//  Created by chetan shishodia on 14/01/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//
#import "Database.h"


#import "NSString+SH.h"


#import "AddressBookDB.h"

static Database *_sharedInstance;

@interface Database (PrivateMethods)
- (void)loadMetaData;

@end


@implementation Database

+ (Database *)database {
	@synchronized([Database class]) {
		if (!_sharedInstance)
			_sharedInstance = [[self alloc] init];
        
		return _sharedInstance;
	}
	return nil;
}

+ (id)alloc {
	@synchronized([Database class]) {
		NSAssert(_sharedInstance == nil, @"Attempted to allocate a second instance of a singleton.");
		_sharedInstance = [super alloc];
		return _sharedInstance;
	}
	return nil;
}

- (id)init {
	if (self = [super init]) {
        //		[self setManagedObjectContext:[AppDelegate managedObjectContext]];
        //        [self.managedObjectContext setRetainsRegisteredObjects:YES];
        //        [self loadMetaData];
	}
	return self;
}



#pragma mark - AddressBook
-(void)DeleteAddressBookFromDatabase:(NSString*)idsToBeDelete{
    if (!idsToBeDelete) {
        return;
    }
    NSManagedObjectContext *context;
    if (!context) {
        context = [APP_DELEGATE managedObjectContext];
    }
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"AddressBookDB" inManagedObjectContext:context];
    
    
    // Fetch the records and handle an error
	NSError *error;
    
    NSArray *array=[idsToBeDelete componentsSeparatedByString:@","];
    for (id obj in array) {
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entity];
        
        
//        NSInteger userId=[[[NSUserDefaults standardUserDefaults] valueForKey:kUserId] integerValue];
        //     NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"(userId == %d)",userId];
        NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"(uniqueContactID == %d)",[obj integerValue]];
        NSPredicate *finalPred = [NSCompoundPredicate andPredicateWithSubpredicates: @[pred2]];
        
        [request setPredicate:finalPred];
        
        
        NSMutableArray *mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
        
        for (NSManagedObject *managedObject in mutableFetchResults) {
            [context deleteObject:managedObject];
        }
        if (![context save:&error])
        {
            abort();
        }
        
        
    }
}
-(void)SaveAddressBookDataBaseBeforeServerUpdate:(NSDictionary*)aDict{
    
    NSManagedObjectContext *context;
    if (!context) {
        context = [APP_DELEGATE managedObjectContext];
    }
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"AddressBookDB" inManagedObjectContext:context];
    
    
    // Fetch the records and handle an error
    NSError *error;
    
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entity];
        
        NSInteger uniqueContactID=[[aDict valueForKey:@"uniqueContactID"] integerValue];
        NSInteger userId=[[[NSUserDefaults standardUserDefaults] valueForKey:kUserId] integerValue];
        NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"(uniqueContactID == %d)",uniqueContactID];
        NSPredicate *finalPred  = nil;
    
            finalPred =[NSCompoundPredicate orPredicateWithSubpredicates:@[pred2]];
//        }
    
        [request setPredicate:finalPred];
        
        
        NSMutableArray *mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
        
        AddressBookDB *addressBook = nil;
        
        if ([mutableFetchResults count]>0) {
            addressBook = (AddressBookDB*)[mutableFetchResults objectAtIndex:0];
        }else
            addressBook = (AddressBookDB *)[NSEntityDescription insertNewObjectForEntityForName:@"AddressBookDB" inManagedObjectContext:context];
        
        addressBook.userId =[NSNumber numberWithInteger:userId];
        
        if ([[aDict valueForKey:@"phones"] count]>0) {
            NSData *jsonDataPhone = [NSJSONSerialization dataWithJSONObject:[aDict valueForKey:@"phones"] options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonPhone = [[NSString alloc] initWithData:jsonDataPhone encoding:NSUTF8StringEncoding];
            addressBook.phone = jsonPhone;
            
        }
        
        if ([[aDict valueForKey:@"emails"] count]>0) {
            NSData *jsonDataEmail = [NSJSONSerialization dataWithJSONObject:[aDict valueForKey:@"emails"] options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonEmail = [[NSString alloc] initWithData:jsonDataEmail encoding:NSUTF8StringEncoding];
            addressBook.email = jsonEmail;
        }
        
        if([aDict valueForKey:@"uniqueContactID"])
        {
            addressBook.uniqueContactID =[NSNumber numberWithInteger: [[aDict valueForKey:@"uniqueContactID"] integerValue]];
        }
        if([[aDict valueForKey:@"name"] length]>0)
        {
            addressBook.fullName=[aDict valueForKey:@"name"];
        }
        if([aDict valueForKey:@"strModifiedDate"])
        {
            addressBook.modifiedDate = [AppManager DateFromString:[aDict valueForKey:@"strModifiedDate"]];
            addressBook.strModifiedDate =[aDict valueForKey:@"strModifiedDate"];
        }
        if([aDict valueForKey:kNotes])
        {
            addressBook.notes=[aDict valueForKey:kNotes];
        }
        if([aDict valueForKey:kJobTitle])
        {
            addressBook.jobTitle=[aDict valueForKey:kJobTitle];
        }
        if([aDict valueForKey:@"orgUserId"])
        {
            addressBook.originalUserId=[NSNumber numberWithInteger:0];
        }
        if([aDict valueForKey:@"orgAppUsername"])
        {
            addressBook.orgAppUsername=@"";
        }
        if([aDict valueForKey:@"orgChatUsername"])
        {
            addressBook.orgChatUsername=@"";
        }
        if([aDict valueForKey:@"profilePic"])
        {
            addressBook.profilePic=@"";
        }
        if([aDict valueForKey:kUserStatus])
        {
            addressBook.userStatus=[aDict valueForKey:kUserStatus];
        }
        //   addressBook.isFriend=[obj valueForKey:@"isFriend"];
        
        if (![context save:&error])
        {
            NSLog(@"Error : %@",error);
            abort();
        }
        
    
    
    
}

-(void)SaveAddressBookDataBase:(NSArray*)array from:(BOOL)updatedArray{
    
    NSManagedObjectContext *context;
    if (!context) {
        context = [APP_DELEGATE managedObjectContext];
    }
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"AddressBookDB" inManagedObjectContext:context];
    
    
  // Fetch the records and handle an error
	NSError *error;
    NSString *strBaseImageUrl=[[NSUserDefaults standardUserDefaults] valueForKey:kServerUrl];
    if(!updatedArray)
    {
        for (id obj in array) {
            NSFetchRequest *request = [[NSFetchRequest alloc] init];
            [request setEntity:entity];

            NSInteger userId=[[[NSUserDefaults standardUserDefaults] valueForKey:kUserId] integerValue];
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"(uniqueContactID == %d)",[[obj valueForKey:@"uniqueContactID"] integerValue]];
            NSPredicate *finalPred  = nil;
            finalPred =[NSCompoundPredicate orPredicateWithSubpredicates:@[pred]];
           [request setPredicate:finalPred];
            
            
            NSMutableArray *mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
            
            AddressBookDB *addressBook = nil;
            
            if ([mutableFetchResults count]>0) {
                addressBook = (AddressBookDB*)[mutableFetchResults objectAtIndex:0];
            }else
            addressBook = (AddressBookDB *)[NSEntityDescription insertNewObjectForEntityForName:@"AddressBookDB" inManagedObjectContext:context];
            
            addressBook.userId =[NSNumber numberWithInteger:userId];
           
            if ([[obj valueForKey:@"phone"] count]>0) {
                NSData *jsonDataEmail = [NSJSONSerialization dataWithJSONObject:[obj valueForKey:@"phone"] options:NSJSONWritingPrettyPrinted error:&error];
                NSString *jsonEmail = [[NSString alloc] initWithData:jsonDataEmail encoding:NSUTF8StringEncoding];
                addressBook.phone = jsonEmail;
            }
            if ([[obj valueForKey:@"email"] count]>0) {
                NSData *jsonDataEmail = [NSJSONSerialization dataWithJSONObject:[obj valueForKey:@"email"] options:NSJSONWritingPrettyPrinted error:&error];
                NSString *jsonEmail = [[NSString alloc] initWithData:jsonDataEmail encoding:NSUTF8StringEncoding];
                addressBook.email = jsonEmail;
            }
            if([obj valueForKey:@"uniqueContactID"])
            {
                addressBook.uniqueContactID =[NSNumber numberWithInteger: [[obj valueForKey:@"uniqueContactID"] integerValue]];
            }
            if([[obj valueForKey:@"name"] length]>0)
            {
                addressBook.fullName=[obj valueForKey:@"name"];
            }
            if([obj valueForKey:@"strModifiedDate"])
            {
                addressBook.modifiedDate = [AppManager DateFromString:[obj valueForKey:@"strModifiedDate"]];
                addressBook.strModifiedDate =[obj valueForKey:@"strModifiedDate"];
            }
            if([obj valueForKey:kNotes])
            {
                addressBook.notes=[obj valueForKey:kNotes];
            }
            if([obj valueForKey:kJobTitle])
            {
                addressBook.jobTitle=[obj valueForKey:kJobTitle];
            }
            if([addressBook.isFriend boolValue]==YES)
            {
                addressBook.isFriend=[NSNumber numberWithBool:YES];
            }
            else
            {
                addressBook.isFriend=[NSNumber numberWithBool:YES];
            }
        }
    }
    else
    {
        for(id obj in array)
        {
            NSFetchRequest *request = [[NSFetchRequest alloc] init];
            [request setEntity:entity];
            
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"phone CONTAINS[cd] %@",[obj valueForKey:@"chatUserName"]];
            NSPredicate *finalPred  = nil;
            finalPred =[NSCompoundPredicate orPredicateWithSubpredicates:@[pred]];
            [request setPredicate:finalPred];
            
            NSMutableArray *mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
            
            AddressBookDB *addressBook = nil;
            
            if ([mutableFetchResults count]>0) {
                addressBook = (AddressBookDB*)[mutableFetchResults objectAtIndex:0];
                addressBook.phone = addressBook.phone;
                addressBook.email = addressBook.email;
                addressBook.fullName = addressBook.fullName;
                addressBook.modifiedDate = addressBook.modifiedDate;
                addressBook.userId = addressBook.userId;
                addressBook.uniqueContactID = addressBook.uniqueContactID;
                addressBook.isFriend = [NSNumber numberWithBool:YES];
                addressBook.jobTitle = addressBook.jobTitle;
                addressBook.notes = addressBook.notes;
                addressBook.strModifiedDate = addressBook.strModifiedDate;
                if([obj valueForKey:@"appUserId"])
                {
                    addressBook.originalUserId=[NSNumber numberWithInteger:[[obj valueForKey:@"appUserId"] integerValue]];
                }
                if([obj valueForKey:@"appUsername"])
                {
                    addressBook.orgAppUsername=[obj valueForKey:@"appUsername"];
                }
                if([obj valueForKey:@"chatUserName"])
                {
                    addressBook.orgChatUsername=[obj valueForKey:@"chatUserName"];
                }
                if([obj valueForKey:@"profilePic"])
                {
                    addressBook.profilePic=[strBaseImageUrl stringByAppendingString:[obj valueForKey:@"profilePic"]];
                }
                if([obj valueForKey:kUserStatus])
                {
                    addressBook.userStatus=[obj valueForKey:kUserStatus];
                }
            }
        }
    }
    if (![context save:&error])
    {
        NSLog(@"Error : %@",error);
        abort();
    }
}
-(NSArray *)fetchDataFromAddressBookDBOfType:(AddressBookContactType)addressBookContactType{
    NSManagedObjectContext *context;
    if (!context) {
        context = [APP_DELEGATE managedObjectContext];
    }
    NSEntityDescription *entityDescription = [NSEntityDescription
                                              entityForName:@"AddressBookDB" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    
    NSPredicate *pred2;
    switch (addressBookContactType) {
        case ADDRESSBOOK_CONTACT_REGISTERED_ON_UMMAPP:
            pred2 = [NSPredicate predicateWithFormat:@"(originalUserId > %d)",0];
            break;
        case ADDRESSBOOK_CONTACT_NOT_REGISTERED_ON_UMMAPP:
            pred2 = [NSPredicate predicateWithFormat:@"(originalUserId == %d)",0];
            break;
        default:
            break;
    }
    //     NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"(userId == %d)",userId];
    NSPredicate *finalPred = [NSCompoundPredicate andPredicateWithSubpredicates: @[pred2]];
    
    [request setPredicate:finalPred];
    
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:request error:&error];
    
    if (array == nil)
    {
        // Deal with error...
    }
    
    return array;
    
}



-(NSArray *)fetchDataFromDatabaseForEntity:(NSString *)entityName{
    NSManagedObjectContext *context;
    if (!context) {
        context = [APP_DELEGATE managedObjectContext];
    }
    NSEntityDescription *entityDescription = [NSEntityDescription
                                              entityForName:entityName inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    
    NSError *error;
    NSArray *array = [context executeFetchRequest:request error:&error];
    
    if (array == nil)
    {
        // Deal with error...
    }
    
    return array;
    
}
-(AddressBookDB *)fetchDataFromDatabaseForEntity:(NSString *)entityName chatUserName:(NSString*)chatUserName keyName:(NSString *)keyName{
    NSManagedObjectContext *context;
    if (!context) {
        context = [APP_DELEGATE managedObjectContext];
    }
    NSEntityDescription *entityDescription = [NSEntityDescription
                                              entityForName:@"AddressBookDB" inManagedObjectContext:context];
    
    NSPredicate *predicate = nil;
    if(keyName!=nil)
    {
        predicate = [NSPredicate predicateWithFormat:@"%@ == nil ",keyName];
    }
    else
    {
        predicate = [NSPredicate predicateWithFormat:@"orgChatUsername == %@",chatUserName];
    }
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *array = [context executeFetchRequest:request error:&error];
    
    if ([array count]==0)
    {
        return nil;
    }
    
    return [array objectAtIndex:0];
    
}
-(NSMutableArray *)fetchDataFromDatabaseForEntityKeyName:(NSString *)keyName{
    NSManagedObjectContext *context;
    if (!context) {
        context = [APP_DELEGATE managedObjectContext];
    }
    NSEntityDescription *entityDescription = [NSEntityDescription
                                              entityForName:@"AddressBookDB" inManagedObjectContext:context];
    
    NSPredicate *predicate = nil;
    if([keyName isEqualToString:@"email"])
    {
        predicate = [NSPredicate predicateWithFormat:@"!(email == nil) "];
    }
    else
    {
        predicate = [NSPredicate predicateWithFormat:@"!(phone == nil)"];
    }
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *array = [context executeFetchRequest:request error:&error];
    
    if ([array count]==0)
    {
        return nil;
    }
    
    return array;
    
}
@end
