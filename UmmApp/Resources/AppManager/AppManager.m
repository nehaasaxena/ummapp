
//
//  AppManager.m
//  SocialParty
//
//  Created by Pankaj on 1/3/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "AppManager.h"
#define kMax_No_Of_contacts 300


AppManager * gAppManager = nil;
UIAlertView *alert = nil;
@implementation AppManager
@synthesize notifyDict;
+(AppManager *)sharedManager
{
    static AppManager *instance = nil;
    if(instance == nil)
    {
        instance = [[AppManager alloc] init];
       
        gAppManager = instance;
        [gAppManager initializeObjects];
    }
    return instance;
}

- (id)init
{
    if ((self = [super init]))
    {
        //sharedInstance = self;
        if (!addressBookRef) {
            addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
            //        people = nil;
            //   [self refresh];
            ABAddressBookRegisterExternalChangeCallback (addressBookRef,
                                                         MyAddressBookExternalChangeCallback,
                                                         (__bridge void *)(self)
                                                         );
        }
        
    }
    return self;
}
-(void)initializeObjects
{
}
+(CLocation *)getLocationByLocationStr:(NSString *)inLocationStr
{
    CLocation *aLocation = [[CLocation alloc] init];
    
    NSArray *aLocationComponents = [inLocationStr componentsSeparatedByString:@","];
    
    CLLocationCoordinate2D  aCoordinate ;
    
    aCoordinate.latitude = [[aLocationComponents objectAtIndex:0] doubleValue];
    aCoordinate.longitude = [[aLocationComponents objectAtIndex:1] doubleValue];
    aLocation.Coordinates = aCoordinate;
    aLocation.LocationName = [NSString stringWithFormat:@"%@",[aLocationComponents objectAtIndex:2]];
    return aLocation;
}

+(void)saveUserDatainUserDefault
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kIsLoggedIn];
    [[AppManager sharedManager]createDefaultAppSettings];
}
- (void)createDefaultAppSettings
{
    [[AppManager sharedManager].dicAppSettings removeAllObjects];
    [AppManager sharedManager].dicAppSettings = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary *dictChatSettings = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"Default",kCHAT_SETTINGS_CHAT_WALLPAPER,@"1",kCHAT_SETTINGS_AUTODOWNLOAD,@"1",kCHAT_SETTINGS_SAVE_INCOMING_MEDIA, nil];
    
    NSMutableDictionary *dictNotifications = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"1",kALERT,@"None",kSOUNDNAME, @"1",kSHOW_PREVIEW,nil];
    
    [[AppManager sharedManager].dicAppSettings setObject:dictChatSettings forKey:kCHAT_SETTINGS];
    [[AppManager sharedManager].dicAppSettings setObject:dictNotifications forKey:kMESSAGE_NOTIFICATION];
    [[NSUserDefaults standardUserDefaults]setObject:[AppManager sharedManager].dicAppSettings forKey:kAPP_SETTINGS];
}
+(void)saveDataToNSUserDefaults:(NSDictionary*)responseDic
{
    [[NSUserDefaults standardUserDefaults] setValue:[responseDic objectForKey:kUserId] forKey:kUserId];
    [[NSUserDefaults standardUserDefaults] setValue:[responseDic objectForKey:kSessionToken] forKey:kSessionToken];

    if([[responseDic objectForKey:kFullName]length]>0)
    {
        [[NSUserDefaults standardUserDefaults] setValue:[responseDic objectForKey:kFullName] forKey:kFullName];
    }
    if([[responseDic objectForKey:kchatUserName]length]>0)
    {
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@@%@",[responseDic objectForKey:kchatUserName],STRChatServerURL] forKey:kXMPPmyJID1];
        [[NSUserDefaults standardUserDefaults] setObject:@"123456" forKey:kXMPPmyPassword1];
        [[NSUserDefaults standardUserDefaults] setValue:[responseDic objectForKey:kchatUserName] forKey:kchatUserName];
    }

    [[NSUserDefaults standardUserDefaults] setValue:[responseDic objectForKey:kUserState] forKey:kUserState];
    [[NSUserDefaults standardUserDefaults] setValue:[responseDic objectForKey:kShowVerification] forKey:kShowVerification];
    [[NSUserDefaults standardUserDefaults] setValue:[responseDic objectForKey:kServerUrl] forKey:kServerUrl];
    if([[responseDic objectForKey:kProfileImage]length]>0)
    {
        UIImageView *imgView = [[UIImageView alloc] init];
        [imgView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] valueForKey:kServerUrl] ,[responseDic objectForKey:kProfileImage]]] placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
            //
        }];
        [[NSUserDefaults standardUserDefaults] setValue:[responseDic objectForKey:kProfileImage] forKey:kProfileImage];
    }

    [[NSUserDefaults standardUserDefaults] synchronize];

}
+(NSArray*)getCountryCodeList{
    // Read plist from bundle and get Root Dictionary out of it
    NSArray *arrRoot = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"CountryCodeList" ofType:@"plist"]];
    
    return arrRoot;
}

#pragma mark - NetWork Indicator on status bar
+(void)startStatusbarActivityIndicatorWithUserInterfaceInteractionEnabled:(BOOL)status{
    
    if (!status) {
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    }
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}


+(void)stopStatusbarActivityIndicator{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}
#pragma mark - Authentication Error Alert
+(void)removeDataFromNSUserDefaults{
    
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        
        NSString *strKey = (NSString *)key;
        
        if (![strKey isEqualToString:kDeviceToken]) {
            [defs removeObjectForKey:key];
        }else{
            NSLog(@"HEHEHE");
        }
        
        
    }
    [defs synchronize];
}
-(void)showAuthenticationFailedAlertView{
    [AppManager removeDataFromNSUserDefaults];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kAPP_SETTINGS];
    [APP_DELEGATE removeTabbarView];

    
    if (!alert) {
        alert = [[UIAlertView alloc] initWithTitle:kAlertTitle message:NSLocalizedString(@"Authentication failed!", @"Authentication failed!")  delegate:self
                                 cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles: nil];
    }
    
    
    if (!alert.isVisible) {
        [alert show];
    }
    
    
}

#pragma mark - AddressBook
-(void)fetchAddressBookWithContactModel
{
  
    
    if (!addressBookRef) {
        addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    }
    if(!addressBookRef)
        return ;
    ABAddressBookRevert(addressBookRef);
    //     ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            
            if (granted) {
                  [AppManager sharedManager].isFetchingContacts=YES;
                [[AppManager sharedManager] PerformTaskAfterContactsPermissionGranted];
            }
        });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
          [AppManager sharedManager].isFetchingContacts=YES;
        [[AppManager sharedManager] PerformTaskAfterContactsPermissionGranted];
    }
    else {
        // Send an alert telling user to change privacy setting in settings app
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kContactsAccessPermission];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // the user has previously denied access - send alert to user to allow access in Settings app
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Privacy Settings", @"Privacy Settings")
                                                        message:NSLocalizedString(@"This app does not have access to your contacts.  You can enable access in Privacy Settings.",nil)
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK",@"OK")
                                              otherButtonTitles:nil];
        [alert show];
        
        
    }
    
}

-(void)PerformTaskAfterContactsPermissionGranted{
    
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kContactsAccessPermission];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
  
    NSArray *allPeople = (__bridge NSArray *)(ABAddressBookCopyArrayOfAllPeople(addressBookRef));
//    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"modifiedDate"]) {
//        NSDate *preModifiedDate=[[NSUserDefaults standardUserDefaults] valueForKey:@"modifiedDate"] ;
//        
//        NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
//            ABRecordRef person=(__bridge ABRecordRef)evaluatedObject;
//            CFTypeRef theProperty = ABRecordCopyValue(person, kABPersonPhoneProperty);
//            CFRelease(theProperty);
//            BOOL result=NO;
//            NSDate* modifiedDate = (__bridge NSDate*) ABRecordCopyValue((person),  kABPersonModificationDateProperty);
//            if ([modifiedDate compare:preModifiedDate] == NSOrderedDescending) {
//                result = YES;
//            }else
//                result = NO;
//            
//            
//            
//            return result;
//        }];
//        
//        NSArray *states = [allPeople filteredArrayUsingPredicate:predicate];
//        
////        NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
////            ABRecordRef person=(__bridge ABRecordRef)evaluatedObject;
////            CFTypeRef theProperty = ABRecordCopyValue(person, kABPersonPhoneProperty);
////            CFRelease(theProperty);
////            BOOL result=NO;
////            NSDate* modifiedDate = (__bridge NSDate*) ABRecordCopyValue((person),  kABPersonModificationDateProperty);
////            //            NSInteger* modifiedDate = (__bridge NSDate*) ABRecordCopyValue((person),  kABPersonModificationDateProperty);
////            
////            NSInteger idstring=(NSInteger )ABRecordGetRecordID((ABRecordRef)(person));
////            
////            
////            if (idstring == 1647) {
////                result = YES;
////            }else
////                result = NO;
////            
////            
////            
////            return result;
////        }];
//        
////        NSArray *states = [allPeople filteredArrayUsingPredicate:predicate];
//
//        
//        
//        NSString *strToBeDelete = [AppManager CheckForDeletedContacts:allPeople];
//        
//        
//        [AppManager NewOrUpdatedAddressBookContacts:states andContactsToBeDeleted:strToBeDelete];
//        
//    }else{
//        
//        
    
        [AppManager NewOrUpdatedAddressBookContacts:allPeople andContactsToBeDeleted:@""];
        
        
        
//    }
    
}
+(void)NewOrUpdatedAddressBookContacts:(NSArray*)allPeople andContactsToBeDeleted:(NSString*)strDeleteContactIDs{
    
    //    NSError *writeError = nil;
    NSMutableArray *arrAddressBookMain= [[NSMutableArray alloc] init];
    
    NSMutableArray* arrAddressBook= [[NSMutableArray alloc] init];
    for (id person in allPeople)
    {
        NSMutableArray *arrPhone=[[NSMutableArray alloc]init];

        NSMutableDictionary *dictData = [[NSMutableDictionary alloc] init];

        NSString *name = @"";
        NSString *firstname = @"";
        NSString *lastname = @"";
        
        
        firstname = (__bridge NSString *)ABRecordCopyValue((__bridge ABRecordRef)(person), kABPersonFirstNameProperty);
        lastname = (__bridge_transfer NSString *)ABRecordCopyValue((__bridge ABRecordRef)(person), kABPersonLastNameProperty);
        
        name = (__bridge_transfer NSString *)ABRecordCopyCompositeName((__bridge ABRecordRef)(person));
        
        if ([name length]==0) {
            name = @" ";
        }
        
        if (!lastname) {
            lastname = @"";
        }
        
        if (!firstname) {
            firstname = @"";
        }
        ABRecordCopyValue((__bridge ABRecordRef)(person), kABPersonPhoneProperty);
        ABMultiValueRef Phone = ABRecordCopyValue((__bridge ABRecordRef)(person), kABPersonPhoneProperty);
        ABMultiValueRef Email = ABRecordCopyValue((__bridge ABRecordRef)(person), kABPersonEmailProperty);
        
        for (CFIndex i = 0; i < ABMultiValueGetCount(Phone); i++) {
            CFStringRef label = ABMultiValueCopyLabelAtIndex(Phone, i);
            
            
            
            NSString *phoneLabel1 =(__bridge NSString*) ABAddressBookCopyLocalizedLabel(label);
            
            NSString *phoneNumber = [AppManager removeSpecialCheractersFromPhoneNumber:(__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(Phone, i)];
            NSMutableDictionary *aTemp=[NSMutableDictionary dictionaryWithObjectsAndKeys:phoneLabel1,kType,phoneNumber,@"number", nil];

            [arrPhone addObject:aTemp];
            if (label != NULL)
                CFRelease(label);
        }
        NSMutableArray *arrEmail=[[NSMutableArray alloc]init];
        for (CFIndex i = 0; i < ABMultiValueGetCount(Email); i++) {
            NSString *phoneNumber;
            phoneNumber = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(Email, i);
            CFStringRef label = ABMultiValueCopyLabelAtIndex(Email, i);
            NSString *emailLabel1 =(__bridge NSString*) ABAddressBookCopyLocalizedLabel(label);
            
            NSMutableDictionary *aTemp=[NSMutableDictionary dictionaryWithObjectsAndKeys:emailLabel1,kType,phoneNumber,@"number", nil];
            
            [arrEmail addObject:aTemp];
            if (label!=NULL) {
                CFRelease(label);
            }
        }
        
        NSInteger idstring=(NSInteger )ABRecordGetRecordID((__bridge ABRecordRef)(person));
        [dictData setObject:[NSString stringWithFormat:@"%ld",(long)idstring] forKey:@"uniqueContactID"];
        
        
        NSDate* modifiedDate = (__bridge NSDate*) ABRecordCopyValue( (__bridge ABRecordRef)(person),  kABPersonModificationDateProperty);
        [dictData setObject:[AppManager stringFromDate:modifiedDate] forKey:@"strModifiedDate"];
        
        
        
        if ([arrEmail count] != 0) {
            [dictData setObject:arrEmail forKey:@"email"];
        }
        
        
        if ([AppManager IsStringEmptyWithoutWhiteSpaces:name] && ([arrEmail count]>0 || [arrPhone count]>0)) {
            name=[NSString stringWithFormat:@"%@",[arrPhone count]>0?[[arrPhone firstObject] valueForKey:@"number"]:[[arrEmail firstObject]valueForKey:@"number"]];
        }
        [dictData setObject:name forKey:@"name"];
        
        NSString *notes = (__bridge NSString *)ABRecordCopyValue((__bridge ABRecordRef)(person), kABPersonNoteProperty);
        if (notes) {
            [dictData setObject:notes forKey:kNotes];
        }
        
        
        NSString *jobTitle = (__bridge NSString *)ABRecordCopyValue((__bridge ABRecordRef)(person), kABPersonOrganizationProperty);
        if (jobTitle) {
            [dictData setObject:jobTitle forKey:kJobTitle];
        }
        
        if ([arrPhone count] != 0) {
            
            for(NSDictionary *dict in arrPhone)
            {
                [arrAddressBook addObject:[dict objectForKey:@"number"]];
            }
            
        }
        [dictData setObject:arrPhone forKey:@"phone"];
        [arrAddressBookMain addObject:[NSDictionary dictionaryWithDictionary:dictData]];

        CFRelease(Phone);
        CFRelease(Email);
    }

    [[Database database] SaveAddressBookDataBase:arrAddressBookMain from:NO];
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    NSArray *arrAddressBook = [NSArray arrayWithObjects:@"8377944952",@"9329666665", nil];
    NSArray *contact = [[Database database] fetchDataFromDatabaseForEntity:@"AddressBookDB"];
    NSMutableArray *realMessages = [NSMutableArray arrayWithArray:[contact valueForKey:@"originalUserId"]];
    [realMessages removeObject:[NSNumber numberWithInt:[@"0" intValue]]];
    if([realMessages count]>0)
    {
        [AppManager callAddressBookWebService:[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] valueForKey:kSessionToken],kSessionToken,[[NSUserDefaults standardUserDefaults] valueForKey:kDeviceToken],kDeviceToken,[[NSUserDefaults standardUserDefaults] valueForKey:kUserId],kUserId,arrAddressBook,kPhoneAddress,kGetPhoneBookFriends,kOption,kDevice,kDeviceType,realMessages,@"updatedContactIds",strDeleteContactIDs,@"idsToBeDelete",nil]];
    }
    else
    {
        [AppManager callAddressBookWebService:[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] valueForKey:kSessionToken],kSessionToken,[[NSUserDefaults standardUserDefaults] valueForKey:kDeviceToken],kDeviceToken,[[NSUserDefaults standardUserDefaults] valueForKey:kUserId],kUserId,arrAddressBook,kPhoneAddress,kGetPhoneBookFriends,kOption,kDevice,kDeviceType,strDeleteContactIDs,@"idsToBeDelete",nil]];
    }

    
    //    return arrAddressBook;
}
//This method removes any type of special character from the phone numbers
+(NSString *) removeSpecialCheractersFromPhoneNumber:(NSString *)phoneNumber
{
    NSMutableString *result = [NSMutableString stringWithCapacity:phoneNumber.length];
    
    NSScanner *scanner = [NSScanner scannerWithString:phoneNumber];
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    while ([scanner isAtEnd] == NO)
    {
        NSString *buffer;
        if ([scanner scanCharactersFromSet:numbers intoString:&buffer])
        {
            [result appendString:buffer];
        }
        else
        {
            [scanner setScanLocation:([scanner scanLocation] + 1)];
        }
    }
    
    return result;
}
+(BOOL)IsStringEmptyWithoutWhiteSpaces:(NSString*)string{
    //    NSCharacterSet *characterset=[NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (trimmed.length==0) {
        return YES;
    }
    return NO;
}
+(NSString*)CheckForDeletedContacts:(NSArray*)allPeople{
    
    
    
    NSArray *arrContacts=[[Database database] fetchDataFromDatabaseForEntity:@"AddressBookDB"];
    
    
    NSMutableSet *set2 = [NSMutableSet setWithArray: [arrContacts valueForKey:@"uniqueContactID"] ];
    
    NSNumber * max = [arrContacts valueForKeyPath:@"@max.uniqueContactID"];
    
    NSArray *newArrayFromAddressBook = [(NSArray*)[AppManager simplifiedArray:allPeople] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"uniqueContactID <= %@", max]];
    
    
    NSMutableSet *set1 = [NSMutableSet setWithArray: [newArrayFromAddressBook valueForKey:@"uniqueContactID"]];
    
    [set2 minusSet: set1];
    NSArray *resultArray = [set2 allObjects];
    
    NSString *strToBeDelete=@"";
    for (id obj  in resultArray) {
        strToBeDelete=[strToBeDelete stringByAppendingFormat:@"%@,",obj];
    }
    if ([strToBeDelete length]>0) {
        strToBeDelete = [strToBeDelete substringToIndex:[strToBeDelete length]-1];
    }
    return strToBeDelete;
}
+(NSString*)stringFromDate:(NSDate*)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *strDate=[formatter stringFromDate:date];
    return strDate;
}
+(NSMutableArray*)simplifiedArray:(NSArray*)arrPeoplee{
    NSMutableArray* arrAddressBook= [[NSMutableArray alloc] init];
    for (id person in arrPeoplee)
    {
        
        
        NSMutableDictionary *aTemp=[[NSMutableDictionary alloc] init];
        NSInteger idstring=(NSInteger )ABRecordGetRecordID((__bridge ABRecordRef)(person));
        [aTemp setObject:[NSNumber numberWithInteger:idstring] forKey:@"uniqueContactID"];
        [arrAddressBook addObject:aTemp];
        
    }
    return arrAddressBook;
}
+(NSDate*)DateFromString:(NSString*)strDate{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *date=[formatter dateFromString:strDate];
    return date;
}
void MyAddressBookExternalChangeCallback (
                                          ABAddressBookRef addressBook,
                                          CFDictionaryRef info,
                                          void *context
                                          )
{
    NSLog(@"MyAddressBookExternalChangeCallback called ");
    [[AppManager sharedManager] refreshAB];
}
-(void)refreshAB{
    [[AppManager sharedManager] fetchAddressBookWithContactModel];
}
+(void)callAddressBookWebService:(NSDictionary*)userData{
    if(![Utils isInternetAvailable])
    {
        [Utils showAlertView:kAlertTitle message:NSLocalizedString(@"Check your internet connection.", @"Check your internet connection.")  delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
    }
    else
    {
        if (!userData) {
            return;
        }
        
        NetworkService *obj = [NetworkService sharedInstance];
        [obj sendAsynchRequestByPostToServer:@"friends" dataToSend:userData delegate:self contentType:eAppJsonType andReqParaType:eJson header:NO];

    }
    
}
#pragma mark - call web service to update settings
+(void)callUpdateUserAppSettingWebService:(NSDictionary *)dict serviceType:(NSString *)serviceType{
    if(![Utils isInternetAvailable])
    {
        [Utils showAlertView:kAlertTitle message:NSLocalizedString(@"Check your internet connection.", @"Check your internet connection.") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
    }
    else
    {
        
        NetworkService *obj = [NetworkService sharedInstance];
        
//        NSMutableDictionary *appSetting=[AppManager getUserAppSettingsFromPlist];
        if([serviceType isEqualToString:kUpdateUserSettings])
        {
            NSDictionary *aDict=[NSDictionary dictionaryWithObjectsAndKeys:kDevice,kDeviceType,[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:kDeviceToken]],kDeviceToken,[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:kSessionToken]],kSessionToken,[[NSUserDefaults standardUserDefaults] valueForKey:kUserId],kUserId,kUpdateUserSettings,kOption,dict,kUserSettings, nil];
        
            [obj sendAsynchRequestByPostToServer:kSettings dataToSend:aDict delegate:self contentType:eAppJsonType andReqParaType:eJson header:NO];
        }
        else
        {
            NSDictionary *aDict=[NSDictionary dictionaryWithObjectsAndKeys:kDevice,kDeviceType,[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:kDeviceToken]],kDeviceToken,[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:kSessionToken]],kSessionToken,[[NSUserDefaults standardUserDefaults] valueForKey:kUserId],kUserId,kResetUserSettings,kOption, nil];
            
            [obj sendAsynchRequestByPostToServer:kSettings dataToSend:aDict delegate:self contentType:eAppJsonType andReqParaType:eJson header:NO];
        }
    }
    
}

- (void)DisconnectUser
{
    
}
#pragma mark - Wen Service response handler


+(void)responseHandler :(id)inResponseDic andRequestIdentifier:(NSString *)inReqIdentifier
{
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    if ([[inResponseDic valueForKey:@"status"] integerValue] == RESPONSE_STATUS_SUCCESS) {
        
        
       if ([[inResponseDic valueForKey:kOption] isEqualToString:kGetPhoneBookFriends])
        {
            
            [[Database database] SaveAddressBookDataBase:[inResponseDic valueForKey:@"phoneData"]from:YES];
            [[NSUserDefaults standardUserDefaults] setValue:[NSDate date] forKey:@"modifiedDate"];
            [AppManager sharedManager].isFetchingContacts=NO;
            AppDelegate *appDel=(AppDelegate*)APP_DELEGATE;
            [appDel refreshFriendsTabbarView];
        }
        

    }else if ([[inResponseDic valueForKey:@"status"] integerValue] == RESPONSE_STATUS_AUTHENTICATION_FAIL){
        
        [[AppManager sharedManager] showAuthenticationFailedAlertView];
    }
    
    else
    {
        
            [Utils showAlertView:kAlertTitle message:[inResponseDic valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
    }
    
}

+(void)requestErrorHandler :(NSError *)inError andRequestIdentifier :(NSString *)inReqIdentifier
{
    [AppManager stopStatusbarActivityIndicator];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    NSLog(@"*** requestErrorHandler Error : %@ and Request Udesntifier AppManager: %@ ****",[inError debugDescription],inReqIdentifier);
}

#pragma mark- create Url

+(NSMutableDictionary *)createDifferentUrlFromUrl:(NSString *)mainUrl{
    
    NSMutableArray *arrTemp = [NSMutableArray arrayWithArray:[mainUrl componentsSeparatedByString:@"."]];
    [arrTemp removeLastObject];
    NSString *strTempUrl = [arrTemp componentsJoinedByString:@"."];
    
    NSString *profilePic = mainUrl;
    
    NSString *profile105 = [[NSString stringWithFormat:@"%@%d.jpg",strTempUrl,IMAGE_URL_TYPE_CODE_105_PIXELS_IMAGE] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *profile210 = [[NSString stringWithFormat:@"%@%d.jpg",strTempUrl,IMAGE_URL_TYPE_CODE_210_PIXELS_IMAGE] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *profilePicOriginalCompress = [[NSString stringWithFormat:@"%@%d.jpg",strTempUrl,IMAGE_URL_TYPE_CODE_ORIGINAL_COMPRESS_IMAGE] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *profilePicBlur = [[NSString stringWithFormat:@"%@%d.jpg",strTempUrl,IMAGE_URL_TYPE_CODE_BLUR] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    
    [dict setObject:profilePic forKey:kProfilePic];
    [dict setObject:profile105 forKey:kprofile105];
    [dict setObject:profile210 forKey:kprofile210];
    [dict setObject:profilePicOriginalCompress forKey:kprofilePicOriginalCompress];
    [dict setObject:profilePicBlur forKey:kprofilePicBlur];
    
    
    return dict;
}
+(CGSize)frameForText:(NSString*)text sizeWithFont:(UIFont*)font constrainedToSize:(CGSize)size{
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          font, NSFontAttributeName,
                                          nil];
    CGRect frame = [text boundingRectWithSize:size
                                      options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                   attributes:attributesDictionary
                                      context:nil];
    
    // This contains both height and width, but we really care about height.
    return frame.size;
}

#pragma mark - create App Settings

+ (void)updateAppSettings:(NSMutableDictionary *)dict withKey:(NSString *)key
{
    [AppManager sharedManager].dicAppSettings = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] valueForKey:kAPP_SETTINGS]];
    if([AppManager sharedManager].dicAppSettings!=nil)
    {
        if([[[AppManager sharedManager].dicAppSettings allKeys]containsObject:key])
        {
            [[AppManager sharedManager].dicAppSettings removeObjectForKey:key];
            [[AppManager sharedManager].dicAppSettings setObject:dict forKey:key];
        }
    }
    else
    {
        [AppManager sharedManager].dicAppSettings = [[NSMutableDictionary alloc]init];
        [[AppManager sharedManager].dicAppSettings setObject:dict forKey:key];
    }
    [[NSUserDefaults standardUserDefaults] setObject:[NSDictionary dictionaryWithDictionary:[AppManager sharedManager].dicAppSettings] forKey:kAPP_SETTINGS];
}
+(void)clearAllConversation
{
    NSManagedObjectContext *context = [gCXMPPController messagesStoreMainThreadManagedObjectContext]; // your managed object context
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"XMPPMessageArchiving_Message_CoreDataObject"];
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    if (objects == nil) {
        // handle error
    } else {
        for (NSManagedObject *object in objects) {
            [context deleteObject:object];
        }
        [context save:&error];
    }
    request = nil;
    request = [NSFetchRequest fetchRequestWithEntityName:@"XMPPMessageArchiving_Contact_CoreDataObject"];
    objects = nil;
    objects = [context executeFetchRequest:request error:&error];
    if(objects == nil)
    {
        
    }
    else
    {
        for (NSManagedObject *object in objects) {
            [context deleteObject:object];
        }
        [context save:&error];
        
    }
}
@end
