//
//  UIImage+Extension.h
//  DailyReports
//
//  Created by Pankaj on 12/16/13.
//  Copyright (c) 2013 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#define kMaxSizeOriginalImage 960

@interface UIImage (Extension)
+(UIImage *)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
+(UIImage *)scaleDownImage:(UIImage *)image toSize:(CGSize)newSize;
+(UIImage *)scaleDownOriginalImage:(UIImage *)image;
+(UIImage*)cropImage:(UIImage *)image withSize:(CGFloat)Size;
//+(UIImage*)cropImage:(UIImage *)image;
+(UIImage *)scaleDownOriginalImage:(UIImage *)image ProportionateTo:(int)MaxHeight;
+(UIImage*)cropImageInSquare:(UIImage *)image;

@end
