//
//  AppDelegate.h
//  UmmApp
//
//  Created by Neha Saxena on 18/10/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SMMessageDelegate.h"
#import "ChatHomeViewControllerViewController.h"
#import "MoreViewController.h"
#import "OfficialAccountViewController.h"
#import "AllContactsViewController.h"
#import "TimelineViewController.h"
#import "AllContactsViewController.h"
#import "PhoneBookViewController.h"
#import "AddressBookDB.h"


@class SMChatViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate>
{
    UIBackgroundTaskIdentifier bgTask;
    __weak NSObject <SMMessageDelegate> *_messageDelegate;
}
@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
#pragma mark - Tabbar Classes

@property (nonatomic, strong) TimelineViewController *timelineViewVC;
@property (nonatomic, strong) PhoneBookViewController *phoneBook;
//@property (nonatomic, strong) OfficialAccountViewController *OfficialAccountVC;
@property (nonatomic, strong) AllContactsViewController *allContactsVC;
@property (nonatomic, strong) ChatHomeViewControllerViewController *chatHomeVC;
@property (nonatomic, strong) MoreViewController *moreVC;



#pragma mark - chat details
@property (strong,nonatomic) UINavigationController *nav;
@property (nonatomic, strong) UITabBarController *tabBarController;

@property(nonatomic,assign)BOOL isChatViewOpened;
@property (strong, nonatomic) NSMutableDictionary *AllChatViewConDic;
@property (nonatomic, strong) SMChatViewController *chatViewController;
@property (nonatomic, strong) NSDictionary *dictNotification;

- (void)showTabbarview;
- (void)removeTabbarView;
- (void)refreshFriendsTabbarView;
//-(BOOL)openChatViewfromNotificationViewByFriendDetail:(FriendsDetails *)inFrndDetail;
-(void)setChatWindowOpenedStatusBySender:(NSString*)inSender andBool:(BOOL)inBool;

-(BOOL)getChatWindowOpenedStatusBySender:(NSString*)inSender;
-(SMChatViewController *)createChatViewByChatUserNameIfNeeded:(NSString *)inChatUser;

-(void)releaseChatViewByChatUserNameIfNeeded:(NSString *)inChatUser;

-(SMChatViewController *)getChatViewDelegateByChatUserName:(NSString *)inChatUser;
-(void)sendPresence:(NSString *)type;
-(BOOL)openChatViewfromNotificationViewByFriendDetail:(AddressBookDB *)inFrndDetail;
-(BOOL)openChatViewfromNotificationViewByFriendDetailAnonymous:(NSString *)inFrndDetail;

@end
