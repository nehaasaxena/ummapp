//
//  AppDelegate.m
//  UmmApp
//
//  Created by Neha Saxena on 18/10/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "AppDelegate.h"
#import "Foursquare2.h"
#import "RegistrationViewController.h"
#import "PhoneVerifyViewController.h"
@interface AppDelegate()
{
    Reachability *aReachability;
}
@end

@implementation AppDelegate
@synthesize isChatViewOpened;
@synthesize AllChatViewConDic;
@synthesize dictNotification;
@synthesize chatViewController = _chatViewController;
@synthesize nav;
@synthesize tabBarController;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self initializeAllSingletonObjects];

      self.isChatViewOpened = NO;
    self.tabBarController = [[UITabBarController  alloc]init];
    aReachability = [Reachability reachabilityForInternetConnection] ;
    [aReachability startNotifier];
    [self setChatWindowOpenedStatusBySender:nil andBool:NO];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(internetConnectionListener:) name: kReachabilityChangedNotification object: nil];

    if ([[[UIDevice currentDevice] model] isEqualToString:@"iPhone Simulator"])
    {
        
        
        [[NSUserDefaults standardUserDefaults] setValue:@"3304645e047e061df52d0635ac8171941826e6dc467aff1d5e12d4c8d4da6be0" forKey:kDeviceToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    else
    {
        //        NSLog(@"Registering Device");
        if(![[NSUserDefaults standardUserDefaults]valueForKey:kDeviceToken])
        {
            [[NSUserDefaults standardUserDefaults] setValue:@"1234567890" forKey:kDeviceToken];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
        {
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
        else
        {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
             (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
        }
        
        if (![self IsPushNotificationOn])
        {
            [[NSUserDefaults standardUserDefaults] setValue:@"1234567890" forKey:kDeviceToken];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        //        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
        
        
    }
    if (![[NSUserDefaults standardUserDefaults] boolForKey:kIsLoggedIn]) {
        
            RegistrationViewController *registeration=[[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController" bundle:nil];
            
            self.nav = [[UINavigationController alloc]initWithRootViewController:registeration];
//        This is done to store refrence of NewHomeViewController so that on receiving remote Notifications we can update the Notifications webservice
        
    }else{
        [gCXMPPController connect];
        UILocalNotification *localNotif =[launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey];
        if (localNotif) {
            [AppManager sharedManager].notifyDict= [NSMutableDictionary dictionaryWithDictionary:[localNotif valueForKey:@"aps"]];
        }
        [self showTabbarview];

    }
    self.window.rootViewController = self.nav;
    
//    [self.nav.navigationBar setBarTintColor:[UIColor colorWithRed:16.0/255.0 green:170.0/255.0 blue:180.0/255.0 alpha:1.0]];
//    [self.nav.navigationBar setTranslucent:YES];
    [self.window makeKeyAndVisible];

    if ([[[NSUserDefaults standardUserDefaults] valueForKey:kUserId] integerValue] != 0 && [[NSUserDefaults standardUserDefaults] valueForKey:kIsLoggedIn] ) {
        [[AppManager sharedManager] fetchAddressBookWithContactModel];
    }
    [Foursquare2 setupFoursquareWithClientId:kFourSquareClientId
                                      secret:kFourSquareSecretId
                                 callbackURL:@"UmmApp1://foursquare"];
    
    return YES;
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [Foursquare2 handleURL:url];
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    if([self.nav.topViewController isKindOfClass:[SMChatViewController class]])
    {
        NSString *Key =[[self.AllChatViewConDic allKeys] lastObject];
        _chatViewController = [self.AllChatViewConDic objectForKey:Key];
        [_chatViewController StopAudioRecorder];
    }

    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    
    if([Utils isInternetAvailable])
    {
        [self sendPresence:@"away"];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STOPAUDIO" object:nil];
    //7-3-14
    UIPasteboard *pb = [UIPasteboard generalPasteboard];
    NSMutableArray *aPasteArr =  [NSMutableArray arrayWithArray: [pb strings]];
    if ([aPasteArr containsObject:kUMMAPPFORWARD])
    {
        [pb setStrings: Nil];
    }
    //end
    UIApplication *app = [UIApplication sharedApplication];
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        XMPPPresence *presence = [XMPPPresence presence];
        NSXMLElement *status = [NSXMLElement elementWithName:@"status"];
        [status setStringValue:@"busy"];
        [presence addChild:status];
        [presence addAttributeWithName:@"date" stringValue:[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]]];
        [gCXMPPController.xmppStream sendElement:presence];
        [app endBackgroundTask:bgTask];
    }];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    if([Utils isInternetAvailable])
    {
        [self sendPresence:@"online"];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:kUserId] integerValue] != 0 && [[NSUserDefaults standardUserDefaults] valueForKey:kIsLoggedIn] ) {
        [[AppManager sharedManager] fetchAddressBookWithContactModel];
    }
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];

    if([Utils isInternetAvailable])
    {
        [self sendPresence:@"online"];
    }

    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    if([Utils isInternetAvailable])
    {
        XMPPPresence *presence = [XMPPPresence presence];
        NSXMLElement *status = [NSXMLElement elementWithName:@"status"];
        [status setStringValue:@"busy"];
        [presence addChild:status];
        [presence addAttributeWithName:@"date" stringValue:[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]]];
        [gCXMPPController.xmppStream sendElement:presence];
    }

    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - enter In tabbar controller
- (void)showTabbarview
{
    tabBarController.tabBar.tintColor = [UIColor colorWithRed:16.0/255.0 green:170.0/255.0 blue:180.0/255.0 alpha:1.0];
    UIImage *tabBackground = nil;
    tabBackground = [[UIImage imageNamed:@"tabBar"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    [self.tabBarController.tabBar setBackgroundImage:tabBackground];

//    _timelineViewVC=[[TimelineViewController alloc] initWithNibName:@"TimelineViewController" bundle:nil];
//    _timelineViewVC.tabBarItem.image = [[UIImage imageNamed:@"iconAccounts"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    _timelineViewVC.tabBarItem.selectedImage = [[UIImage imageNamed:@"iconAccountsActive"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    _timelineViewVC.extendedLayoutIncludesOpaqueBars = YES;
//    _timelineViewVC.title = @"Timeline";
// 
//    
//    UINavigationController *nav_timeline = [[UINavigationController alloc] initWithRootViewController: _timelineViewVC];
//    nav_timeline.navigationBar.barTintColor = [UIColor colorWithRed:16.0/255.0 green:170.0/255.0 blue:180.0/255.0 alpha:1.0];
    
    
    _phoneBook=[[PhoneBookViewController alloc] initWithNibName:@"PhoneBookViewController" bundle:nil];
    _phoneBook.tabBarItem.image = [[UIImage imageNamed:@"tabBarStar"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    _phoneBook.tabBarItem.selectedImage = [[UIImage imageNamed:@"tabBarStarActive"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    _phoneBook.extendedLayoutIncludesOpaqueBars = YES;
    _phoneBook.title = NSLocalizedString(@"Accounts", @"Accounts") ;

    UINavigationController *nav_officials = [[UINavigationController alloc] initWithRootViewController: _phoneBook];
//    nav_officials.navigationBar.barTintColor = [UIColor colorWithRed:16.0/255.0 green:170.0/255.0 blue:180.0/255.0 alpha:1.0];

    _allContactsVC=[[AllContactsViewController alloc] initWithNibName:@"AllContactsViewController" bundle:nil];
    _allContactsVC.tabBarItem.image = [[UIImage imageNamed:@"tabBarContacts"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    _allContactsVC.tabBarItem.selectedImage = [[UIImage imageNamed:@"tabBarContactsActive"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    _allContactsVC.extendedLayoutIncludesOpaqueBars = YES;
    _allContactsVC.title = NSLocalizedString(@"Contacts", @"Contacts");

    UINavigationController *nav_Contacts = [[UINavigationController alloc] initWithRootViewController: _allContactsVC];
//    nav_Contacts.navigationBar.barTintColor = [UIColor colorWithRed:16.0/255.0 green:170.0/255.0 blue:180.0/255.0 alpha:1.0];

    _chatHomeVC=[[ChatHomeViewControllerViewController alloc] initWithNibName:@"ChatHomeViewControllerViewController" bundle:nil];
    _chatHomeVC.tabBarItem.image = [[UIImage imageNamed:@"tabBarChat"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    _chatHomeVC.tabBarItem.selectedImage = [[UIImage imageNamed:@"tabBarChatActive"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    _chatHomeVC.extendedLayoutIncludesOpaqueBars = YES;
    _chatHomeVC.title = NSLocalizedString(@"Chats", @"Chats");

    
    UINavigationController *nav_chatHome= [[UINavigationController alloc] initWithRootViewController: _chatHomeVC];
//    nav_chatHome.navigationBar.barTintColor = [UIColor colorWithRed:16.0/255.0 green:170.0/255.0 blue:180.0/255.0 alpha:1.0];

    _moreVC=[[MoreViewController alloc] initWithNibName:@"MoreViewController" bundle:nil];
    _moreVC.tabBarItem.image = [[UIImage imageNamed:@"tabBarMore"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    _moreVC.tabBarItem.selectedImage = [[UIImage imageNamed:@"tabBarMoreActive"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    _moreVC.title = NSLocalizedString(@"More", @"More");
    _moreVC.extendedLayoutIncludesOpaqueBars = YES;
    
    UINavigationController *nav_More= [[UINavigationController alloc] initWithRootViewController: _moreVC];
//    nav_More.navigationBar.barTintColor = [UIColor colorWithRed:16.0/255.0 green:170.0/255.0 blue:180.0/255.0 alpha:1.0];

    NSArray* controllers = [NSArray arrayWithObjects:nav_officials,nav_Contacts,nav_chatHome,nav_More, nil];
    
    self.tabBarController.viewControllers = controllers;
    self.tabBarController.selectedIndex=2;
    self.tabBarController.delegate = self;
//    for(id viewController in self.nav.viewControllers)
//    {
//        if([viewController isKindOfClass:[RegistrationViewController class]])
//        {
//            [self.nav popToViewController:viewController animated:YES];
//            break;
//        }
//    }
    self.tabBarController.tabBar.translucent = NO;
    NSLog(@"%@",self.nav.viewControllers);
    if([self.nav.viewControllers count]>0)
    {
        [self.nav pushViewController:self.tabBarController animated:YES];
    }
    else
    {
        if(!self.nav)
            self.nav = [[UINavigationController alloc]initWithRootViewController:self.tabBarController];
    }
    [self.nav navigationBar].hidden = YES;

}
- (void)removeTabbarView
{
    [self.nav navigationBar].hidden = NO;
    if([[self.nav viewControllers]containsObject:[RegistrationViewController class]])
    {
        [self.nav popToRootViewControllerAnimated:YES];
    }
    else
    {
        RegistrationViewController *registration = [[RegistrationViewController alloc]initWithNibName:@"RegistrationViewController" bundle:nil];
        self.nav = [[UINavigationController alloc]initWithRootViewController:registration];
        self.window.rootViewController = self.nav;
        [self.window makeKeyAndVisible];
    }
}
- (void)refreshFriendsTabbarView
{
    UINavigationController *navController=(UINavigationController*)self.tabBarController.selectedViewController;
    UIViewController *viewController=[[navController childViewControllers] objectAtIndex:0];
    if ([viewController isKindOfClass:[AllContactsViewController class]]) {
        AllContactsViewController *contacts=(AllContactsViewController*)viewController;
        [contacts refreshArrayOfAllContacts];
    }else if ([viewController isKindOfClass:[PhoneBookViewController class]]){
        PhoneBookViewController *phoneBook=(PhoneBookViewController*)viewController;
        [phoneBook refreshArrayOfAllContacts];
    }
}
#pragma mark - Push notifiction methods

-(BOOL)IsPushNotificationOn{
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        if ([[UIApplication sharedApplication] isRegisteredForRemoteNotifications]) {
            return YES;
        }else
            return NO;
    }else{
        
        UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if (types & UIRemoteNotificationTypeAlert)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *deviceTokenStr = [[[[deviceToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""] stringByReplacingOccurrencesOfString: @">" withString: @""] stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    [[NSUserDefaults standardUserDefaults] setValue:deviceTokenStr forKey:kDeviceToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
//        NSLog(@"Registered");
    
        NSLog(@"Device Token: %@ ", deviceTokenStr);
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    NSDictionary *apsDict = [userInfo valueForKey:@"aps"];
    
    NSLog(@"********************** &&&&&&&& -- > %@",apsDict);
    //When app is active than only home screen counts should be refreshed
}
-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    
    NSLog(@"@@@Local Notification@@@");
    NSDictionary *dict = notification.userInfo;
    self.dictNotification = [NSDictionary dictionaryWithDictionary:dict];
    if([[self.dictNotification valueForKey:@"type"]isEqual:@"chat"])
    {
        if([self getChatWindowOpenedStatusBySender:[[[self.dictNotification valueForKey:kchatUserName] stringByAppendingString:[NSString stringWithFormat:@"@%@",STRChatServerURL]]lowercaseString]])
        {
            if([self.nav.topViewController isKindOfClass:[SMChatViewController class]])
                return;
        }
        SMChatViewController *chatView = nil;
        chatView = [self createChatViewByChatUserNameIfNeeded: [self.dictNotification objectForKey:kchatUserName]];
        chatView.chatWithUser =[[[self.dictNotification valueForKey:kchatUserName] stringByAppendingString:[NSString stringWithFormat:@"@%@",STRChatServerURL]]lowercaseString];
        chatView.userName = [self.dictNotification valueForKey:@""];
        //    UIImageView *img = [[UIImageView alloc]init];
        //    [img setImageWithURL:[NSURL URLWithString:[self.dictNotification valueForKey:kprofilePicActual]] placeholderImage:[UIImage imageNamed:kUserDefaultImage]];
        chatView.imageString = [self.dictNotification objectForKey:@""];
        //    chatView.img = img.image;
        chatView.friendNameId = [self.dictNotification objectForKey:@""];
        
        //   [self.nav pushViewController:chatView animated:YES];
        switch (chatView.eSMChatStatus) {
            case SMCHAT_AT_TOP_OF_STACK:
                
                break;
            case SMCHAT_EXIST_BUT_NOT_ON_TOP:
                [self.nav popToViewController:chatView animated:YES];
                break;
            case SMCHAT_NOT_IN_STACK:
                [self.nav pushViewController:chatView animated:YES];
                break;
            default:
                break;
        }
    }

}
#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"UmmApp" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"UmmApp.sqlite"];
    NSLog(@"%@",storeURL);
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}
#pragma mark - Chatting Methods

- (XMPPStream *)xmppStream {
    return [gCXMPPController xmppStream];
}
-(void)initializeAllSingletonObjects
{
    [CXMPPController sharedXMPPController];
}

-(SMChatViewController *)createChatViewByChatUserNameIfNeeded:(NSString *)inChatUser
{
    
    if (self.AllChatViewConDic == nil)
    {
        self.AllChatViewConDic = [[NSMutableDictionary alloc] init];
        
    }
    SMChatViewController *aChatViewCon = [self.AllChatViewConDic objectForKey: inChatUser];
    NSUInteger index=[[self.nav childViewControllers] indexOfObject:aChatViewCon];
    
    
    if (aChatViewCon)
    {
        if (index == NSNotFound)
        {
            aChatViewCon.isAlreadyInStack= NO;
            aChatViewCon.eSMChatStatus = SMCHAT_NOT_IN_STACK;
            
        }
        else if (index == [[self.nav childViewControllers] count]-1)
        {
            aChatViewCon.isAlreadyInStack= YES;
            aChatViewCon.eSMChatStatus = SMCHAT_AT_TOP_OF_STACK;
            
        }
        else
        {
            aChatViewCon.isAlreadyInStack= YES;
            aChatViewCon.eSMChatStatus = SMCHAT_EXIST_BUT_NOT_ON_TOP;
            
        }
    }
    else
    {
        
        aChatViewCon = [[SMChatViewController alloc] initWithNibName:@"SMChatViewController" bundle:nil];
        [self.AllChatViewConDic setObject: aChatViewCon forKey: inChatUser];
        
        aChatViewCon.isAlreadyInStack= NO;
        aChatViewCon.eSMChatStatus=SMCHAT_NOT_IN_STACK;
    }
    return  aChatViewCon;
    
}

-(SMChatViewController *)getChatViewDelegateByChatUserName:(NSString *)inChatUser
{
    SMChatViewController *aChatViewCon = [self.AllChatViewConDic objectForKey: inChatUser];
    return  aChatViewCon;
}

-(void)releaseChatViewByChatUserNameIfNeeded:(NSString *)inChatUser
{
    id aChatViewCon = [self.AllChatViewConDic objectForKey: inChatUser];
    
    if (aChatViewCon)
    {
        [self.AllChatViewConDic removeObjectForKey: inChatUser];
        
    }
}
-(void)sendPresence:(NSString *)type
{
    if([type isEqualToString:@"away"])
    {
        XMPPPresence *presence = [XMPPPresence presence];
        NSXMLElement *show = [NSXMLElement elementWithName:@"show" stringValue:@"away"];
        NSXMLElement *status = [NSXMLElement elementWithName:@"status" stringValue:@"away"];
        [presence addChild:show];
        [presence addChild:status];
        [presence addAttributeWithName:@"date" stringValue:[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]]];
        [gCXMPPController.xmppStream sendElement:presence];
    }
    else
    {
        XMPPPresence *presence = [XMPPPresence presence];
        NSXMLElement *status = [NSXMLElement elementWithName:@"status"];
        [status setStringValue:@"online"];
        [presence addChild:status];
        [presence addAttributeWithName:@"date" stringValue:[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]]];
        [gCXMPPController.xmppStream sendElement:presence];
    }
}


//-(BOOL)openChatViewfromNotificationViewByFriendDetail:(FriendsDetails *)inFrndDetail
//{
//    
//}
-(void)setChatWindowOpenedStatusBySender:(NSString*)inSender andBool:(BOOL)inBool
{
    if(inBool)
    {
        [[NSUserDefaults standardUserDefaults] setValue:inSender forKey:kMessageChatWithUser];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:kMessageChatWithUser];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
}
-(BOOL)getChatWindowOpenedStatusBySender:(NSString*)inSender
{
    if(inSender==nil)
        return NO;
    
    BOOL aStatus = NO;
    if([[[NSUserDefaults standardUserDefaults] valueForKey:kMessageChatWithUser]isEqualToString:inSender])
    {
        aStatus = YES;
    }
    return  aStatus;
}
-(BOOL)openChatViewfromNotificationViewByFriendDetailAnonymous:(NSString *)inFrndDetail
{
    UINavigationController *navcon = (UINavigationController*)self.tabBarController.selectedViewController;
    if([self getChatWindowOpenedStatusBySender:inFrndDetail])
    {
        if([navcon.topViewController isKindOfClass:[SMChatViewController class]])
            return NO;
    }
    SMChatViewController *chatView = nil;
    chatView = [self createChatViewByChatUserNameIfNeeded: inFrndDetail];
    chatView.chatWithUser =[[[inFrndDetail lowercaseString] stringByAppendingString:[NSString stringWithFormat:@"@%@",STRChatServerURL]]lowercaseString];
    chatView.userName = inFrndDetail;
    chatView.imageString = nil;
    
    chatView.friendNameId = nil;
    
    switch (chatView.eSMChatStatus) {
        case SMCHAT_AT_TOP_OF_STACK:
            return NO;
            break;
        case SMCHAT_EXIST_BUT_NOT_ON_TOP:
            [navcon popToViewController:chatView animated:YES];
            return YES;
            break;
        case SMCHAT_NOT_IN_STACK:
            [navcon pushViewController:chatView animated:YES];
            return YES;
            break;
        default:
            break;
    }
    
    
}
-(BOOL)openChatViewfromNotificationViewByFriendDetail:(AddressBookDB *)inFrndDetail
{
   UINavigationController *navcon = (UINavigationController*)self.tabBarController.selectedViewController;
    if([self getChatWindowOpenedStatusBySender:inFrndDetail.orgChatUsername])
    {
        if([navcon.topViewController isKindOfClass:[SMChatViewController class]])
            return NO;
    }
    SMChatViewController *chatView = nil;
    chatView = [self createChatViewByChatUserNameIfNeeded: [inFrndDetail.orgChatUsername lowercaseString]];
    chatView.chatWithUser =[[[inFrndDetail.orgChatUsername lowercaseString] stringByAppendingString:[NSString stringWithFormat:@"@%@",STRChatServerURL]]lowercaseString];
    chatView.userName = inFrndDetail.fullName;
    chatView.imageString = inFrndDetail.profilePic;
    
    chatView.friendNameId = [NSString stringWithFormat:@"%@",inFrndDetail.userId];
    
    switch (chatView.eSMChatStatus) {
        case SMCHAT_AT_TOP_OF_STACK:
            return NO;
            break;
        case SMCHAT_EXIST_BUT_NOT_ON_TOP:
            [navcon popToViewController:chatView animated:YES];
            return YES;
            break;
        case SMCHAT_NOT_IN_STACK:
            [navcon pushViewController:chatView animated:YES];
            return YES;
            break;
        default:
            break;
    }
    
    
}

#pragma mark check internet avalability method

-(void)internetConnectionListener:(NSNotification *)inNotification
{
    if([Utils isInternetAvailable])
    {
        
        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground)
        {
            if([[aReachability currentReachabilityString]isEqualToString:@"Cellular"])
            {
                [gCXMPPController disconnect];
            }
            if(![gCXMPPController isConnected])
            {
                
                [gCXMPPController connect];
            }
            [self sendPresence:@"away"];
        }
        else
        {
            [gCXMPPController disconnect];
            [gCXMPPController connect];
            [self sendPresence:@"online"];
            
        }
        
    }
    else
    {
    }
}

@end
