//
//  ShowContactViewController.m
//  UmmApp
//
//  Created by Neha Saxena on 13/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "ShowContactViewController.h"

@interface ShowContactViewController ()

@end

@implementation ShowContactViewController
- (void)setNavigationBar
{
    self.navigationController.navigationBarHidden=NO;
    if(self.isFromSelf==NO)
    {
        UIButton *doneButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
        [doneButton setTitle:NSLocalizedString(@"Save", @"Save")  forState:UIControlStateNormal];
        [doneButton setTitleColor:[UIColor colorWithRed:16.0/255.0 green:170.0/255.0 blue:180.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [doneButton.titleLabel setFont:[UIFont boldSystemFontOfSize:17.0f]];
        [doneButton addTarget:self action:@selector(btnSaveClicked:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
    }

    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,11, 20)];
    [backButton setImage:[UIImage imageNamed:@"arrow1"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem.enabled = YES;
}
- (void)btnSaveClicked:(id)sender
{
    ABRecordRef newPerson = ABPersonCreate();
    
    CFErrorRef error = NULL;
    NSString *name =[[self.contact objectAtIndex:0] objectForKey:kFullName];
    NSString *job =[[self.contact objectAtIndex:0] objectForKey:kJobTitle];
    ABRecordSetValue(newPerson, kABPersonFirstNameProperty, (__bridge CFTypeRef)(name), &error);
    if([job length]>0)
        ABRecordSetValue(newPerson, kABPersonJobTitleProperty, (__bridge CFTypeRef)(job), &error);
    ABMutableMultiValueRef multiPhone = ABMultiValueCreateMutable(kABMultiStringPropertyType);

    for (NSString *number in self.arrContact) {
        ABMultiValueAddValueAndLabel(multiPhone, (__bridge CFTypeRef)(number), kABHomeLabel, NULL);
    }
    
    ABRecordSetValue(newPerson, kABPersonPhoneProperty, multiPhone,nil);

    NSAssert( !error, @"Something bad happened here." );
    
    // Create and set-up the new person view controller
    ABNewPersonViewController* newPersonViewController = [[ABNewPersonViewController alloc] initWithNibName:nil bundle:nil];
    [newPersonViewController setDisplayedPerson:newPerson];
    [newPersonViewController setNewPersonViewDelegate:self];
    // Wrap in a nav controller and display
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:newPersonViewController];
    [self presentViewController:navController animated:YES completion:nil];
    
    // Clean up everything
    CFRelease(newPerson);
}
#pragma mark ABNewPersonViewControllerDelegate methods
// Dismisses the new-person view controller.
- (void)newPersonViewController:(ABNewPersonViewController *)newPersonViewController didCompleteWithNewPerson:(ABRecordRef)person
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)backButtonClicked
{
    self.navigationController.navigationBarHidden=YES;
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar];
    [self setTableViewHeader];
//    self.tvShowContact.frame = CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height);

    if(!self.arrContact)
    {
        self.arrContact = [[NSMutableArray alloc] init];
    }
    [self.arrContact removeAllObjects];
    self.arrContact  = [NSMutableArray arrayWithArray:[[[self.contact objectAtIndex:0] objectForKey:@"number"] componentsSeparatedByString:@","]];
    // Do any additional setup after loading the view from its nib.
}

-(void)setTableViewHeader{
    CGSize postTextHeight=[AppManager frameForText:[[self.contact objectAtIndex:0] objectForKey:kFullName] sizeWithFont:[UIFont systemFontOfSize:17] constrainedToSize:CGSizeMake(250, 1000)];
    
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, ceil(postTextHeight.height)+50)];
    UILabel *labelFullName=[[UILabel alloc] initWithFrame:CGRectMake(40, 10, 250, ceil(postTextHeight.height))];
    [labelFullName setNumberOfLines:1];
    [labelFullName setBackgroundColor:[UIColor clearColor]];
    [labelFullName setTextColor:[UIColor blackColor]];
    [labelFullName setText:[[self.contact objectAtIndex:0] objectForKey:kFullName]];
    [labelFullName setFont:[UIFont boldSystemFontOfSize:19]];
    [view addSubview:labelFullName];
    
            if ([[[self.contact objectAtIndex:0] objectForKey:kJobTitle] length]>0) {
                CGSize jobTitleTextHeight=[AppManager frameForText:[[self.contact objectAtIndex:0] objectForKey:kJobTitle] sizeWithFont:[UIFont systemFontOfSize:17] constrainedToSize:CGSizeMake(250, 1000)];
                UILabel *lblJobTitle = [[UILabel alloc] initWithFrame:CGRectMake(40, labelFullName.frame.size.height+labelFullName.frame.origin.y, 250, ceil(jobTitleTextHeight.height))];
                [lblJobTitle setNumberOfLines:1];
                [lblJobTitle setBackgroundColor:[UIColor clearColor]];
                [lblJobTitle setTextColor:[UIColor blackColor]];
                [lblJobTitle setText:[[self.contact objectAtIndex:0] objectForKey:kJobTitle]];
                [lblJobTitle setFont:[UIFont systemFontOfSize:16]];
                [view addSubview:lblJobTitle];
                
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width,ceil(jobTitleTextHeight.height)+ceil(postTextHeight.height)+50)];
                
            }
    
    //    if (_strJobTitle) {
    //        CGSize jobTitleTextHeight=[AppManager frameForText:_strJobTitle sizeWithFont:[UIFont fontWithName:ssFontHelveticaNeue size:17] constrainedToSize:CGSizeMake(250, 1000)];
    //        UILabel *lblJobTitle = [[UILabel alloc] initWithFrame:CGRectMake(40, labelFullName.frame.size.height+labelFullName.frame.origin.y, 250, ceil(jobTitleTextHeight.height))];
    //        [lblJobTitle setNumberOfLines:2];
    //        [lblJobTitle setBackgroundColor:[UIColor clearColor]];
    //        [lblJobTitle setTextColor:[UIColor blackColor]];
    //        [lblJobTitle setText:_strJobTitle];
    //        [lblJobTitle setFont:[UIFont systemFontOfSize:16]];
    //        [view addSubview:lblJobTitle];
    //
    //        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width,ceil(jobTitleTextHeight.height)+ceil(postTextHeight.height)+50)];
    //
    //    }
    
    
    self.tvShowContact.tableHeaderView=view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrContact count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    cellIdentifier = @"ContactsCell";
    UITableViewCell *cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    cell.textLabel.text=NSLocalizedString(@"Mobile", @"Mobile");
    cell.textLabel.textColor = [UIColor blueColor];
    cell.textLabel.font = [UIFont systemFontOfSize:12.0];
    cell.detailTextLabel.text = [self.arrContact objectAtIndex:indexPath.row];
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
