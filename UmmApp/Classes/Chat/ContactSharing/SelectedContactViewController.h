//
//  SelectedContactViewController.h
//  UmmApp
//
//  Created by Neha Saxena on 12/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SelectedContactViewDelegate <NSObject>
- (void)selectedContactDetail:(NSMutableDictionary *)dictContact;
@end

@interface SelectedContactViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tvSelectedContact;
@property (strong, nonatomic) NSMutableArray *arrContact;
@property (strong, nonatomic) AddressBookDB *address;
@property (strong, nonatomic) NSMutableArray *selectedNo;
@property (nonatomic, weak) id <SelectedContactViewDelegate>delegate;
@end
