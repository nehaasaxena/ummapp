//
//  SelectedContactViewController.m
//  UmmApp
//
//  Created by Neha Saxena on 12/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "SelectedContactViewController.h"

@interface SelectedContactViewController ()

@end

@implementation SelectedContactViewController
- (void)setNavigationBar
{
    UIButton *doneButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
    [doneButton setTitle:NSLocalizedString(@"Send", @"Send")  forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor colorWithRed:16.0/255.0 green:170.0/255.0 blue:180.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [doneButton.titleLabel setFont:[UIFont boldSystemFontOfSize:17.0f]];
    [doneButton addTarget:self action:@selector(btnSendClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,11, 20)];
    [backButton setImage:[UIImage imageNamed:@"arrow1"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem.enabled = YES;
}
- (void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)btnSendClicked:(id)sender
{
    if([self.selectedNo count]>0)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        if([address.fullName length]>0)
        {
            [dict setObject:address.fullName forKey:kFullName];
        }
        else
        {
            [dict setObject:@"" forKey:kFullName];
        }
        if([address.jobTitle length]>0)
        {
            [dict setObject:address.jobTitle forKey:kJobTitle];
        }
        else
        {
            [dict setObject:@"" forKey:kJobTitle];
        }
        NSString *strNumber = [self.selectedNo componentsJoinedByString:@","];
        [dict setObject:strNumber forKey:@"number"];
        if([self.delegate respondsToSelector:@selector(selectedContactDetail:)])
        {
            [self.delegate selectedContactDetail:[NSMutableDictionary dictionaryWithDictionary:dict]];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@synthesize address;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar];
    [self setTableViewHeader];
    if(!self.arrContact)
    {
        self.arrContact = [[NSMutableArray alloc] init];
    }
    if(!self.selectedNo)
    {
        self.selectedNo = [[NSMutableArray alloc] init];
    }

    [self.arrContact removeAllObjects];
    [self.selectedNo removeAllObjects];
    id data=[NSJSONSerialization JSONObjectWithData:[address.phone dataUsingEncoding:NSUTF8StringEncoding]
                                            options:0 error:NULL];
    for(NSDictionary *dict in data)
    {
        [self.arrContact addObject:[dict objectForKey:@"number"]];
        [self.selectedNo addObject:[dict objectForKey:@"number"]];
    }
    self.tvSelectedContact.frame = CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height);
    // Do any additional setup after loading the view from its nib.
}
#pragma mark - table view methods
-(void)setTableViewHeader{
    CGSize postTextHeight=[AppManager frameForText:address.fullName sizeWithFont:[UIFont systemFontOfSize:17] constrainedToSize:CGSizeMake(250, 1000)];
    
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, ceil(postTextHeight.height)+50)];
    UILabel *labelFullName=[[UILabel alloc] initWithFrame:CGRectMake(40, 10, 250, ceil(postTextHeight.height))];
    [labelFullName setNumberOfLines:1];
    [labelFullName setBackgroundColor:[UIColor clearColor]];
    [labelFullName setTextColor:[UIColor blackColor]];
    [labelFullName setText:address.fullName];
    [labelFullName setFont:[UIFont boldSystemFontOfSize:19]];
    [view addSubview:labelFullName];
    
    switch ([address.originalUserId integerValue]) {
        case 0:
            if (address.jobTitle) {
                CGSize jobTitleTextHeight=[AppManager frameForText:address.jobTitle sizeWithFont:[UIFont systemFontOfSize:17] constrainedToSize:CGSizeMake(250, 1000)];
                UILabel *lblJobTitle = [[UILabel alloc] initWithFrame:CGRectMake(40, labelFullName.frame.size.height+labelFullName.frame.origin.y, 250, ceil(jobTitleTextHeight.height))];
                [lblJobTitle setNumberOfLines:1];
                [lblJobTitle setBackgroundColor:[UIColor clearColor]];
                [lblJobTitle setTextColor:[UIColor blackColor]];
                [lblJobTitle setText:address.jobTitle];
                [lblJobTitle setFont:[UIFont systemFontOfSize:16]];
                [view addSubview:lblJobTitle];
                
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width,ceil(jobTitleTextHeight.height)+ceil(postTextHeight.height)+50)];
                
            }
            break;
            
        default:
            break;
    }
    
    //    if (_strJobTitle) {
    //        CGSize jobTitleTextHeight=[AppManager frameForText:_strJobTitle sizeWithFont:[UIFont fontWithName:ssFontHelveticaNeue size:17] constrainedToSize:CGSizeMake(250, 1000)];
    //        UILabel *lblJobTitle = [[UILabel alloc] initWithFrame:CGRectMake(40, labelFullName.frame.size.height+labelFullName.frame.origin.y, 250, ceil(jobTitleTextHeight.height))];
    //        [lblJobTitle setNumberOfLines:2];
    //        [lblJobTitle setBackgroundColor:[UIColor clearColor]];
    //        [lblJobTitle setTextColor:[UIColor blackColor]];
    //        [lblJobTitle setText:_strJobTitle];
    //        [lblJobTitle setFont:[UIFont systemFontOfSize:16]];
    //        [view addSubview:lblJobTitle];
    //
    //        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width,ceil(jobTitleTextHeight.height)+ceil(postTextHeight.height)+50)];
    //
    //    }
    
    
    self.tvSelectedContact.tableHeaderView=view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrContact count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    cellIdentifier = @"ContactsCell";
    UITableViewCell *cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    cell.textLabel.text=NSLocalizedString(@"Mobile", @"Mobile");
    cell.textLabel.textColor = [UIColor blueColor];
    cell.textLabel.font = [UIFont systemFontOfSize:12.0];
    cell.detailTextLabel.text = [self.arrContact objectAtIndex:indexPath.row];
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    if([self.selectedNo containsObject:[self.arrContact objectAtIndex:indexPath.row]])
    {
        cell.imageView.image = [UIImage imageNamed:@"checkSelected"];
    }
    else
    {
        cell.imageView.image = [UIImage imageNamed:@"check"];
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if([self.selectedNo containsObject:[self.arrContact objectAtIndex:indexPath.row]])
    {
        [self.selectedNo removeObject:[self.arrContact objectAtIndex:indexPath.row]];
    }
    else
    {
        [self.selectedNo addObject:[self.arrContact objectAtIndex:indexPath.row]];
    }
    [self.tvSelectedContact reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
