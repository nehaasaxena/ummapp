//
//  ShareContactViewController.h
//  UmmApp
//
//  Created by Neha Saxena on 12/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectedContactViewController.h"
@protocol ShareContactViewDelegate <NSObject>
- (void)selectedContact:(NSMutableDictionary *)dictContact;
@end
@interface ShareContactViewController : UIViewController<SelectedContactViewDelegate>
{
    __weak IBOutlet UITableView *contactsTableView;
    NSArray *arrAllContacts;
    NSMutableArray *sectionedListContent;
    NSArray *filteredResult;
    UIActivityIndicatorView *activityIndicatorView;
}
@property (nonatomic, weak) id<ShareContactViewDelegate> delegate;
- (IBAction)btnCancelClicked:(id)sender;
@end
