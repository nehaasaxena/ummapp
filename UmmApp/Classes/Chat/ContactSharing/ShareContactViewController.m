//
//  ShareContactViewController.m
//  UmmApp
//
//  Created by Neha Saxena on 12/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "ShareContactViewController.h"
@interface NSArray (SSArrayOfArrays)
- (id)objectAtIndexPath:(NSIndexPath *)indexPath;
@end
@implementation NSArray (SSArrayOfArrays)

- (id)objectAtIndexPath:(NSIndexPath *)indexPath
{
    return [[self objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]];
}
@end
@interface NSMutableArray (SSArrayOfArrays)
// If idx is beyond the bounds of the reciever, this method automatically extends the reciever to fit with empty subarrays.
- (void)addObject:(id)anObject toSubarrayAtIndex:(NSUInteger)idx;
@end
@implementation NSMutableArray (SSArrayOfArrays)

- (void)addObject:(id)anObject toSubarrayAtIndex:(NSUInteger)idx
{
    while ([self count] <= idx) {
        [self addObject:[NSMutableArray array]];
    }
    
    [[self objectAtIndex:idx] addObject:anObject];
}

@end
@interface ShareContactViewController ()

@end

@implementation ShareContactViewController
- (void)setNavigationBar
{
    self.title = NSLocalizedString(@"All Contacts", nil);
    UIButton *doneButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,60, 45)];
    [doneButton setTitle:NSLocalizedString(@"Cancel", @"Cancel")  forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor colorWithRed:16.0/255.0 green:170.0/255.0 blue:180.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [doneButton.titleLabel setFont:[UIFont boldSystemFontOfSize:17.0f]];
    [doneButton addTarget:self action:@selector(btnCancelClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar];
 //   self.navigationController.navigationBarHidden = YES;
    [contactsTableView setFrame:CGRectMake(0, 44, 320, [UIScreen mainScreen].bounds.size.height-44)];
    //    arrAllContacts = [[Database database] fetchDataFromDatabaseForEntity:@"AddressBookDB"];
    activityIndicatorView =[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [activityIndicatorView setFrame:CGRectMake(0, 0, 40, 40)];
    [activityIndicatorView setCenter:contactsTableView.center];
    [activityIndicatorView setHidesWhenStopped:YES];
    [self.view addSubview:activityIndicatorView];
    if(![Utils isIPhone5])
    {
        self.searchDisplayController.searchBar.frame = CGRectMake(0, 67, 320, 44);
    }
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    [self refreshArrayOfAllContacts];
}

-(void)refreshArrayOfAllContacts{
    arrAllContacts = [NSArray arrayWithArray:[[Database database] fetchDataFromDatabaseForEntityKeyName:@"phone"]];

    if ([arrAllContacts count]==0 && [AppManager sharedManager].isFetchingContacts) {
        [activityIndicatorView startAnimating];
        [contactsTableView setHidden:YES];
    }else{
        [activityIndicatorView stopAnimating];
        [contactsTableView setHidden:NO];
    }
    [self setListContent:arrAllContacts];
    [contactsTableView reloadData];
}
- (void)setListContent:(NSArray *)inListContent
{
    
    NSMutableArray *sections = [NSMutableArray array];
    UILocalizedIndexedCollation *collation = [UILocalizedIndexedCollation currentCollation];
    for (NSDictionary *product in inListContent)
    {
        NSInteger section = [collation sectionForObject:product collationStringSelector:@selector(fullName)];
        [sections addObject:product toSubarrayAtIndex:section];
    }
    
    NSInteger section = 0;
    for (section = 0; section < [sections count]; section++) {
        NSArray *sortedSubarray = [collation sortedArrayFromArray:[sections objectAtIndex:section]
                                          collationStringSelector:@selector(fullName)];
        [sections replaceObjectAtIndex:section withObject:sortedSubarray];
    }
    sectionedListContent = sections ;
}
#pragma mark - UITableView delegate & Datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == [[self searchDisplayController] searchResultsTableView])
    {
        return 1;
    }
    else
        return [sectionedListContent count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == [[self searchDisplayController] searchResultsTableView])
    {
        return [filteredResult count];
    }
    else
    {
        return [[sectionedListContent objectAtIndex:section] count];
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return nil;
    } else {
        return [[sectionedListContent objectAtIndex:section] count] ? [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section] : nil;
    }
    
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return nil;
    } else {
        return [[NSArray arrayWithObject:UITableViewIndexSearch] arrayByAddingObjectsFromArray:
                [[UILocalizedIndexedCollation currentCollation] sectionIndexTitles]];
    }
}
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return 0;
    } else {
        if (title == UITableViewIndexSearch) {
            [tableView scrollRectToVisible:self.searchDisplayController.searchBar.frame animated:NO];
            return -1;
        } else {
            return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index-1];
        }
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    cellIdentifier = @"ContactsCell";
    UITableViewCell *cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    AddressBookDB *address;
    if(tableView == [[self searchDisplayController] searchResultsTableView])
    {
        address=(AddressBookDB*)[filteredResult objectAtIndex:indexPath.row];
    }
    else
    {
        address=(AddressBookDB*)[sectionedListContent objectAtIndexPath:indexPath];
    }
    cell.textLabel.text=address.fullName;
    cell.detailTextLabel.text = address.userStatus;
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [[[self searchDisplayController] searchBar] resignFirstResponder];
    
    AddressBookDB *address;
    if(tableView == [[self searchDisplayController] searchResultsTableView])
    {
        address=(AddressBookDB*)[filteredResult objectAtIndex:indexPath.row];
    }
    else
    {
        address=(AddressBookDB*)[sectionedListContent objectAtIndexPath:indexPath];
    }
    
    SelectedContactViewController *selectedContact = [[SelectedContactViewController alloc] initWithNibName:@"SelectedContactViewController" bundle:nil];
    selectedContact.delegate = self;
    selectedContact.address = address;
    [self.navigationController pushViewController:selectedContact animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Search Display Controller Delegates
-(void) filterForSearchText:(NSString *) text scope:(NSString *) scope
{
    filteredResult = nil; // clearing filter array
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"SELF.fullName contains[c] %@",text]; // Creating filter condition
    filteredResult = [NSMutableArray arrayWithArray:[arrAllContacts filteredArrayUsingPredicate:filterPredicate]]; // filtering result
}
-(BOOL) searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterForSearchText:searchString scope:[[[[self searchDisplayController] searchBar] scopeButtonTitles] objectAtIndex:[[[self searchDisplayController] searchBar] selectedScopeButtonIndex] ]];
    
    return YES;
}

-(BOOL) searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    [self filterForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    
    return YES;
}
- (IBAction)btnCancelClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)selectedContactDetail:(NSMutableDictionary *)dictContact
{
    if([self.delegate respondsToSelector:@selector(selectedContact:)])
    {
        [self.delegate selectedContact:dictContact];
    }
}
@end
