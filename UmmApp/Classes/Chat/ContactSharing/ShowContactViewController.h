//
//  ShowContactViewController.h
//  UmmApp
//
//  Created by Neha Saxena on 13/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBookUI/ABNewPersonViewController.h>
@interface ShowContactViewController : UIViewController<ABNewPersonViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tvShowContact;
@property (strong, nonatomic) NSMutableArray *arrContact;
@property (strong, nonatomic) NSArray *contact;
@property (nonatomic, assign) BOOL isFromSelf;

@end
