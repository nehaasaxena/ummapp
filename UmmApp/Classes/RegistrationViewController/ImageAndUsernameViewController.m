//
//  ImageAndUsernameViewController.m
//  UmmApp
//
//  Created by Neha Saxena on 28/10/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "ImageAndUsernameViewController.h"
#import "NetworkService.h"
@interface ImageAndUsernameViewController ()

@end

@implementation ImageAndUsernameViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])
    {
        self.isComingFromSettings = NO;
    }
    return self;
}
- (void)setNavigationBar
{
    self.title = NSLocalizedString(@"Profile", @"Profile") ;
    if(!self.isComingFromSettings)
    {
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 45)];
        [backButton setTitle:NSLocalizedString(@"Next",@"Next") forState:UIControlStateNormal];
        [backButton setTitleColor:[UIColor colorWithRed:16.0/255.0 green:170.0/255.0 blue:180.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [backButton.titleLabel setFont:[UIFont boldSystemFontOfSize:17.0f]];
        [backButton addTarget:self action:@selector(callWebService) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationItem.rightBarButtonItem.enabled = NO;
        self.navigationItem.hidesBackButton = YES;
    }
    else
    {
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 11, 20)];
        [backButton setImage:[UIImage imageNamed:@"arrow1"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationItem.leftBarButtonItem.enabled = YES;
        self.navigationItem.hidesBackButton = YES;
        
        UIButton *doneBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
        [doneBtn setTitle:NSLocalizedString( @"Done",  @"Done") forState:UIControlStateNormal];
        [doneBtn setTitleColor:[UIColor colorWithRed:180.0/255.0 green:225.0/255.0 blue:225.0/255.0 alpha:1.0] forState:UIControlStateDisabled];
        [doneBtn setTitleColor:[UIColor colorWithRed:16.0/255.0 green:170.0/255.0 blue:180.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [doneBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:17.0f]];
        [doneBtn addTarget:self action:@selector(callWebService) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:doneBtn];
        self.navigationItem.rightBarButtonItem.enabled = NO;
        

    }
    
}
 - (void)setupView
{
    if ([self.tfUsername respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor whiteColor];
        self.tfUsername.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Enter username for notification", @"Enter username for notification") attributes:@{NSForegroundColorAttributeName: color}];
    }
    self.lblAddPicture.text = NSLocalizedString(@"Add Picture", @"Add Picture");
    self.lblMessage1.text = NSLocalizedString(@"Enter username and add an optional profile picture.", @"Enter username and add an optional profile picture.");
    self.lblAddPicture.adjustsFontSizeToFitWidth = TRUE;
    self.lblAddPicture.minimumFontSize = 10.0;

}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    self.btnImage.layer.cornerRadius = self.btnImage.frame.size.width/2;
    self.btnImage.clipsToBounds = YES;
    self.imgViewUser.layer.cornerRadius = self.btnImage.frame.size.width/2;
    self.imgViewUser.clipsToBounds = YES;

    [self setNavigationBar];
    svos = self.sv.contentOffset;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapped:)];
    [tap setNumberOfTapsRequired:1];
    [self.sv addGestureRecognizer:tap];
    if(self.isComingFromSettings)
    {
        [self.imgViewUser setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] valueForKey:kServerUrl],[[NSUserDefaults standardUserDefaults] valueForKey:kProfileImage]]] placeholderImage:[UIImage imageNamed:@"chatDefault"]];
        self.tfUsername.text = [Utils setTextWithNonLossy:[[NSUserDefaults standardUserDefaults] valueForKey:kFullName]];
    }

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - buttom clicked methods
- (void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnImageChangeClicked:(id)sender {
    [self.tfUsername resignFirstResponder];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Take Photo",@"Choose Existing Photo",nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    actionSheet.tag=1;
    [actionSheet showInView:self.view];
}
#pragma mark - Gesture recogniser method

- (void)tapped:(UITapGestureRecognizer *)gesture
{
    [self.tfUsername resignFirstResponder];
}

#pragma mark - call web service

- (void)setActivityIndicator:(BOOL)show
{
    if(show)
    {
        self.navigationItem.rightBarButtonItem.enabled = NO;
        self.tfUsername.enabled = NO;
        self.btnImage.enabled = NO;
        [AppManager startStatusbarActivityIndicatorWithUserInterfaceInteractionEnabled:YES];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
    else
    {
        self.navigationItem.rightBarButtonItem.enabled = YES;
        self.tfUsername.enabled = YES;
        self.btnImage.enabled = YES;
        [AppManager stopStatusbarActivityIndicator];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
    
}
- (void)callWebService
{
    if([Utils isInternetAvailable])
    {
        [self setActivityIndicator:YES];
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] valueForKey:kDeviceToken],kDeviceToken,kDeviceTypeiPhone,kDeviceType,[[NSUserDefaults standardUserDefaults] valueForKey:kSessionToken],kSessionToken,kUpdateProfile,kOption,[[NSUserDefaults standardUserDefaults] valueForKey:kUserId],kUserId,self.tfUsername.text,kFullName, nil];
        if(userImage !=nil)
        {
            CGSize size = CGSizeMake(320, 320);
            userImage = [UIImage scaleDownImage:userImage toSize:size];
            NSData *originalImageData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(userImage, 0.5)];
            [dict setObject:originalImageData forKey:kProfileImage];
        }
        NetworkService *obj = [NetworkService sharedInstance];
        [obj sendAsynchRequestUploadImageByPostToServer:kUserProfile dataToSend:dict delegate:self contentType:eAppJsonType andReqParaType:eJson header:NO boundry:@"111000111"];
    }
    else
    {
        [Utils showAlertView:kAlertTitle message:@"No internet available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
}
-(void) responseHandler :(id)inResponseDic andRequestIdentifier:(NSString *)inReqIdentifier
{
    [self setActivityIndicator:NO];
    if ([[inResponseDic valueForKey:SUCCESS] integerValue] ==1 ) {
        if([[inResponseDic objectForKey:kOption]isEqualToString:kUpdateProfile])
        {
            [AppManager saveDataToNSUserDefaults:inResponseDic];
            if(!self.isComingFromSettings)
            {
                [AppManager saveUserDatainUserDefault];
                [APP_DELEGATE showTabbarview];
            }
            else
            {
                                if([gCXMPPController isConnected])
                                {
                                    [self SetImageOnXMPP];
                                }

                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }else{
        [Utils showAlertView:kAlertTitle message:[inResponseDic valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
}
-(void) requestErrorHandler :(NSError *)inError andRequestIdentifier :(NSString *)inReqIdentifier
{
    [self setActivityIndicator:NO];
}
#pragma mark - image picker methods

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self ChoosePhotoOrVideo];
            break;
        case 1:
            [self ChooseExistingPhoto];
            break;
        default:
            break;
    }
}
-(void)ChoosePhotoOrVideo{    
    picker=[[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType =  UIImagePickerControllerSourceTypeCamera;
    picker.allowsEditing = NO;
    
    [self presentViewController:picker animated:YES completion:nil];
}
-(void)ChooseExistingPhoto{
    
    UIImagePickerController *imagePicker=[[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage, nil];
    
    imagePicker.allowsEditing = YES;
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}
- (void)SetImageOnXMPP
{
    NSData *originalImageData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(userImage, 1.0)];
    [gCXMPPController.xmppvCardTemp setPhoto:originalImageData];
    [[gCXMPPController xmppvCardTempModule] updateMyvCardTemp:gCXMPPController.xmppvCardTemp];

//
//    XMPPvCardTemp *myvCardTemp = [[gCXMPPController xmppvCardTempModule] myvCardTemp];
//    
//    if (myvCardTemp) {
//        
//        [myvCardTemp setPhoto:originalImageData];
//    }
}
#pragma mark - imagePicker delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker1 didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(self.isComingFromSettings)
    {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    NSDictionary *dic = [NSDictionary dictionaryWithDictionary: info];
    userImage = [dic objectForKey:UIImagePickerControllerEditedImage];
    userImage = [UIImage scaleDownOriginalImage:userImage];
    [self.imgViewUser setImage:userImage];
//    NSData *originalImageData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(image, 0.5)];
//
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - text field delegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(![Utils isIPhone5])
    {

    svos = self.sv.contentOffset;
    CGPoint pt;
    CGRect rc = [textField bounds];
    rc = [textField convertRect:rc toView:self.sv];
    pt = rc.origin;
    pt.x = 0;
        pt.y -=180 ;
    //        pt.y -= 70;
    [self.sv setContentOffset:pt animated:YES];
    }

}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if([textField.text length]+[string length] >0)
    {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    if(range.location==0&&string.length==0)
    {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }

    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(![Utils isIPhone5])
    {
        [self.sv setContentOffset:svos animated:YES];
    }
    [textField resignFirstResponder];
}

@end

