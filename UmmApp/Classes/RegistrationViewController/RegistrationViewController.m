//
//  RegistrationViewController.m
//  UmmApp
//
//  Created by Neha Saxena on 20/10/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "RegistrationViewController.h"
#import "PhoneVerifyViewController.h"
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import "ImageAndUsernameViewController.h"
@interface RegistrationViewController ()

@end

@implementation RegistrationViewController
#pragma mark - Navigation setup method
- (void)setNavigationBar
{
    self.title = NSLocalizedString(@"Phone Number", @"Phone Number");
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 45)];
    [backButton setTitle:NSLocalizedString(@"Next", @"Next")  forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor colorWithRed:16.0/255.0 green:170.0/255.0 blue:180.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont boldSystemFontOfSize:17.0f]];
    [backButton addTarget:self action:@selector(goToVerify) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.rightBarButtonItem.enabled = NO;
}
#pragma mark - Country code method
- (void)setCountryCode
{
   NSArray *arrCountryCode=[NSArray arrayWithArray:[AppManager getCountryCodeList]];

    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    // Get mobile country code
    NSString *mcc = [carrier isoCountryCode];
    if (mcc != nil){
        //        NSLog(@"Mobile Country Code (MCC): %@", mcc);
        
        NSInteger filteredIndex = [arrCountryCode indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop){
            NSString *ID =[NSString stringWithFormat:@"%@",[obj valueForKey:@"ISO"]];
            return [ID isEqualToString:mcc];
        }];
        if (filteredIndex != NSNotFound) {
            strCountryCode=[NSString stringWithFormat:@"+%@",[[arrCountryCode objectAtIndex:filteredIndex] valueForKey:@"Country Code"]];
            strCountryName = [NSString stringWithFormat:@"%@",[[arrCountryCode objectAtIndex:filteredIndex] valueForKey:@"Country"]];
            [self.lblCountryCode setText:[NSString stringWithFormat:@"%@ %@",strCountryCode,strCountryName]];
        }
        
        
    }
}
#pragma mark - gesture method

- (void)tapped:(UITapGestureRecognizer *)gesture
{
    [self.tfPhoneNo resignFirstResponder];
}
#pragma mark - View Life Cycle
- (void)viewSetup
{
    self.lblEnterYourPhone.text = NSLocalizedString(@"Enter your phone number below to get started!", @"Enter your phone number below to get started!");
    self.tfPhoneNo.placeholder = NSLocalizedString(@"your phone number", @"your phone number");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar];
    [self setCountryCode];
    [self viewSetup];
    if ([self.tfPhoneNo respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor whiteColor];
        self.tfPhoneNo.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"your phone number", @"your phone number") attributes:@{NSForegroundColorAttributeName: color}];
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapped:)];
    [tap setNumberOfTapsRequired:1];
    [self.view addGestureRecognizer:tap];

    [self.tfPhoneNo becomeFirstResponder];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - call web service
- (void)setActivityIndicator:(BOOL)show
{
    if(show)
    {
        self.navigationItem.rightBarButtonItem.enabled = NO;
        self.tfPhoneNo.enabled = NO;
        self.btnCountryCode.enabled = NO;
        [AppManager startStatusbarActivityIndicatorWithUserInterfaceInteractionEnabled:YES];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
    else
    {
        self.navigationItem.rightBarButtonItem.enabled = YES;
        self.tfPhoneNo.enabled = YES;
        self.btnCountryCode.enabled = YES;
        [AppManager stopStatusbarActivityIndicator];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }

}
- (void)callWebService
{
    [self setActivityIndicator:YES];
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] valueForKey:kDeviceToken],kDeviceToken,kDeviceTypeiPhone,kDeviceType,self.tfPhoneNo.text,kMobile,strCountryCode,kCountryCode,strCountryName,kCountryName,kSignup,kOption, nil];
    NetworkService *req = [NetworkService sharedInstance];
    [req sendAsynchRequestByPostToServer:kSignup dataToSend:dict  delegate: self contentType:eAppJsonType andReqParaType:eJson header:NO];
//    [req sendAsynchRequestByPostToServer:kSignup dataToSend:dict delegate:self contentType:eAppJsonType andReqParaType:eJson header:NO];
}
-(void) responseHandler :(id)inResponseDic andRequestIdentifier:(NSString *)inReqIdentifier
{
    [self setActivityIndicator:NO];
    if ([[inResponseDic valueForKey:SUCCESS] integerValue] ==1 ) {
        if([[inResponseDic objectForKey:kOption]isEqualToString:kSendOAuthSMS])
        {
            [self pushToVerifyPhoneScreen:[inResponseDic objectForKey:@"smsVerificationCode"]];
        }
        else
        {
            [AppManager saveDataToNSUserDefaults:inResponseDic];
            if([[inResponseDic objectForKey:kShowVerification] integerValue]==1)
            {
                [self setActivityIndicator:YES];
                NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] valueForKey:kDeviceToken],kDeviceToken,kDeviceTypeiPhone,kDeviceType,[[NSUserDefaults standardUserDefaults] valueForKey:kSessionToken],kSessionToken,kSendOAuthSMS,kOption,[[NSUserDefaults standardUserDefaults] valueForKey:kUserId],kUserId, nil];
                NetworkService *req = [NetworkService sharedInstance];
                [req sendAsynchRequestByPostToServer:kUsers dataToSend:dict  delegate: self contentType:eAppJsonType andReqParaType:eJson header:NO];
            }
            else
            {
                if([[inResponseDic objectForKey:@"fullName"] length]>0)
                {
                    [AppManager saveUserDatainUserDefault];
                    [[AppManager sharedManager] fetchAddressBookWithContactModel];
                    [APP_DELEGATE showTabbarview];
                }
                else
                {
                    [self pushToProfileDetailView];
                }
            }
        }
    }else{
        [Utils showAlertView:kAlertTitle message:[inResponseDic valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
}
-(void) requestErrorHandler :(NSError *)inError andRequestIdentifier :(NSString *)inReqIdentifier
{
    [self setActivityIndicator:NO];
}
#pragma mark - button clicked methods

- (IBAction)btnCountryCodeClicked:(id)sender {
    CountryListVC *list=[[CountryListVC alloc] initWithNibName:@"CountryListVC" bundle:nil];
    list.delegate=self;
    [self.navigationController pushViewController:list animated:YES];
}
-(void)countrySelected:(Country*)countrySDetail
{
    strCountryCode=[NSString stringWithFormat:@"+%@",countrySDetail.code];
    strCountryName = [NSString stringWithFormat:@"%@",countrySDetail.name];
    [self.lblCountryCode setText:[NSString stringWithFormat:@"%@ %@",strCountryCode,strCountryName]];
}

- (void)goToVerify
{
    NSInteger tot= [self.tfPhoneNo.text length]+[strCountryCode length]-1;
    if(tot>14)
    {
        [Utils showAlertView:kAlertTitle message:[NSString stringWithFormat:@"%@%@ is too long for %@",strCountryCode,self.tfPhoneNo.text,strCountryName] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    else if(tot < 10)
    {
        [Utils showAlertView:kAlertTitle message:[NSString stringWithFormat:@"%@%@ is too short for %@",strCountryCode,self.tfPhoneNo.text,strCountryName] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    else
    {
        [self callWebService];
    }
}
- (void)pushToProfileDetailView
{
    ImageAndUsernameViewController *imAndUn = [[ImageAndUsernameViewController alloc] initWithNibName:@"ImageAndUsernameViewController" bundle:nil];
    [self.navigationController pushViewController:imAndUn animated:YES];
}
- (void)pushToVerifyPhoneScreen:(NSString *)code
{
    PhoneVerifyViewController *phoneVerify = [[PhoneVerifyViewController alloc] initWithNibName:@"PhoneVerifyViewController" bundle:nil];
    phoneVerify.SmsCode = code;
    [self.navigationController pushViewController:phoneVerify animated:YES];
}
#pragma mark - text field delegates
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if([textField.text length]+[string length] >0)
    {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    if(range.location==0&&string.length==0)
    {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
    return YES;
}

@end
