//
//  PhoneVerifyViewController.m
//  UmmApp
//
//  Created by Neha Saxena on 28/10/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "PhoneVerifyViewController.h"
#import "ImageAndUsernameViewController.h"
@interface PhoneVerifyViewController ()

@end

@implementation PhoneVerifyViewController
@synthesize SmsCode;
- (void)setNavigationBar
{
//    CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 320, 44);
//    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
//    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
//    _headerTitleSubtitleView.autoresizesSubviews = NO;
//    
//    CGRect titleFrame = CGRectMake(60,0, 200, 44);
//    UILabel *titleView = [[UILabel alloc] initWithFrame:titleFrame];
//    titleView.backgroundColor = [UIColor clearColor];
//    titleView.font = [UIFont boldSystemFontOfSize:18];
//    titleView.textAlignment = NSTextAlignmentCenter;
//    titleView.textColor = [UIColor blackColor];
//    titleView.text = @"Verify";
//    titleView.adjustsFontSizeToFitWidth = YES;
//    [_headerTitleSubtitleView addSubview:titleView];
//    
//    
//    self.navigationItem.titleView = _headerTitleSubtitleView;
    self.title = NSLocalizedString(@"Verify", @"Verify");
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 45)];
    [backButton setTitle:NSLocalizedString(@"Next",@"Next") forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor colorWithRed:16.0/255.0 green:170.0/255.0 blue:180.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [backButton.titleLabel setFont:[UIFont boldSystemFontOfSize:17.0f]];
    [backButton addTarget:self action:@selector(Verify) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.hidesBackButton = YES;
//    self.navigationItem.rightBarButtonItem.enabled = NO;
}
- (void)setupView
{
    self.btnResend.titleLabel.adjustsFontSizeToFitWidth = TRUE;
    self.btnResend.titleLabel.minimumFontSize = 10.0;
    [self.btnResend setTitle:NSLocalizedString(@"RESEND PIN", @"RESEND PIN") forState:UIControlStateNormal];
    self.lblMessage1.text = NSLocalizedString(@"Did you get an SMS PIN? Enter it below.", @"Did you get an SMS PIN? Enter it below.");
    self.lblMessage2.text = NSLocalizedString(@"Didn't get the PIN?", @"Didn't get the PIN?");

}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar];
    [self setupView];
    if ([self.tfVerificationCode respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor whiteColor];
        self.tfVerificationCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Pin", @"Pin") attributes:@{NSForegroundColorAttributeName: color}];
    }

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapped:)];
    [tap setNumberOfTapsRequired:1];
    [self.view addGestureRecognizer:tap];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - call web service

- (void)setActivityIndicator:(BOOL)show
{
    if(show)
    {
        self.btnResend.enabled = NO;
        self.tfVerificationCode.enabled = NO;
        [AppManager startStatusbarActivityIndicatorWithUserInterfaceInteractionEnabled:YES];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
    else
    {
        self.btnResend.enabled = YES;
        self.tfVerificationCode.enabled = YES;
        [AppManager stopStatusbarActivityIndicator];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
    
}
- (void)callWebService:(NSString*)serviceType
{
    [self setActivityIndicator:YES];
    if([serviceType isEqualToString:@"Resend"])
    {
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] valueForKey:kDeviceToken],kDeviceToken,kDeviceTypeiPhone,kDeviceType,[[NSUserDefaults standardUserDefaults] valueForKey:kSessionToken],kSessionToken,kSendOAuthSMS,kOption,[[NSUserDefaults standardUserDefaults] valueForKey:kUserId],kUserId, nil];
        NetworkService *req = [NetworkService sharedInstance];
        [req sendAsynchRequestByPostToServer:kUsers dataToSend:dict  delegate: self contentType:eAppJsonType andReqParaType:eJson header:NO];
    }
    else
    {
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] valueForKey:kDeviceToken],kDeviceToken,kDeviceTypeiPhone,kDeviceType,[[NSUserDefaults standardUserDefaults] valueForKey:kSessionToken],kSessionToken,kCheckCode,kOption,self.tfVerificationCode.text,kCode,[[NSUserDefaults standardUserDefaults] valueForKey:kUserId],kUserId, nil];
        NetworkService *req = [NetworkService sharedInstance];
        [req sendAsynchRequestByPostToServer:kUsers dataToSend:dict  delegate: self contentType:eAppJsonType andReqParaType:eJson header:NO];
    }
}
- (void)stop
{
    [self setActivityIndicator:NO];
}
-(void) responseHandler :(id)inResponseDic andRequestIdentifier:(NSString *)inReqIdentifier
{
    [self setActivityIndicator:NO];
    if ([[inResponseDic valueForKey:SUCCESS] integerValue] ==1 ) {
        if([[inResponseDic valueForKey:kOption]isEqualToString:kSendOAuthSMS])
        {
            NSLog(@"%@",inResponseDic);
            [Utils showAlertView:kAlertTitle message:@"Your verification code SMS has been resend on your number." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        }
        else
        {
            [AppManager saveDataToNSUserDefaults:inResponseDic];
            if([[inResponseDic objectForKey:kVerified] integerValue]==1)
            {
                if([[inResponseDic objectForKey:kFullName] length]>0)
                {
                    [AppManager saveUserDatainUserDefault];
                    [APP_DELEGATE showTabbarview];
                }
                else
                {
                    [self pushToProfileDetailView];
                }
                [[AppManager sharedManager] fetchAddressBookWithContactModel];
            }
        }
    }else{
        [Utils showAlertView:kAlertTitle message:[inResponseDic valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
}
-(void) requestErrorHandler :(NSError *)inError andRequestIdentifier :(NSString *)inReqIdentifier
{
    [self setActivityIndicator:NO];
}

#pragma mark - button clicked methods
- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnResendClicked:(id)sender {
    self.tfVerificationCode.text = @"";
    [self callWebService:@"Resend"];
}

- (void)pushToProfileDetailView
{
    ImageAndUsernameViewController *imAndUn = [[ImageAndUsernameViewController alloc] initWithNibName:@"ImageAndUsernameViewController" bundle:nil];
    [self.navigationController pushViewController:imAndUn animated:YES];
}

#pragma mark - Gesture recogniser method

- (void)tapped:(UITapGestureRecognizer *)gesture
{
    [self.tfVerificationCode resignFirstResponder];
}

- (void)Verify
{
    [self performSelector:@selector(callWebService:) withObject:@"Verify" afterDelay:0.2];
}
#pragma mark - text field delegates

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
   if([string length]>0)
   {
        if([textField.text length]+[string length] <=6)
        {
            return YES;
        }
       else
       {
           return NO;
       }
   }
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.tfVerificationCode resignFirstResponder];
    return YES;
}
@end
