//
//  ImageAndUsernameViewController.h
//  UmmApp
//
//  Created by Neha Saxena on 28/10/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageAndUsernameViewController : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UITextFieldDelegate>
{
    UIImage *userImage;
    UIImagePickerController *picker;
    CGPoint svos;
}
@property (strong, nonatomic) IBOutlet UIButton *btnImage;
@property (strong, nonatomic) IBOutlet UILabel *lblAddPicture;
@property (strong, nonatomic) IBOutlet UILabel *lblMessage1;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewUser;
@property (strong, nonatomic) IBOutlet UITextField *tfUsername;
@property (strong, nonatomic) IBOutlet UIScrollView *sv;
@property (nonatomic, assign) BOOL isComingFromSettings;
- (IBAction)btnImageChangeClicked:(id)sender;
@end
