//
//  CountryListVC.h
//  SocialParty
//
//  Created by Pankaj on 3/2/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Country.h"

@protocol CountryListDelegate <NSObject>
-(void)countrySelected:(Country*)countrySDetail;
@end

@interface CountryListVC : UIViewController{
    id<CountryListDelegate> delegate;
    NSMutableArray *arrCountryDetail;
    IBOutlet UITableView *countryTableView;
    NSMutableArray *sectionedListContent;
    IBOutlet UILabel *lblNavTitle;
}
- (IBAction)btnBackClicked:(id)sender;
@property (nonatomic, strong) id<CountryListDelegate> delegate;
@end
