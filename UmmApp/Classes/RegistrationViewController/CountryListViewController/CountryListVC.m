//
//  CountryListVC.m
//  SocialParty
//
//  Created by Pankaj on 3/2/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "CountryListVC.h"

@interface NSArray (SSArrayOfArrays)
- (id)objectAtIndexPath:(NSIndexPath *)indexPath;
@end

@implementation NSArray (SSArrayOfArrays)

- (id)objectAtIndexPath:(NSIndexPath *)indexPath
{
    return [[self objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]];
}

@end

@interface NSMutableArray (SSArrayOfArrays)
// If idx is beyond the bounds of the reciever, this method automatically extends the reciever to fit with empty subarrays.
- (void)addObject:(id)anObject toSubarrayAtIndex:(NSUInteger)idx;
@end

@implementation NSMutableArray (SSArrayOfArrays)

- (void)addObject:(id)anObject toSubarrayAtIndex:(NSUInteger)idx
{
    while ([self count] <= idx) {
        [self addObject:[NSMutableArray array]];
    }
    
    [[self objectAtIndex:idx] addObject:anObject];
}

@end
@interface CountryListVC ()

@end

@implementation CountryListVC
@synthesize delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    countryTableView.tableHeaderView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 0.01f)];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    NSArray *arrCont=[[[NSMutableArray arrayWithArray:[AppManager getCountryCodeList]] reverseObjectEnumerator] allObjects];
    
    arrCountryDetail=[[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in arrCont) {
        Country *country=[[Country alloc] init];
        country.name=[dict valueForKey:@"Country"];
        country.ISO=[dict valueForKey:@"ISO"];
        country.mcc=[[dict valueForKey:@"MCC"] integerValue];
        country.code=[dict valueForKey:@"Country Code"];

        [arrCountryDetail addObject:country];
    }
    
    [self setListContent:arrCountryDetail];
    
    if (![Utils isIPhone5]) {
        [countryTableView setFrame:CGRectMake(0, 65, 320, 505)];
    }

//    lblNavTitle.font = [UIFont fontWithName:@"" size:<#(CGFloat)#>];
    lblNavTitle.textColor = [UIColor blackColor];
    lblNavTitle.text =NSLocalizedString(@"Select Country", @"Select Country") ;
    
    countryTableView.backgroundColor = [UIColor clearColor];
    
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
}


- (void)setListContent:(NSArray *)inListContent
{
    
    NSMutableArray *sections = [NSMutableArray array];
    UILocalizedIndexedCollation *collation = [UILocalizedIndexedCollation currentCollation];
    for (NSDictionary *product in inListContent)
    {
        NSInteger section = [collation sectionForObject:product collationStringSelector:@selector(name)];
        [sections addObject:product toSubarrayAtIndex:section];
    }
    
    NSInteger section = 0;
    for (section = 0; section < [sections count]; section++) {
        NSArray *sortedSubarray = [collation sortedArrayFromArray:[sections objectAtIndex:section]
                                          collationStringSelector:@selector(name)];
        [sections replaceObjectAtIndex:section withObject:sortedSubarray];
    }
    sectionedListContent = sections ;
}


#pragma mark -
#pragma mark - UITableView Delegates & DataSource Methods
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return 20;
//}
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 20)];
//    //    [view setBackgroundColor:[UIColor colorWithRed:136/255.0 green:137/255.0 blue:140/255.0 alpha:1.0]];
//    [view setBackgroundColor:[UIColor whiteColor]];
//    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 20)];
//    label.center=CGPointMake(110, view.center.y);
//    label.textColor=[UIColor colorWithRed:138/255.0 green:140/255.0 blue:142/255.0 alpha:1.0];
//    label.text=@"Select Privacy";
//    label.font=[UIFont fontWithName:label.font.fontName size:13];
//    [view addSubview:label];
//    return view;
//}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//    return 1;
    return [sectionedListContent count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return [arrCountryDetail count];
    return [[sectionedListContent objectAtIndex:section] count];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if (tableView == self.searchDisplayController.searchResultsTableView) {
        return nil;
    } else {
        return [[sectionedListContent objectAtIndex:section] count] ? [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section] : nil;
    }
    
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return nil;
    } else {
        return [[NSArray arrayWithObject:UITableViewIndexSearch] arrayByAddingObjectsFromArray:
                [[UILocalizedIndexedCollation currentCollation] sectionIndexTitles]];
    }
}
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return 0;
    } else {
        if (title == UITableViewIndexSearch) {
            [tableView scrollRectToVisible:self.searchDisplayController.searchBar.frame animated:NO];
            return -1;
        } else {
            return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index-1];
        }
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    cellIdentifier = @"CellPhotosOptions";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        
        UILabel *labelCntryName=[[UILabel alloc] initWithFrame:CGRectMake(10, 7, 250, 30)];
        labelCntryName.tag=10;
        [cell.contentView addSubview:labelCntryName];
        
        UILabel *labelCntryCode=[[UILabel alloc] initWithFrame:CGRectMake(260, 7, 50, 30)];
        labelCntryCode.tag=11;
        [labelCntryCode setTextColor:[UIColor grayColor]];
        [labelCntryCode setBackgroundColor:[UIColor clearColor]];
        [labelCntryCode setFont:[UIFont systemFontOfSize:13]];
        [cell.contentView addSubview:labelCntryCode];
    }
    
    UILabel *labelCntryName=(UILabel *)[cell.contentView viewWithTag:10];
    UILabel *labelCntryCode=(UILabel *)[cell.contentView viewWithTag:11];
    Country *country=(Country*)  [sectionedListContent objectAtIndexPath:indexPath];
    labelCntryName.text=country.name;
    labelCntryCode.text=[NSString stringWithFormat:@"+%@",country.code];;
    
//    labelCntryName.text=[[arrCountryDetail objectAtIndex:indexPath.row] valueForKey:@"Country"];
//    labelCntryCode.text=[NSString stringWithFormat:@"+%@",[[arrCountryDetail objectAtIndex:indexPath.row] valueForKey:@"Country Code"]];;
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     Country *country=(Country*)  [sectionedListContent objectAtIndexPath:indexPath];
    if ([delegate respondsToSelector:@selector(countrySelected:)])[delegate countrySelected:country];
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
