//
//  Country.h
//  SocialParty
//
//  Created by Pankaj on 3/6/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Country : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *ISO;
@property (nonatomic) NSInteger mcc;
@property (nonatomic, strong) NSString *code;

@end
