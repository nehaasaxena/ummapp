//
//  PhoneVerifyViewController.h
//  UmmApp
//
//  Created by Neha Saxena on 28/10/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkService.h"
@interface PhoneVerifyViewController : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *tfVerificationCode;
@property (strong, nonatomic) IBOutlet UIButton *btnResend;
@property (strong, nonatomic) IBOutlet UILabel *lblMessage1;
@property (strong, nonatomic) IBOutlet UILabel *lblMessage2;
@property (nonatomic, strong) NSString *SmsCode;
- (IBAction)btnResendClicked:(id)sender;

@end
