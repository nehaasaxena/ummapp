//
//  RegistrationViewController.h
//  UmmApp
//
//  Created by Neha Saxena on 20/10/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CountryListVC.h"
#import "NetworkService.h"


@interface RegistrationViewController : UIViewController<CountryListDelegate,UITextFieldDelegate>
{
    NSString *strCountryCode;
    NSString *strCountryName;
    UIButton *face;
}
@property (strong, nonatomic) IBOutlet UIButton *btnCountryCode;
@property (strong, nonatomic) IBOutlet UITextField *tfPhoneNo;
@property (strong, nonatomic) IBOutlet UILabel *lblCountryCode;
@property (strong, nonatomic) IBOutlet UILabel *lblEnterYourPhone;

- (IBAction)btnCountryCodeClicked:(id)sender;

@end
