//
//  MoreViewController.h
//  UmmApp
//
//  Created by Neha Saxena on 20/10/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "ContactListViewController.h"

@interface MoreViewController : UIViewController<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,UIActionSheetDelegate,ContactListViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tblViewMore;

@end
