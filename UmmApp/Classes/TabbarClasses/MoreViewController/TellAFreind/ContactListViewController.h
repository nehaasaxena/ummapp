//
//  ContactListViewController.h
//  UmmApp
//
//  Created by Neha Saxena on 06/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ContactListViewDelegate <NSObject>
- (void)selectedNumbers:(NSArray *)arrNumbers;
- (void)selectedEmail:(NSArray *)arrEmail;
@end

@interface ContactListViewController : UIViewController<UIGestureRecognizerDelegate>
{
    __weak IBOutlet UITableView *contactsTableView;
    NSArray *arrAllContacts;
    NSMutableArray *sectionedListContent;
    NSArray *filteredResult;
    UIActivityIndicatorView *activityIndicatorView;
}
@property (nonatomic, weak) id<ContactListViewDelegate> delegate;
@property (nonatomic, strong) NSMutableArray *selectedContacts;
@property (nonatomic, assign) BOOL showEmail;
- (IBAction)btnBackClicked:(id)sender;
-(void)refreshArrayOfAllContacts;
- (IBAction)btnDoneClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnCancel;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnDone;

@end
