//
//  ContactListViewController.m
//  UmmApp
//
//  Created by Neha Saxena on 06/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "ContactListViewController.h"


@interface NSArray (SSArrayOfArrays)
- (id)objectAtIndexPath:(NSIndexPath *)indexPath;
@end
@implementation NSArray (SSArrayOfArrays)

- (id)objectAtIndexPath:(NSIndexPath *)indexPath
{
    return [[self objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]];
}
@end
@interface NSMutableArray (SSArrayOfArrays)
// If idx is beyond the bounds of the reciever, this method automatically extends the reciever to fit with empty subarrays.
- (void)addObject:(id)anObject toSubarrayAtIndex:(NSUInteger)idx;
@end
@implementation NSMutableArray (SSArrayOfArrays)

- (void)addObject:(id)anObject toSubarrayAtIndex:(NSUInteger)idx
{
    while ([self count] <= idx) {
        [self addObject:[NSMutableArray array]];
    }
    
    [[self objectAtIndex:idx] addObject:anObject];
}

@end
@interface ContactListViewController ()
@end
@implementation ContactListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.selectedContacts = [[NSMutableArray alloc] init];
    [contactsTableView setFrame:CGRectMake(0, 110, 320, [UIScreen mainScreen].bounds.size.height-44-64-50)];
    //    arrAllContacts = [[Database database] fetchDataFromDatabaseForEntity:@"AddressBookDB"];
    activityIndicatorView =[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [activityIndicatorView setFrame:CGRectMake(0, 0, 40, 40)];
    [activityIndicatorView setCenter:contactsTableView.center];
    [activityIndicatorView setHidesWhenStopped:YES];
    [self.view addSubview:activityIndicatorView];

    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    [self refreshArrayOfAllContacts];
}
#pragma Longpress Backbutton Gesture Method




-(void)refreshArrayOfAllContacts{
    if(self.showEmail)
    {
        arrAllContacts = [NSArray arrayWithArray:[[Database database] fetchDataFromDatabaseForEntityKeyName:@"email"]];
    }
    else
    {
        arrAllContacts = [NSArray arrayWithArray:[[Database database] fetchDataFromDatabaseForEntityKeyName:@"phone"]];
    }
    if ([arrAllContacts count]==0 && [AppManager sharedManager].isFetchingContacts) {
        [activityIndicatorView startAnimating];
        [contactsTableView setHidden:YES];
    }else{
        [activityIndicatorView stopAnimating];
        [contactsTableView setHidden:NO];
    }
    
    
    [self setListContent:arrAllContacts];
    [contactsTableView reloadData];
}

- (IBAction)btnDoneClicked:(id)sender {
        NSMutableArray *array = nil;
    if(self.showEmail)
    {
        array = [self.selectedContacts valueForKey:@"email"];
    }
    else
    {
        array = [self.selectedContacts valueForKey:@"phone"];
    }
    NSMutableArray *selectedArray = [[NSMutableArray alloc]init];
   
    for(NSString *string in array)
    {
        id data=[NSJSONSerialization JSONObjectWithData:[string dataUsingEncoding:NSUTF8StringEncoding]
                                                options:0 error:NULL];
        for(NSDictionary *dict in data)
        {
            [selectedArray addObject:[dict objectForKey:@"number"]];
        }
    }
    if(self.showEmail)
    {
        if([self.delegate respondsToSelector:@selector(selectedEmail:)])
        {
            [self.delegate selectedEmail:selectedArray];
        }
    }
    else
    {
        if([self.delegate respondsToSelector:@selector(selectedNumbers:)])
        {
            [self.delegate selectedNumbers:selectedArray];
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setListContent:(NSArray *)inListContent
{
    
    NSMutableArray *sections = [NSMutableArray array];
    UILocalizedIndexedCollation *collation = [UILocalizedIndexedCollation currentCollation];
    for (NSDictionary *product in inListContent)
    {
        NSInteger section = [collation sectionForObject:product collationStringSelector:@selector(fullName)];
        [sections addObject:product toSubarrayAtIndex:section];
    }
    
    NSInteger section = 0;
    for (section = 0; section < [sections count]; section++) {
        NSArray *sortedSubarray = [collation sortedArrayFromArray:[sections objectAtIndex:section]
                                          collationStringSelector:@selector(fullName)];
        [sections replaceObjectAtIndex:section withObject:sortedSubarray];
    }
    sectionedListContent = sections ;
}
#pragma mark - UITableView delegate & Datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == [[self searchDisplayController] searchResultsTableView])
    {
        return 1;
    }
    else
        return [sectionedListContent count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == [[self searchDisplayController] searchResultsTableView])
    {
        return [filteredResult count];
    }
    else
    {
        return [[sectionedListContent objectAtIndex:section] count];
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return nil;
    } else {
        return [[sectionedListContent objectAtIndex:section] count] ? [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section] : nil;
    }
    
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return nil;
    } else {
        return [[NSArray arrayWithObject:UITableViewIndexSearch] arrayByAddingObjectsFromArray:
                [[UILocalizedIndexedCollation currentCollation] sectionIndexTitles]];
    }
}
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return 0;
    } else {
        if (title == UITableViewIndexSearch) {
            [tableView scrollRectToVisible:self.searchDisplayController.searchBar.frame animated:NO];
            return -1;
        } else {
            return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index-1];
        }
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    cellIdentifier = @"ContactsCell";
    UITableViewCell *cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    AddressBookDB *address;
    if(tableView == [[self searchDisplayController] searchResultsTableView])
    {
        address=(AddressBookDB*)[filteredResult objectAtIndex:indexPath.row];
    }
    else
    {
        address=(AddressBookDB*)[sectionedListContent objectAtIndexPath:indexPath];
    }
    cell.textLabel.text=address.fullName;
    if([self.selectedContacts containsObject:address])
        cell.imageView.image = [UIImage imageNamed:@"checkSelected"];
    else
        cell.imageView.image = [UIImage imageNamed:@"check"];

    cell.detailTextLabel.text = address.userStatus;
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [[[self searchDisplayController] searchBar] resignFirstResponder];
    
    AddressBookDB *address;
    if(tableView == [[self searchDisplayController] searchResultsTableView])
    {
        address=(AddressBookDB*)[filteredResult objectAtIndex:indexPath.row];
    }
    else
    {
        address=(AddressBookDB*)[sectionedListContent objectAtIndexPath:indexPath];
    }
    
    if([self.selectedContacts containsObject:address])
    {
        [self.selectedContacts removeObject:address];
    }
    else
    {
        [self.selectedContacts addObject:address];
    }
    [tableView reloadData];
}



#pragma mark - Search Display Controller Delegates
-(void) filterForSearchText:(NSString *) text scope:(NSString *) scope
{
    filteredResult = nil; // clearing filter array
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"SELF.fullName contains[c] %@",text]; // Creating filter condition
    filteredResult = [NSMutableArray arrayWithArray:[arrAllContacts filteredArrayUsingPredicate:filterPredicate]]; // filtering result
}
-(BOOL) searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterForSearchText:searchString scope:[[[[self searchDisplayController] searchBar] scopeButtonTitles] objectAtIndex:[[[self searchDisplayController] searchBar] selectedScopeButtonIndex] ]];
    
    return YES;
}

-(BOOL) searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    [self filterForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    
    return YES;
}
- (IBAction)btnBackClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
