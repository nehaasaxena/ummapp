//
//  ChatSettingsViewController.m
//  UmmApp
//
//  Created by Neha Saxena on 03/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "ChatSettingsViewController.h"
#import "chatWallpaperViewController.h"

@interface ChatSettingsViewController ()
{
    NSMutableDictionary *dict;
    NSString *saveIncomingMedia;
    NSString *selectedWallPaper;
    NSString *autoDownload;
}
@end

@implementation ChatSettingsViewController
- (void)setNavigationBar
{
//    CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 150, 44);
//    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
//    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
//    _headerTitleSubtitleView.autoresizesSubviews = NO;
//    
//    CGRect titleFrame = CGRectMake(0,0, 150, 44);
//    UILabel *titleView = [[UILabel alloc] initWithFrame:titleFrame];
//    titleView.backgroundColor = [UIColor clearColor];
//    titleView.font = [UIFont fontWithName:ssFontBrushScriptStd size:20];
//    titleView.textAlignment = NSTextAlignmentCenter;
//    titleView.textColor = [UIColor whiteColor];
//    titleView.text = @"Chat Settings";
//    titleView.adjustsFontSizeToFitWidth = YES;
//    [_headerTitleSubtitleView addSubview:titleView];
//    self.navigationItem.titleView = _headerTitleSubtitleView;
    self.title =NSLocalizedString(@"Chat Settings",@"Chat Settings") ;
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 11, 20)];
    [backButton setImage:[UIImage imageNamed:@"arrow1"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem.enabled = YES;
    self.navigationItem.hidesBackButton = YES;
    
    UIButton *doneButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
    [doneButton setTitle:NSLocalizedString(@"Done", @"Done")  forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor colorWithRed:16.0/255.0 green:170.0/255.0 blue:180.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [doneButton.titleLabel setFont:[UIFont boldSystemFontOfSize:17.0f]];
    [doneButton addTarget:self action:@selector(doneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
    self.navigationItem.rightBarButtonItem.enabled = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar];
    dict = [[NSMutableDictionary alloc] init];
    dict = [[[NSUserDefaults standardUserDefaults] valueForKey:kAPP_SETTINGS] objectForKey:kCHAT_SETTINGS];
    saveIncomingMedia = [dict objectForKey:kCHAT_SETTINGS_SAVE_INCOMING_MEDIA];
    selectedWallPaper = [dict objectForKey:kCHAT_SETTINGS_CHAT_WALLPAPER];
    autoDownload = [dict objectForKey:kCHAT_SETTINGS_AUTODOWNLOAD];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - button clicked methods
- (IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)doneButtonClicked:(id)sender
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:selectedWallPaper,kCHAT_SETTINGS_CHAT_WALLPAPER,autoDownload,kCHAT_SETTINGS_AUTODOWNLOAD,saveIncomingMedia,kCHAT_SETTINGS_SAVE_INCOMING_MEDIA, nil];
    [AppManager updateAppSettings:[NSMutableDictionary dictionaryWithDictionary:dictionary] withKey:kCHAT_SETTINGS];
    [self.navigationController popViewControllerAnimated:YES];

}
#pragma mark - switch methods
- (IBAction)saveIncomingMediaSwitch:(id)sender
{
    self.navigationItem.rightBarButtonItem.enabled = YES;
    UISwitch *swi = (UISwitch *)sender;
    if ([swi isOn]) {
        saveIncomingMedia = @"1";
    }
    else
    {
        saveIncomingMedia = @"0";
    }
}
- (IBAction)autoDownloadMedia:(id)sender
{
    self.navigationItem.rightBarButtonItem.enabled = YES;
    UISwitch *swi = (UISwitch *)sender;
    if ([swi isOn]) {
        autoDownload = @"1";
    }
    else
    {
        autoDownload = @"0";
    }
}
#pragma mark - tableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return 2;
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    
    
    NSArray* nib;
    switch (indexPath.section)
    {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    if (cell==nil) {
                        
                        nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
                        cell = [nib objectAtIndex:0];
                        
                    }
                    UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:100];
                    lblSettingNameSection2.textColor = [UIColor darkGrayColor];
                    lblSettingNameSection2.text =NSLocalizedString(@"Chat Wallpaper", @"Chat Wallpaper") ;
                }
                    break;
            }
        }
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    if (cell==nil) {
                        
                        nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
                        cell = [nib objectAtIndex:1];
                        
                    }
                    UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:200];
                    lblSettingNameSection2.textColor = [UIColor darkGrayColor];
                    lblSettingNameSection2.text = NSLocalizedString(@"Auto-Download Media", @"Auto-Download Media") ;
                    
                    UISwitch *autoSwitch = (UISwitch *)[cell.contentView viewWithTag:201];
                    [autoSwitch addTarget:self action:@selector(autoDownloadMedia:) forControlEvents:UIControlEventTouchUpInside];
                    if([autoDownload integerValue]==1)
                    {
                        [autoSwitch setOn:YES];
                    }
                    else
                    {
                        [autoSwitch setOn:NO];
                    }
                }
                    break;
                case 1:
                {
                    if (cell==nil) {
                        
                        nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
                        cell = [nib objectAtIndex:1];
                        
                    }
                    UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:200];
                    lblSettingNameSection2.textColor = [UIColor darkGrayColor];
                    lblSettingNameSection2.text = NSLocalizedString(@"Save Incoming Media", @"Save Incoming Media") ;
                    
                    UISwitch *saveSwitch = (UISwitch *)[cell.contentView viewWithTag:201];
                    [saveSwitch addTarget:self action:@selector(saveIncomingMediaSwitch:) forControlEvents:UIControlEventTouchUpInside];
                    if([saveIncomingMedia integerValue]==1)
                    {
                        [saveSwitch setOn:YES];
                    }
                    else
                    {
                        [saveSwitch setOn:NO];
                    }
                }
                    break;
            }
        }
            break;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    self.navigationItem.rightBarButtonItem.enabled = YES;
    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    chatWallpaperViewController *chatWallpaper = [[chatWallpaperViewController alloc] initWithNibName:@"chatWallpaperViewController" bundle:nil];
                    chatWallpaper.delegate = self;
                    chatWallpaper.selectedWallpaperLink = selectedWallPaper;
                    [self.navigationController pushViewController:chatWallpaper animated:YES];
                }
                    break;
            }
        }
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    UISwitch *saveSwitch = (UISwitch *)[cell.contentView viewWithTag:201];
                    if([autoDownload integerValue]==1)
                    {
                        autoDownload = @"0";
                        [saveSwitch setOn:NO];
                    }
                    else
                    {
                        autoDownload = @"1";
                        [saveSwitch setOn:YES];
                    }
                }
                    break;
                case 1:
                {
                    UISwitch *saveSwitch = (UISwitch *)[cell.contentView viewWithTag:201];

                    if([saveIncomingMedia integerValue]==1)
                    {
                        saveIncomingMedia = @"0";
                        [saveSwitch setOn:NO];
                    }
                    else
                    {
                        saveIncomingMedia = @"1";
                        [saveSwitch setOn:YES];
                    }
                }
                    break;
            }
        }
            break;
    }
}

#pragma mark - wallpaper delegate method
- (void)setSelectedWallpaper:(NSString *)wallpaper
{
    selectedWallPaper = wallpaper;
}

@end
