//
//  WallpaperLibViewController.m
//  UmmApp
//
//  Created by Neha Saxena on 07/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "WallpaperLibViewController.h"
#import "WallpaperViewController.h"
@interface WallpaperLibViewController ()

@end

@implementation WallpaperLibViewController
- (void)setNavigationBar
{
    self.title = NSLocalizedString(@"Wallpaper", @"Wallpaper") ;
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 11, 20)];
    [backButton setImage:[UIImage imageNamed:@"arrow1"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem.enabled = YES;
    self.navigationItem.hidesBackButton = YES;
}
- (IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar];
    self.scrollView.contentSize = CGSizeMake(320, 902);
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnImageClicked:(id)sender {
    UIButton *btn = (UIButton*)sender;
    UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"%d",btn.tag]];
    WallpaperViewController *wpVC = [[WallpaperViewController alloc] initWithNibName:@"WallpaperViewController" bundle:nil];
    wpVC.isNavigate = YES;
    wpVC.imgName =[NSString stringWithFormat:@"%d",btn.tag];
    wpVC.selectedImage = img;
    [self.navigationController pushViewController:wpVC animated:YES];
}
@end
