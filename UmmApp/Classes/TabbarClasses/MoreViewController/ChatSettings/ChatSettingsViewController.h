//
//  ChatSettingsViewController.h
//  UmmApp
//
//  Created by Neha Saxena on 03/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "chatWallpaperViewController.h"
@interface ChatSettingsViewController : UIViewController<chatWallpaperViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tblView;

@end
