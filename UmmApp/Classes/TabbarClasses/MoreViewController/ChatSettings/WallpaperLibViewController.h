//
//  WallpaperLibViewController.h
//  UmmApp
//
//  Created by Neha Saxena on 07/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WallpaperLibViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)btnImageClicked:(id)sender;


@end
