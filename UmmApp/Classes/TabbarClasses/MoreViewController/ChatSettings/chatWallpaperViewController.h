//
//  chatWallpaperViewController.h
//  UmmApp
//
//  Created by Neha Saxena on 04/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WallpaperViewController.h"
@protocol chatWallpaperViewControllerDelegate <NSObject>
- (void)setSelectedWallpaper:(NSString *)wallpaper;
@end
@interface chatWallpaperViewController : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIImagePickerController *picker;
}
@property (strong, nonatomic) IBOutlet UITableView *tview;
@property (nonatomic, strong)    NSString *selectedWallpaperLink;
@property (nonatomic, weak) id<chatWallpaperViewControllerDelegate> delegate;


@end
