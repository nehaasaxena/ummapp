//
//  chatWallpaperViewController.m
//  UmmApp
//
//  Created by Neha Saxena on 04/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "chatWallpaperViewController.h"
#import "WallpaperLibViewController.h"
@interface chatWallpaperViewController ()

@end

@implementation chatWallpaperViewController
@synthesize selectedWallpaperLink;
- (void)setNavigationBar
{
//    CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 150, 44);
//    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
//    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
//    _headerTitleSubtitleView.autoresizesSubviews = NO;
//    
//    CGRect titleFrame = CGRectMake(0,0, 150, 44);
//    UILabel *titleView = [[UILabel alloc] initWithFrame:titleFrame];
//    titleView.backgroundColor = [UIColor clearColor];
//    titleView.font = [UIFont fontWithName:ssFontBrushScriptStd size:20];
//    titleView.textAlignment = NSTextAlignmentCenter;
//    titleView.textColor = [UIColor whiteColor];
//    titleView.text = @"Chat Wallpaper";
//    titleView.adjustsFontSizeToFitWidth = YES;
//    [_headerTitleSubtitleView addSubview:titleView];
//    self.navigationItem.titleView = _headerTitleSubtitleView;
    self.title = NSLocalizedString(@"Chat Wallpaper", @"Chat Wallpaper") ;

    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 11, 20)];
    [backButton setImage:[UIImage imageNamed:@"arrow1"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem.enabled = YES;
    self.navigationItem.hidesBackButton = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar];
    
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   NSDictionary *dict = [[[NSUserDefaults standardUserDefaults] valueForKey:kAPP_SETTINGS] objectForKey:kCHAT_SETTINGS];
    selectedWallpaperLink = [dict objectForKey:kCHAT_SETTINGS_CHAT_WALLPAPER];

}
#pragma mark - button Clicked methods
- (IBAction)backBtnClicked:(id)sender
{
    if([self.delegate respondsToSelector:@selector(setSelectedWallpaper:)])
    {
        [self.delegate setSelectedWallpaper:selectedWallpaperLink];
    }

    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - imagepicker methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:NO completion:nil];
    UIImage *image = (UIImage *)[info objectForKey:@"UIImagePickerControllerOriginalImage"];
    WallpaperViewController *wallpaper = [[WallpaperViewController alloc] initWithNibName:@"WallpaperViewController" bundle:nil];
    wallpaper.selectedImage = image;
    [self presentViewController:wallpaper animated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)openPicker
{
    picker=[[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    picker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage, nil];

    [self presentViewController:picker animated:YES completion:nil];
}
- (void)wallpaperSelectionDone
{
    selectedWallpaperLink = @"selected";
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:selectedWallpaperLink,kCHAT_SETTINGS_CHAT_WALLPAPER,[[[[NSUserDefaults standardUserDefaults] valueForKey:kAPP_SETTINGS] objectForKey:kCHAT_SETTINGS] objectForKey:kCHAT_SETTINGS_AUTODOWNLOAD],kCHAT_SETTINGS_AUTODOWNLOAD,[[[[NSUserDefaults standardUserDefaults] valueForKey:kAPP_SETTINGS] objectForKey:kCHAT_SETTINGS] objectForKey:kCHAT_SETTINGS_SAVE_INCOMING_MEDIA],kCHAT_SETTINGS_SAVE_INCOMING_MEDIA, nil];
    [AppManager updateAppSettings:[NSMutableDictionary dictionaryWithDictionary:dictionary] withKey:kCHAT_SETTINGS];

}
#pragma mark - buttom clicked methods
- (void)backButtonClicked
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 2;
            break;
        case 1:
            return 1;
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    
    
    NSArray* nib;
    switch (indexPath.section)
    {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    if (cell==nil) {
                        
                        nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
                        cell = [nib objectAtIndex:0];
                        
                    }
                    UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:100];
                    lblSettingNameSection2.textColor = [UIColor darkGrayColor];
                    lblSettingNameSection2.text = NSLocalizedString(@"Photos", @"Photos") ;
                }
                    break;
                case 1:
                {
                    if (cell==nil) {
                        
                        nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
                        cell = [nib objectAtIndex:0];
                        
                    }
                    UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:100];
                    lblSettingNameSection2.textColor = [UIColor darkGrayColor];
                    lblSettingNameSection2.text = NSLocalizedString(@"Wallpaper Library", @"Wallpaper Library") ;
                }
                    break;
    
                
            }
        }
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    if (cell==nil) {
                        
                        nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
                        cell = [nib objectAtIndex:2];
                        
                    }
                    UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:300];
                    lblSettingNameSection2.text = NSLocalizedString(@"Reset Wallpaper", @"Reset Wallpaper") ;
                }
                    break;
                    
            }
        }
            break;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.navigationItem.rightBarButtonItem.enabled = YES;
    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    [self openPicker];
                }
                    break;
                    
                case 1:
                {
                    [self OpenWallPaperLibrary];
                }
            }
        }
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    selectedWallpaperLink = @"default";
                    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"default",kCHAT_SETTINGS_CHAT_WALLPAPER,[[[[NSUserDefaults standardUserDefaults] valueForKey:kAPP_SETTINGS] objectForKey:kCHAT_SETTINGS] objectForKey:kCHAT_SETTINGS_AUTODOWNLOAD],kCHAT_SETTINGS_AUTODOWNLOAD,[[[[NSUserDefaults standardUserDefaults] valueForKey:kAPP_SETTINGS] objectForKey:kCHAT_SETTINGS] objectForKey:kCHAT_SETTINGS_SAVE_INCOMING_MEDIA],kCHAT_SETTINGS_SAVE_INCOMING_MEDIA, nil];
                    [AppManager updateAppSettings:[NSMutableDictionary dictionaryWithDictionary:dictionary] withKey:kCHAT_SETTINGS];
                }
                    break;
            }
        }
            break;
    }
}
- (void)OpenWallPaperLibrary
{
    WallpaperLibViewController *wl = [[WallpaperLibViewController alloc] initWithNibName:@"WallpaperLibViewController" bundle:nil];
    [self.navigationController pushViewController:wl animated:YES];
}

@end
