//
//  WallpaperViewController.h
//  UmmApp
//
//  Created by Neha Saxena on 04/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WallpaperViewController : UIViewController
@property (strong,nonatomic) UIImage *selectedImage;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewSelected;
@property (nonatomic, assign) BOOL isNavigate;
@property (nonatomic, retain) NSString *imgName;
- (IBAction)cancelBtnClicked:(id)sender;
- (IBAction)doneButtonClicked:(id)sender;

@end
