//
//  WallpaperViewController.m
//  UmmApp
//
//  Created by Neha Saxena on 04/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "WallpaperViewController.h"
#import "NSData+Base64.h"

@interface WallpaperViewController ()

@end

@implementation WallpaperViewController

#pragma mark - view life cycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.isNavigate = NO;
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    if(self.isNavigate)
    {
        self.navigationController.navigationBarHidden = YES;
    }
    self.imgViewSelected.image = self.selectedImage;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelBtnClicked:(id)sender {
    if(self.isNavigate)
    {
        self.navigationController.navigationBarHidden = NO;
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}
- (IBAction)doneButtonClicked:(id)sender {
    if(!self.isNavigate)
    {
    NSData *data = UIImageJPEGRepresentation(self.selectedImage, 0.5);
    NSString *baseString = [data base64Encoding];
    [[NSUserDefaults standardUserDefaults] setObject:baseString  forKey:@"Wallpaper"];
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"photos",kCHAT_SETTINGS_CHAT_WALLPAPER,[[[[NSUserDefaults standardUserDefaults] valueForKey:kAPP_SETTINGS] objectForKey:kCHAT_SETTINGS] objectForKey:kCHAT_SETTINGS_AUTODOWNLOAD],kCHAT_SETTINGS_AUTODOWNLOAD,[[[[NSUserDefaults standardUserDefaults] valueForKey:kAPP_SETTINGS] objectForKey:kCHAT_SETTINGS] objectForKey:kCHAT_SETTINGS_SAVE_INCOMING_MEDIA],kCHAT_SETTINGS_SAVE_INCOMING_MEDIA, nil];
        [AppManager updateAppSettings:[NSMutableDictionary dictionaryWithDictionary:dictionary] withKey:kCHAT_SETTINGS];

    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:self.imgName  forKey:@"Wallpaper"];
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"wallpaper",kCHAT_SETTINGS_CHAT_WALLPAPER,[[[[NSUserDefaults standardUserDefaults] valueForKey:kAPP_SETTINGS] objectForKey:kCHAT_SETTINGS] objectForKey:kCHAT_SETTINGS_AUTODOWNLOAD],kCHAT_SETTINGS_AUTODOWNLOAD,[[[[NSUserDefaults standardUserDefaults] valueForKey:kAPP_SETTINGS] objectForKey:kCHAT_SETTINGS] objectForKey:kCHAT_SETTINGS_SAVE_INCOMING_MEDIA],kCHAT_SETTINGS_SAVE_INCOMING_MEDIA, nil];
        [AppManager updateAppSettings:[NSMutableDictionary dictionaryWithDictionary:dictionary] withKey:kCHAT_SETTINGS];
    }
    if(self.isNavigate)
    {
        self.navigationController.navigationBarHidden = NO;
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self dismissViewControllerAnimated:NO completion:nil];
    }

}
@end
