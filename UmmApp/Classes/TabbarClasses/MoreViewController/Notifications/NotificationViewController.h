//
//  NotificationViewController.h
//  UmmApp
//
//  Created by Neha Saxena on 04/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationToneViewController.h"
@interface NotificationViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,NotificationToneViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tViewNotification;

@end
