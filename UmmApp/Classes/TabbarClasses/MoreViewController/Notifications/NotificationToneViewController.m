//
//  NotificationToneViewController.m
//  UmmApp
//
//  Created by Neha Saxena on 04/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "NotificationToneViewController.h"

@interface NotificationToneViewController ()

@end

@implementation NotificationToneViewController
@synthesize soundFileObject = _soundFileObject;
@synthesize soundFileURLRef = _soundFileURLRef;
#pragma mark - notification bar method

- (void)setNavigationBar
{
    self.title = NSLocalizedString(@"Notification Tone", @"Notification Tone") ;
    self.navigationItem.hidesBackButton = YES;
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 11, 20)];
    [backButton setImage:[UIImage imageNamed:@"arrow1"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(btnBackClicked) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
}
- (void)btnBackClicked {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - view life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar];
    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    NSString *plistPath;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                              NSUserDomainMask, YES) objectAtIndex:0];
    plistPath = [rootPath stringByAppendingPathComponent:@"sounds.plist"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        plistPath = [[NSBundle mainBundle] pathForResource:@"sounds" ofType:@"plist"];
    }
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    self.arrSounds = (NSMutableArray *)[NSPropertyListSerialization
                                        propertyListFromData:plistXML
                                        mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                        format:&format
                                        errorDescription:&errorDesc];

    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tViewTone reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrSounds count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    
    if (cell == nil)
    {
        NSArray* nib;
        nib  = [[NSBundle mainBundle] loadNibNamed:@"NotificationCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    UILabel *lblName = (UILabel *)[cell.contentView viewWithTag:200];
    [lblName setText:[[self.arrSounds objectAtIndex:indexPath.row] objectForKey:@"Name"]];
    [lblName setFont:[UIFont fontWithName:ssFontHelveticaNeue size:15.0]];
    [lblName setTextColor:[UIColor blackColor]];
    
    UIImageView *imgView = (UIImageView *)[cell.contentView viewWithTag:100];
    if([self.selectedSound isEqualToString:[NSString stringWithFormat:@"%@",[[_arrSounds objectAtIndex:indexPath.row] valueForKey:@"Name"]]])//.aif
    {
        [imgView setImage:[UIImage imageNamed:@"settingsTick"]];
    }
    else
    {
        [imgView setImage:[UIImage imageNamed:@""]];
    }
    
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    self.btnSave.hidden = NO;
    NSURL *tapSound   = [[NSBundle mainBundle] URLForResource: [[self.arrSounds objectAtIndex:indexPath.row] objectForKey:@"Tone"]
                                                withExtension: @"wav"];
    if(tapSound != nil)
    {
        [Utils playSound:[[self.arrSounds objectAtIndex:indexPath.row] objectForKey:@"Tone"]];
    }
    self.selectedSound =[NSString stringWithFormat:@"%@",[[_arrSounds objectAtIndex:indexPath.row] objectForKey:@"Name"]];
    if([self.delegate respondsToSelector:@selector(setSelectedTone:)])
    {
      [self.delegate setSelectedTone:[NSString stringWithFormat:@"%@",[[_arrSounds objectAtIndex:indexPath.row] objectForKey:@"Name"]]];
    }
    [self.tViewTone reloadData];
}

@end
