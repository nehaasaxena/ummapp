//
//  NotificationViewController.m
//  UmmApp
//
//  Created by Neha Saxena on 04/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "NotificationViewController.h"

@interface NotificationViewController ()
{
    NSMutableDictionary *dict;
    NSString *alert;
    NSString *tone;
    NSString *showPreview;
}
@end

@implementation NotificationViewController
#pragma mark - navigation setting method
- (void)setNavigationBar
{
//    CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 150, 44);
//    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
//    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
//    _headerTitleSubtitleView.autoresizesSubviews = NO;
//    
//    CGRect titleFrame = CGRectMake(0,0, 150, 44);
//    UILabel *titleView = [[UILabel alloc] initWithFrame:titleFrame];
//    titleView.backgroundColor = [UIColor clearColor];
//    titleView.font = [UIFont fontWithName:ssFontBrushScriptStd size:20];
//    titleView.textAlignment = NSTextAlignmentCenter;
//    titleView.textColor = [UIColor whiteColor];
//    titleView.text = @"Notifications";
//    titleView.adjustsFontSizeToFitWidth = YES;
//    [_headerTitleSubtitleView addSubview:titleView];
//    self.navigationItem.titleView = _headerTitleSubtitleView;
    self.title = NSLocalizedString(@"General Notifications",nil);

    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 11, 20)];
    [backButton setImage:[UIImage imageNamed:@"arrow1"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem.enabled = YES;
    self.navigationItem.hidesBackButton = YES;
    
    UIButton *doneButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
    [doneButton setTitle:NSLocalizedString(@"Done",@"Done") forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor colorWithRed:16.0/255.0 green:170.0/255.0 blue:180.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [doneButton.titleLabel setFont:[UIFont boldSystemFontOfSize:17.0f]];
    [doneButton addTarget:self action:@selector(doneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
    self.navigationItem.rightBarButtonItem.enabled = NO;
}
- (IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)doneButtonClicked:(id)sender
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:alert,kALERT,tone,kSOUNDNAME,showPreview,kSHOW_PREVIEW, nil];
    [AppManager updateAppSettings:[NSMutableDictionary dictionaryWithDictionary:dictionary] withKey:kMESSAGE_NOTIFICATION];
    [AppManager callUpdateUserAppSettingWebService:[[NSMutableDictionary alloc] initWithObjectsAndKeys:dictionary,kMESSAGE_NOTIFICATION, nil] serviceType:kUpdateUserSettings];
    [self.navigationController popViewControllerAnimated:YES];
    
}
#pragma mark - view life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar];
    
    dict = [[NSMutableDictionary alloc] init];
    dict = [[[NSUserDefaults standardUserDefaults] valueForKey:kAPP_SETTINGS] objectForKey:kMESSAGE_NOTIFICATION];
    alert = [dict objectForKey:kALERT];
    tone = [dict objectForKey:kSOUNDNAME];
    showPreview = [dict objectForKey:kSHOW_PREVIEW];
    if(!showPreview)
        showPreview = @"1";

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        UIView *viewSection = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        viewSection.backgroundColor = [UIColor clearColor];
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 300, 25)];
        lbl.backgroundColor = [UIColor clearColor];
        lbl.text = NSLocalizedString(@"MESSAGE NOTIFICATIONS", @"MESSAGE NOTIFICATIONS") ;
        lbl.textColor = [UIColor lightGrayColor];
        lbl.font = [UIFont systemFontOfSize:14.0F];
        [viewSection addSubview:lbl];
        
        return viewSection;
    }
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 2;
            break;
        case 1:
            return 1;
            break;
        case 2:
            return 1;
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    
    
    NSArray* nib;
    switch (indexPath.section)
    {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    if (cell==nil) {
                        
                        nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
                        cell = [nib objectAtIndex:0];
                        
                    }
                    UILabel *lblSettingNameSection1 = (UILabel *)[cell.contentView viewWithTag:100];
                    lblSettingNameSection1.text = NSLocalizedString(@"New Message", @"New Message") ;
                    
                    UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:101];
                    lblSettingNameSection2.text = tone;

                }
                    break;
                case 1:
                {
                    if (cell==nil) {
                        
                        nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
                        cell = [nib objectAtIndex:1];
                        
                    }
                    UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:200];
                    lblSettingNameSection2.text = NSLocalizedString(@"Alerts",@"Alerts");
                    
                    UISwitch *saveSwitch = (UISwitch *)[cell.contentView viewWithTag:201];
                    [saveSwitch addTarget:self action:@selector(alertSwitch:) forControlEvents:UIControlEventTouchUpInside];
                    if([alert integerValue]==1)
                    {
                        [saveSwitch setOn:YES];
                    }
                    else
                    {
                        [saveSwitch setOn:NO];
                    }
                }
                    break;

            }
        }
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    if (cell==nil) {
                        
                        nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
                        cell = [nib objectAtIndex:1];
                        
                    }
                    UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:200];
                    lblSettingNameSection2.text = NSLocalizedString(@"Show Preview",@"Show Preview");
                    
                    UISwitch *autoSwitch = (UISwitch *)[cell.contentView viewWithTag:201];
                    [autoSwitch addTarget:self action:@selector(showPreview:) forControlEvents:UIControlEventTouchUpInside];
                    if([showPreview integerValue]==1)
                    {
                        [autoSwitch setOn:YES];
                    }
                    else
                    {
                        [autoSwitch setOn:NO];
                    }
                }
                    break;
            }
        }
            break;
        case 2:
        {
            switch (indexPath.row) {
                case 0:
                {
                    if (cell==nil) {
                        
                        nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
                        cell = [nib objectAtIndex:0];
                        
                    }
                    UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:100];
                    lblSettingNameSection2.textColor = [UIColor redColor];
                    lblSettingNameSection2.text = NSLocalizedString( @"Reset All Notifications",  @"Reset All Notifications");
                }
                    break;
            }
        }
            break;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    self.navigationItem.rightBarButtonItem.enabled = YES;
    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    NotificationToneViewController *toneNotify = [[NotificationToneViewController alloc] initWithNibName:@"NotificationToneViewController" bundle:nil];
                    toneNotify.selectedSound = tone;
                    toneNotify.delegate = self;
                    [self.navigationController pushViewController:toneNotify animated:YES];
                }
                    break;
                case 1:
                {
                    UISwitch *saveSwitch = (UISwitch *)[cell.contentView viewWithTag:201];
                    
                    if([alert integerValue]==1)
                    {
                        alert = @"0";
                        [saveSwitch setOn:NO];
                    }
                    else
                    {
                        alert = @"1";
                        [saveSwitch setOn:YES];
                    }
                }
                    break;

            }
        }
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    UISwitch *saveSwitch = (UISwitch *)[cell.contentView viewWithTag:201];
                    if([showPreview integerValue]==1)
                    {
                        showPreview = @"0";
                        [saveSwitch setOn:NO];
                    }
                    else
                    {
                        showPreview = @"1";
                        [saveSwitch setOn:YES];
                    }
                }
                    break;
            }
        }
            break;
        case 2:
        {
            switch (indexPath.row)
            {
                case 0:
                {
                    alert = @"1";
                    showPreview = @"1";
                    tone = @"None";
                    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:alert,kALERT,tone,kSOUNDNAME,showPreview,kSHOW_PREVIEW, nil];
                    [AppManager updateAppSettings:[NSMutableDictionary dictionaryWithDictionary:dictionary] withKey:kMESSAGE_NOTIFICATION];
                    [AppManager callUpdateUserAppSettingWebService:[[NSMutableDictionary alloc] initWithObjectsAndKeys:dictionary,kMESSAGE_NOTIFICATION, nil] serviceType:kResetUserSettings];
                    [self.tViewNotification reloadData];
                }
                    break;
            }
        }
            break;
    }
}

#pragma mark - switch methods
- (IBAction)alertSwitch:(id)sender
{
    self.navigationItem.rightBarButtonItem.enabled = YES;
    UISwitch *swi = (UISwitch *)sender;
    if ([swi isOn]) {
        alert = @"1";
    }
    else
    {
        alert = @"0";
    }
}
- (IBAction)showPreview:(id)sender
{
    self.navigationItem.rightBarButtonItem.enabled = YES;
    UISwitch *swi = (UISwitch *)sender;
    if ([swi isOn]) {
        showPreview = @"1";
    }
    else
    {
        showPreview = @"0";
    }
}

-(void)setSelectedTone:(NSString *)alertTone
{
    tone = alertTone;
    [self.tViewNotification reloadData];
}
@end
