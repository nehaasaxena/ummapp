//
//  NotificationToneViewController.h
//  UmmApp
//
//  Created by Neha Saxena on 04/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AudioToolbox/AudioServices.h> 
@protocol NotificationToneViewControllerDelegate <NSObject>
- (void)setSelectedTone:(NSString *)alertTone;
@end
@interface NotificationToneViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tViewTone;
@property (nonatomic, strong) NSMutableArray *arrSounds;
@property (nonatomic, strong) NSString *selectedSound;
@property (nonatomic, weak) id<NotificationToneViewControllerDelegate> delegate;


@property (readwrite)	CFURLRef		soundFileURLRef;
@property (readonly)	SystemSoundID	soundFileObject;
@end
