//
//  HelpAndFAQViewController.h
//  UmmApp
//
//  Created by Neha Saxena on 03/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpAndFAQViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic) BOOL isContactUs;
@end
