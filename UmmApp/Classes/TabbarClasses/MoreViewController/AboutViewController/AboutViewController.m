//
//  AboutViewController.m
//  UmmApp
//
//  Created by Neha Saxena on 03/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "AboutViewController.h"
#import "HelpAndFAQViewController.h"
@interface AboutViewController ()

@end

@implementation AboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if([[UIScreen mainScreen] bounds].size.height > 480)
    {
        nibNameOrNil = @"AboutViewController";
    }
    else
    {
        nibNameOrNil = @"AboutViewController4";
    }
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

        // Custom initialization
    }
    return self;
}
- (void)setNavigationBar
{
//    CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 150, 44);
//    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
//    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
//    _headerTitleSubtitleView.autoresizesSubviews = NO;
//    
//    CGRect titleFrame = CGRectMake(0,0, 150, 44);
//    UILabel *titleView = [[UILabel alloc] initWithFrame:titleFrame];
//    titleView.backgroundColor = [UIColor clearColor];
//    titleView.font = [UIFont fontWithName:ssFontBrushScriptStd size:20];
//    titleView.textAlignment = NSTextAlignmentCenter;
//    titleView.textColor = [UIColor whiteColor];
//    titleView.text = @"About Ummapp";
//    titleView.adjustsFontSizeToFitWidth = YES;
//    [_headerTitleSubtitleView addSubview:titleView];
//    self.navigationItem.titleView = _headerTitleSubtitleView;
//    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};

    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.opaque = YES;
    self.title =NSLocalizedString(@"About Ummapp", @"About Ummapp") ;

    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,11, 20)];
    [backButton setImage:[UIImage imageNamed:@"arrow1"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem.enabled = YES;
//    self.navigationItem.hidesBackButton = YES;

}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - buttom clicked methods
- (void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    
    
    NSArray* nib;
    switch (indexPath.row)
    {

        case 0:
        {
            if (cell==nil) {
                
                nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
                cell = [nib objectAtIndex:0];
                
            }
            UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:100];
            lblSettingNameSection2.textColor = [UIColor blackColor];
            lblSettingNameSection2.text =NSLocalizedString(@"Help/FAQ", @"Help/FAQ") ;
        }
            break;
        case 1:
        {
            if (cell==nil) {
                
                nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
                cell = [nib objectAtIndex:0];
                
            }
            UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:100];
            lblSettingNameSection2.textColor = [UIColor blackColor];
            lblSettingNameSection2.text = NSLocalizedString(@"Contact Us", @"Contact Us") ;
        }
        break;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case 0:
        {
            HelpAndFAQViewController *hAF = [[HelpAndFAQViewController alloc] initWithNibName:@"HelpAndFAQViewController" bundle:nil];
            hAF.isContactUs = NO;
            [self.navigationController pushViewController:hAF animated:YES];
        }
            break;
        case 1:
        {
            HelpAndFAQViewController *hAF = [[HelpAndFAQViewController alloc] initWithNibName:@"HelpAndFAQViewController" bundle:nil];
            hAF.isContactUs = YES;
            [self.navigationController pushViewController:hAF animated:YES];

        }
            break;
    }
}

@end
