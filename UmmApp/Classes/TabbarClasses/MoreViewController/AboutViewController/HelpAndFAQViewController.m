//
//  HelpAndFAQViewController.m
//  UmmApp
//
//  Created by Neha Saxena on 03/11/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "HelpAndFAQViewController.h"

@interface HelpAndFAQViewController ()

@end

@implementation HelpAndFAQViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!_isContactUs) {
        self.title = NSLocalizedString(@"Help/FAQ",@" Help/FAQ");
    }else
        self.title = NSLocalizedString(@"Contact Us",@" Contact Us");
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 11, 20)];
    [backButton setImage:[UIImage imageNamed:@"arrow1"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem.enabled = YES;
    self.navigationItem.hidesBackButton = YES;

//    if(![Utils isIPhone5])
//    {
//        CGRect rect = self.webView.frame;
//        rect.size.height = self.webView.frame.size.height - 88;
//        self.webView.frame = rect;
//    }
    
    if (!_isContactUs) {
         [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.ummapp.de/faq"]]];
    }else{
         [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.ummapp.de/contact"]]];
    }
    
   

    // Do any additional setup after loading the view from its nib.
}
- (void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
