//
//  MoreViewController.m
//  UmmApp
//
//  Created by Neha Saxena on 20/10/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "MoreViewController.h"
#import "ImageAndUsernameViewController.h"
#import "AboutViewController.h"
#import "ChatSettingsViewController.h"
#import "NotificationViewController.h"
#import "ContactListViewController.h"
@interface MoreViewController ()

@end

@implementation MoreViewController
-(void)setNavigationBar
{
//    CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 150, 44);
//    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
//    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
//    _headerTitleSubtitleView.autoresizesSubviews = NO;
//    
//    CGRect titleFrame = CGRectMake(0,0, 150, 44);
//    UILabel *titleView = [[UILabel alloc] initWithFrame:titleFrame];
//    titleView.backgroundColor = [UIColor clearColor];
//    titleView.font = [UIFont fontWithName:ssFontBrushScriptStd size:20];
//    titleView.textAlignment = NSTextAlignmentCenter;
//    titleView.textColor = [UIColor whiteColor];
//    titleView.text = @"More";
//    titleView.adjustsFontSizeToFitWidth = YES;
//    [_headerTitleSubtitleView addSubview:titleView];
//    self.navigationItem.titleView = _headerTitleSubtitleView;
    self.navigationController.navigationItem.title =NSLocalizedString(@"More", @"More") ;

    self.navigationItem.rightBarButtonItem.enabled = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 2;
            break;
        case 1:
            return 3;
            break;
        case 2:
            return 2;
            break;
        default:
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    
    
    NSArray* nib;
    switch (indexPath.section)
    {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    if (cell==nil) {
                        
                        nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
                        cell = [nib objectAtIndex:0];
                        
                    }
                    UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:100];
                    lblSettingNameSection2.textColor = [UIColor darkGrayColor];
                    lblSettingNameSection2.text =NSLocalizedString(@"About", @"About") ;
                }
                break;
                case 1:
                {
                    if (cell==nil) {
                        
                        nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
                        cell = [nib objectAtIndex:0];
                        
                    }
                    UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:100];
                    lblSettingNameSection2.textColor = [UIColor darkGrayColor];
                    lblSettingNameSection2.text = NSLocalizedString(@"Tell A Friend",nil);
                }
                    break;
            }
        }
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    if (cell==nil) {
                        
                        nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
                        cell = [nib objectAtIndex:0];
                        
                    }
                    UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:100];
                    lblSettingNameSection2.textColor = [UIColor darkGrayColor];
                    lblSettingNameSection2.text = NSLocalizedString(@"Profile",nil);
                }
                    break;
//                case 1:
//                {
//                    if (cell==nil) {
//                        
//                        nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
//                        cell = [nib objectAtIndex:0];
//                        
//                    }
//                    UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:100];
//                    lblSettingNameSection2.textColor = [UIColor darkGrayColor];
//                    lblSettingNameSection2.text = NSLocalizedString(@"Account",nil);
//                }
//                    break;
                case 1:
                {
                    if (cell==nil) {
                        
                        nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
                        cell = [nib objectAtIndex:0];
                        
                    }
                    UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:100];
                    lblSettingNameSection2.textColor = [UIColor darkGrayColor];

                    lblSettingNameSection2.text = NSLocalizedString(@"Chat Settings",nil);
                }
                    break;
                case 2:
                {
                    if (cell==nil) {
                        
                        nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
                        cell = [nib objectAtIndex:0];
                        
                    }
                    UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:100];
                    lblSettingNameSection2.textColor = [UIColor darkGrayColor];

                    lblSettingNameSection2.text = NSLocalizedString(@"General Notifications",nil);
                }
                    break;
            }
        }
            break;
        case 2:
        {
            switch (indexPath.row) {
                case 0:
                {
                    if (cell==nil) {
                        
                        nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
                        cell = [nib objectAtIndex:0];
                        
                    }
                    UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:100];
                    lblSettingNameSection2.textColor = [UIColor redColor];
                    lblSettingNameSection2.text = NSLocalizedString(@"Clear All Conversations",nil);
                }
                    break;
                case 1:
                {
                    if (cell==nil) {
                        
                        nib  = [[NSBundle mainBundle] loadNibNamed:@"CellFriendsSetting" owner:self options:nil];
                        cell = [nib objectAtIndex:0];
                        
                    }
                    UILabel *lblSettingNameSection2 = (UILabel *)[cell.contentView viewWithTag:100];
                    lblSettingNameSection2.textColor = [UIColor redColor];
                    lblSettingNameSection2.text = NSLocalizedString(@"Delete Account",nil);
                }
                    break;
            }
        }
            break;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    AboutViewController *about = [[AboutViewController alloc] initWithNibName:nil bundle:nil];
                    [about setHidesBottomBarWhenPushed:YES];
                    [self.navigationController pushViewController:about animated:YES];
                }
                    break;
                case 1:
                {
                    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Mail",nil),NSLocalizedString(@"Message",nil), nil];
                    actionSheet.tag = 2000;
                    [actionSheet showInView:self.view];
                }
                    break;
            }
        }
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    ImageAndUsernameViewController *imageAndUN= [[ImageAndUsernameViewController alloc] initWithNibName:@"ImageAndUsernameViewController" bundle:nil];
                    imageAndUN.isComingFromSettings = YES;
                    imageAndUN.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:imageAndUN animated:YES];
                }
                    break;
//                case 1:
//                {
//                    
//                }
//                    break;
                case 1:
                {
                    ChatSettingsViewController *chatSettings = [[ChatSettingsViewController alloc] initWithNibName:@"ChatSettingsViewController" bundle:nil];
                    chatSettings.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:chatSettings animated:YES];
                }
                    break;
                case 2:
                {
                    NotificationViewController *notification = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
                    notification.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:notification animated:YES];
                }
                    break;
            }
        }
            break;
        case 2:
        {
            switch (indexPath.row) {
                case 0:
                {
                    [Utils showAlertView:kAlertTitle message:NSLocalizedString(@"Are you sure you want to delete all conversations?",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"Cancel") otherButtonTitles:NSLocalizedString(@"Yes",@"Yes")];
                }
                    break;
                case 1:
                {
                    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:NSLocalizedString(@"Are you sure you want to delete your account?",@"Are you sure you want to delete your account?") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"Cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Yes",@"Yes"), nil];
                    
                    [self changeTextColorForUIActionSheet:actionSheet];
                    actionSheet.tag = 1000;
                    [actionSheet showInView:self.view];
                }
                    break;

            }
        }
            break;
    }
}
- (void) changeTextColorForUIActionSheet:(UIActionSheet*)actionSheet {
    UIColor *tintColor = [UIColor redColor];
    
    NSArray *actionSheetButtons = actionSheet.subviews;
    for (int i = 0; [actionSheetButtons count] > i; i++) {
        UIView *view = (UIView*)[actionSheetButtons objectAtIndex:i];
        if([view isKindOfClass:[UIButton class]]){
            UIButton *btn = (UIButton*)view;
            [btn setTitleColor:tintColor forState:UIControlStateNormal];
            
        }
    }
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(actionSheet.tag == 2000)
    {
        switch (buttonIndex) {
            case 0:
            {
                ContactListViewController *list = nil;
                if([Utils isIPhone5])
                {
                    list = [[ContactListViewController alloc] initWithNibName:@"ContactListViewController" bundle:nil];
                }
                else
                {
                    list = [[ContactListViewController alloc] initWithNibName:@"ContactListViewController4" bundle:nil];
                }
                list.showEmail = YES;
                list.delegate = self;
                [self presentViewController:list animated:YES completion:nil];
            }
                break;
            case 1:
            {
                ContactListViewController *list = nil;
                if([Utils isIPhone5])
                {
                    list = [[ContactListViewController alloc] initWithNibName:@"ContactListViewController" bundle:nil];
                }
                else
                {
                    list = [[ContactListViewController alloc] initWithNibName:@"ContactListViewController4" bundle:nil];
                }
                list.showEmail = NO;
                list.delegate =self;
                [self presentViewController:list animated:YES completion:nil];
            }
            default:
                break;
        }
    }
    else
    {
        switch (buttonIndex) {
            case 0:
            {
                [self callWebService];
            }
                break;
            default:
                break;
        }
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 1:
            [AppManager clearAllConversation];
            break;
            
        default:
            break;
    }
}

- (void)selectedEmail:(NSArray *)arrEmail
{
    [self performSelector:@selector(sendEmail:) withObject:arrEmail afterDelay:0.5];
}
- (void)sendEmail:(NSArray *)arrEmail
{
    NSString *strMessage= NSLocalizedString(@"I want to add you as a friend on UmmApp.",nil);
    
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setSubject:NSLocalizedString(@"UmmApp Invitation",nil)];
    [controller setMessageBody:strMessage isHTML:YES];
    [controller setToRecipients:arrEmail];
    
    if (controller) [self presentViewController:controller animated:YES completion:nil];
}
- (void)selectedNumbers:(NSArray *)arrNumbers
{
    [self performSelector:@selector(sendMessage:) withObject:arrNumbers afterDelay:0.5];
}
- (void)sendMessage:(NSArray *)arrNumbers
{
    NSString *strMessage=NSLocalizedString(@"I want to add you as a friend on UmmApp.",nil);
    MFMessageComposeViewController *mfMessageComposer=[[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        mfMessageComposer.body = strMessage;
        mfMessageComposer.recipients = arrNumbers;
        mfMessageComposer.messageComposeDelegate = self;
        [self presentViewController:mfMessageComposer animated:YES completion:nil];
    }

}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            //NSLog(@"Result: canceled");
            break;
        case MFMailComposeResultSaved:
            //NSLog(@"Result: saved");
            
            break;
        case MFMailComposeResultSent:
            //NSLog(@"Result: sent");
            
            break;
        case MFMailComposeResultFailed:
            //NSLog(@"Result: failed");
            
            break;
        default:
            //NSLog(@"Result: not sent");
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    NSLog(@"Messaged Successfully");
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - call web Service Methods
- (void)callWebService
{
    if(![Utils isInternetAvailable])
    {
        [Utils showAlertView:kAlertTitle message:NSLocalizedString(@"Check your internet connection.", @"Check your internet connection.")  delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
    }
    else
    {
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] valueForKey:kSessionToken],kSessionToken,[[NSUserDefaults standardUserDefaults] valueForKey:kDeviceToken],kDeviceToken,[[NSUserDefaults standardUserDefaults] valueForKey:kUserId],kUserId,kDeleteAccount,kOption,kDevice,kDeviceType,nil];
        
        NetworkService *obj = [NetworkService sharedInstance];
        [obj sendAsynchRequestByPostToServer:kUsers dataToSend:dict delegate:self contentType:eAppJsonType andReqParaType:eJson header:NO];
        
    }
}
-(void) responseHandler :(id)inResponseDic andRequestIdentifier:(NSString *)inReqIdentifier
{
    [self setActivityIndicator:NO];
    if ([[inResponseDic valueForKey:SUCCESS] integerValue] ==1 ) {
        if([[inResponseDic valueForKey:kOption]isEqualToString:kDeleteAccount])
         {
             [AppManager removeDataFromNSUserDefaults];
             [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kAPP_SETTINGS];
             [APP_DELEGATE removeTabbarView];
        }
    }else{
        [Utils showAlertView:kAlertTitle message:[inResponseDic valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
}
-(void) requestErrorHandler :(NSError *)inError andRequestIdentifier :(NSString *)inReqIdentifier
{
    [self setActivityIndicator:NO];
}
- (void)setActivityIndicator:(BOOL)show
{
    if(show)
    {
        self.tblViewMore.userInteractionEnabled = NO;
        [AppManager startStatusbarActivityIndicatorWithUserInterfaceInteractionEnabled:YES];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
    else
    {
        self.tblViewMore.userInteractionEnabled = YES;
        [AppManager stopStatusbarActivityIndicator];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
    
}
@end
