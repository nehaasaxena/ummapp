//
//  AllContactsViewController.m
//  SocialParty
//
//  Created by pankaj on 18/08/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "AllContactsViewController.h"

#import "AddressBookDB.h"
#import "ContactsDetailViewController.h"
@interface NSArray (SSArrayOfArrays)
- (id)objectAtIndexPath:(NSIndexPath *)indexPath;
@end

@implementation NSArray (SSArrayOfArrays)

- (id)objectAtIndexPath:(NSIndexPath *)indexPath
{
    return [[self objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]];
}

@end

@interface NSMutableArray (SSArrayOfArrays)
// If idx is beyond the bounds of the reciever, this method automatically extends the reciever to fit with empty subarrays.
- (void)addObject:(id)anObject toSubarrayAtIndex:(NSUInteger)idx;
@end

@implementation NSMutableArray (SSArrayOfArrays)

- (void)addObject:(id)anObject toSubarrayAtIndex:(NSUInteger)idx
{
    while ([self count] <= idx) {
        [self addObject:[NSMutableArray array]];
    }
    
    [[self objectAtIndex:idx] addObject:anObject];
}

@end
@interface AllContactsViewController ()

@end

@implementation AllContactsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)setNavigationBar
{
    self.navigationItem.rightBarButtonItem.enabled = YES;
    self.title = NSLocalizedString(@"Contacts", @"Contacts") ;

}
- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setNavigationBar];
    [contactsTableView setFrame:CGRectMake(0, 110, 320, [UIScreen mainScreen].bounds.size.height-44-64-50)];
//    arrAllContacts = [[Database database] fetchDataFromDatabaseForEntity:@"AddressBookDB"];
    activityIndicatorView =[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [activityIndicatorView setFrame:CGRectMake(0, 0, 40, 40)];
    [activityIndicatorView setCenter:contactsTableView.center];
    [activityIndicatorView setHidesWhenStopped:YES];
    [self.view addSubview:activityIndicatorView];


    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    [self refreshArrayOfAllContacts];
}
#pragma Longpress Backbutton Gesture Method




-(void)refreshArrayOfAllContacts{
    arrAllContacts = [[Database database] fetchDataFromDatabaseForEntity:@"AddressBookDB"];
    if ([arrAllContacts count]==0 && [AppManager sharedManager].isFetchingContacts) {
        [activityIndicatorView startAnimating];
        [contactsTableView setHidden:YES];
    }else{
        [activityIndicatorView stopAnimating];
        [contactsTableView setHidden:NO];
    }
    
    
    [self setListContent:arrAllContacts];
    [contactsTableView reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setListContent:(NSArray *)inListContent
{
    
    NSMutableArray *sections = [NSMutableArray array];
    UILocalizedIndexedCollation *collation = [UILocalizedIndexedCollation currentCollation];
    for (NSDictionary *product in inListContent)
    {
        NSInteger section = [collation sectionForObject:product collationStringSelector:@selector(fullName)];
        [sections addObject:product toSubarrayAtIndex:section];
    }
    
    NSInteger section = 0;
    for (section = 0; section < [sections count]; section++) {
        NSArray *sortedSubarray = [collation sortedArrayFromArray:[sections objectAtIndex:section]
                                          collationStringSelector:@selector(fullName)];
        [sections replaceObjectAtIndex:section withObject:sortedSubarray];
    }
    sectionedListContent = sections ;
}
#pragma mark - UITableView delegate & Datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == [[self searchDisplayController] searchResultsTableView])
    {
        return 1;
    }
    else
        return [sectionedListContent count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == [[self searchDisplayController] searchResultsTableView])
    {
        return [filteredResult count];
    }
    else
    {
        return [[sectionedListContent objectAtIndex:section] count];
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if (tableView == self.searchDisplayController.searchResultsTableView) {
        return nil;
    } else {
        return [[sectionedListContent objectAtIndex:section] count] ? [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section] : nil;
    }
    
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return nil;
    } else {
        return [[NSArray arrayWithObject:UITableViewIndexSearch] arrayByAddingObjectsFromArray:
                [[UILocalizedIndexedCollation currentCollation] sectionIndexTitles]];
    }
}
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return 0;
    } else {
        if (title == UITableViewIndexSearch) {
            [tableView scrollRectToVisible:self.searchDisplayController.searchBar.frame animated:NO];
            return -1;
        } else {
            return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index-1];
        }
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    cellIdentifier = @"ContactsCell";
    UITableViewCell *cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    AddressBookDB *address;
    if(tableView == [[self searchDisplayController] searchResultsTableView])
    {
        address=(AddressBookDB*)[filteredResult objectAtIndex:indexPath.row];
    }
    else
    {
        address=(AddressBookDB*)[sectionedListContent objectAtIndexPath:indexPath];
    }
    cell.textLabel.text=address.fullName;

    cell.detailTextLabel.text = address.userStatus;
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [[[self searchDisplayController] searchBar] resignFirstResponder];
    
    AddressBookDB *address;
    if(tableView == [[self searchDisplayController] searchResultsTableView])
    {
        address=(AddressBookDB*)[filteredResult objectAtIndex:indexPath.row];
    }
    else
    {
        address=(AddressBookDB*)[sectionedListContent objectAtIndexPath:indexPath];
    }
    

    
    if ([address.originalUserId integerValue] == 0) {
         ContactsDetailViewController *contactDetail=[[ContactsDetailViewController alloc] initWithNibName:@"ContactsDetailViewController" bundle:nil];
        
        
        
        NSMutableArray *arrContacDetail=[[NSMutableArray alloc] init];
        
        NSArray *valuesPhone = nil;
        if (address.phone) {
            NSData* data = [address.phone dataUsingEncoding:NSUTF8StringEncoding];
            valuesPhone = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            
        }
        
        NSArray *valuesEmail = nil;
        if (address.email) {
            NSData* data = [address.email dataUsingEncoding:NSUTF8StringEncoding];
            valuesEmail = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            
        }
//        contactDetail.navigatefrom=PROFILE_PAGE_NAVIGATED_FROM_ADDRESS_BOOK;
        //    NSMutableDictionary *aDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:address.fullName,kfullName, nil];
        
        if (address.fullName) {
            contactDetail.strFullName=address.fullName;
        }
        if (valuesPhone) {
            NSMutableDictionary *aTemp=[NSMutableDictionary dictionaryWithObjectsAndKeys:valuesPhone,kPhoneNumber, nil];
            [arrContacDetail addObject:aTemp];
            
        }
        if (valuesEmail) {
            NSMutableDictionary *aTemp=[NSMutableDictionary dictionaryWithObjectsAndKeys:valuesEmail,kemail, nil];
            [arrContacDetail addObject:aTemp];
            
        }
        if (address.notes) {
            NSMutableDictionary *aTemp=[NSMutableDictionary dictionaryWithObjectsAndKeys:address.notes,kNotes, nil];
            [arrContacDetail addObject:aTemp];
            CGSize notesHeight=[AppManager frameForText:address.notes sizeWithFont:[UIFont fontWithName:ssFontHelveticaNeue size:17] constrainedToSize:CGSizeMake(250, 1000)];
            contactDetail.notesHeight=ceil(notesHeight.height);
        }
        
        NSMutableDictionary *aTemp=[NSMutableDictionary dictionaryWithObjectsAndKeys:@"Invite to UmmApp",@"button",[NSNumber numberWithInt:ADDRESSBOOK_DETAIL_BUTTON_TYPE_INVITE_TO_UMMAPP],@"type", nil];
        
        NSArray *arrtemp=[NSArray arrayWithObjects:aTemp, nil];
        NSDictionary *aTempOuter=[NSDictionary dictionaryWithObjectsAndKeys:arrtemp,@"button", nil];
        
        
        [arrContacDetail addObject:aTempOuter];
        
        contactDetail.strJobTitle=address.jobTitle;
        contactDetail.arrContactDetail=arrContacDetail;
        contactDetail.strProfilePic=address.profilePic;
        contactDetail.originalUserId=[address.originalUserId integerValue];
        contactDetail.strChatUserName=address.orgChatUsername;
        //    contactDetail.aDictContactDetail=aDict;
        
        [self.navigationController pushViewController:contactDetail animated:YES];
        
    }else{
       
       
        AppDelegate *deleg = APP_DELEGATE;
        SMChatViewController *chatView = nil;
        chatView = [deleg createChatViewByChatUserNameIfNeeded: address.orgChatUsername];
        chatView.chatWithUser =[[address.orgChatUsername stringByAppendingString:[NSString stringWithFormat:@"@%@",STRChatServerURL]]lowercaseString];
        chatView.userName = address.fullName;
        chatView.imageString = address.profilePic;
        chatView.isComingFromAddressBook = YES;
        chatView.friendNameId = [NSString stringWithFormat:@"%@",address.originalUserId];
        //    chatView.navigatedFrom=PROFILE_PAGE_NAVIGATED_FROM_ADDRESS_BOOK;
        chatView.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:chatView animated:YES];

    }
    
    
    
}



#pragma mark - Search Display Controller Delegates
-(void) filterForSearchText:(NSString *) text scope:(NSString *) scope
{
    filteredResult = nil; // clearing filter array
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"SELF.fullName contains[c] %@",text]; // Creating filter condition
    filteredResult = [NSMutableArray arrayWithArray:[arrAllContacts filteredArrayUsingPredicate:filterPredicate]]; // filtering result
}
-(BOOL) searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterForSearchText:searchString scope:[[[[self searchDisplayController] searchBar] scopeButtonTitles] objectAtIndex:[[[self searchDisplayController] searchBar] selectedScopeButtonIndex] ]];
    
    return YES;
}

-(BOOL) searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    [self filterForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    
    return YES;
}
//- (IBAction)btnBackClicked:(id)sender {
//    AppDelegate *app=(AppDelegate*)APP_DELEGATE;
//    [app removeFriendsTabbarView];
//}
@end
