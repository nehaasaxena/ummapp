//
//  ContactsDetailViewController.h
//  SocialParty
//
//  Created by pankaj on 01/09/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactDetailTableViewCell.h"
#import <MessageUI/MessageUI.h>
@protocol ContactsDetailViewDelegate<NSObject>
- (void)DeletedAllHistory;
@end
@interface ContactsDetailViewController: UIViewController<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,ContactDetailCellDelegate,UIActionSheetDelegate,UIGestureRecognizerDelegate>
{
    
    __weak IBOutlet UITableView *tableViewDetail;
    __weak IBOutlet UIButton *btnBack;
}
@property (nonatomic, weak) id<ContactsDetailViewDelegate> delegate;
@property (nonatomic, strong) NSArray *arrContactDetail;
@property (nonatomic, strong) NSString *strChatUserName;
@property (nonatomic, strong) NSString *strFullName;
@property (nonatomic, strong) NSString *strJobTitle;
@property (nonatomic) NSInteger originalUserId;
@property (nonatomic, strong) NSString *strProfilePic;
@property (nonatomic) NSInteger notesHeight;
//@property (nonatomic) ProfilePageNavigatedFrom navigatefrom;
- (IBAction)btnBackClicked:(id)sender;
@end
