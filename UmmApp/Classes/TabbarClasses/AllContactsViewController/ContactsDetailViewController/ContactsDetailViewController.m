//
//  ContactsDetailViewController.m
//  SocialParty
//
//  Created by pankaj on 01/09/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "ContactsDetailViewController.h"
#import "SMChatViewController.h"
#import "AddressBookDB.h"
#import <MessageUI/MessageUI.h> 
#import "ShowLargePhotoViewController.h"
#import "ShowFullProfileImageViewController.h"
@interface ContactsDetailViewController ()
{
    NSMutableArray *messages;
    NSMutableArray *arrPhotos;
}
@end

@implementation ContactsDetailViewController
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        arrPhotos = [[NSMutableArray alloc] init];
        messages = [[NSMutableArray alloc] init];
    }
    return self;
}
- (void)setNavigationBar
{
    self.navigationItem.hidesBackButton = YES;
    self.title = NSLocalizedString(@"Contact Info", @"Contact Info");
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 11, 20)];
    [backButton setImage:[UIImage imageNamed:@"arrow1"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(btnBackClicked) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
}
- (void)btnBackClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNavigationBar];
    [tableViewDetail setFrame:CGRectMake(0, 64, 320, [UIScreen mainScreen].bounds.size.height-80)];
    
    [self setTableViewHeader];
    
    // Do any additional setup after loading the view from its nib.
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if([arrPhotos count]==0)
    {
        [self getDatabaseImages];
    }
    if([messages count]==0)
    {
        messages = [[NSMutableArray alloc]initWithArray:[self fetchMsgByFetchingCursor]];
    }
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    self.tabBarController.tabBar.hidden = NO;
}
#pragma mark - UITableView delegate & Datasource
-(void)setTableViewHeader{
    CGSize postTextHeight=[AppManager frameForText:_strFullName sizeWithFont:[UIFont fontWithName:ssFontHelveticaNeue size:17] constrainedToSize:CGSizeMake(250, 1000)];
    
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, ceil(postTextHeight.height)+50)];
    UILabel *labelFullName=[[UILabel alloc] initWithFrame:CGRectMake(40, 10, 250, ceil(postTextHeight.height))];
    [labelFullName setNumberOfLines:1];
    [labelFullName setBackgroundColor:[UIColor clearColor]];
    [labelFullName setTextColor:[UIColor blackColor]];
    [labelFullName setText:_strFullName];
    [labelFullName setFont:[UIFont boldSystemFontOfSize:19]];
    [view addSubview:labelFullName];
    
    switch (_originalUserId) {
        case 0:
            if (_strJobTitle) {
                CGSize jobTitleTextHeight=[AppManager frameForText:_strJobTitle sizeWithFont:[UIFont fontWithName:ssFontHelveticaNeue size:17] constrainedToSize:CGSizeMake(250, 1000)];
                UILabel *lblJobTitle = [[UILabel alloc] initWithFrame:CGRectMake(40, labelFullName.frame.size.height+labelFullName.frame.origin.y, 250, ceil(jobTitleTextHeight.height))];
                [lblJobTitle setNumberOfLines:1];
                [lblJobTitle setBackgroundColor:[UIColor clearColor]];
                [lblJobTitle setTextColor:[UIColor blackColor]];
                [lblJobTitle setText:_strJobTitle];
                [lblJobTitle setFont:[UIFont systemFontOfSize:16]];
                [view addSubview:lblJobTitle];
                
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width,ceil(jobTitleTextHeight.height)+ceil(postTextHeight.height)+50)];
                
            }
            break;
            
        default:
            if (_strProfilePic) {
                
                CGSize jobTitleTextHeight=[AppManager frameForText:_strJobTitle sizeWithFont:[UIFont boldSystemFontOfSize:17] constrainedToSize:CGSizeMake(250, 1000)];
                UILabel *lblJobTitle = [[UILabel alloc] initWithFrame:CGRectMake(100, labelFullName.frame.size.height+labelFullName.frame.origin.y, 250, ceil(jobTitleTextHeight.height))];
                [lblJobTitle setNumberOfLines:1];
                [lblJobTitle setBackgroundColor:[UIColor clearColor]];
                [lblJobTitle setTextColor:[UIColor blackColor]];
                [lblJobTitle setText:_strJobTitle];
                [lblJobTitle setFont:[UIFont systemFontOfSize:16]];
                [view addSubview:lblJobTitle];

                
                [labelFullName setFrame:CGRectMake(100, 10, 210, ceil(postTextHeight.height))];
                UIImageView *imageProfilPic=[[UIImageView alloc] initWithFrame:CGRectMake(40, 10, 50, 50)];
                
                [imageProfilPic setImageWithURL:[NSURL URLWithString:_strProfilePic] placeholderImage:[UIImage imageNamed:@"chatDefaultPic"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                    
                }];
                [imageProfilPic.layer setCornerRadius:imageProfilPic.frame.size.width/2];
                [imageProfilPic.layer setMasksToBounds:YES];
                
                [view addSubview:imageProfilPic];
                
                UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                btn.frame = [imageProfilPic frame];
                btn.backgroundColor = [UIColor clearColor];
                [btn addTarget:self action:@selector(showProfile:) forControlEvents:UIControlEventTouchUpInside];
                [view addSubview:btn];
            }
            break;
    }
    
//    if (_strJobTitle) {
//        CGSize jobTitleTextHeight=[AppManager frameForText:_strJobTitle sizeWithFont:[UIFont fontWithName:ssFontHelveticaNeue size:17] constrainedToSize:CGSizeMake(250, 1000)];
//        UILabel *lblJobTitle = [[UILabel alloc] initWithFrame:CGRectMake(40, labelFullName.frame.size.height+labelFullName.frame.origin.y, 250, ceil(jobTitleTextHeight.height))];
//        [lblJobTitle setNumberOfLines:2];
//        [lblJobTitle setBackgroundColor:[UIColor clearColor]];
//        [lblJobTitle setTextColor:[UIColor blackColor]];
//        [lblJobTitle setText:_strJobTitle];
//        [lblJobTitle setFont:[UIFont systemFontOfSize:16]];
//        [view addSubview:lblJobTitle];
//        
//        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width,ceil(jobTitleTextHeight.height)+ceil(postTextHeight.height)+50)];
//
//    }
    
    
    tableViewDetail.tableHeaderView=view;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    CGSize postTextHeight=[AppManager frameForText:_strFullName sizeWithFont:[UIFont fontWithName:ssFontHelveticaNeue size:14] constrainedToSize:CGSizeMake(300, 1000)];
//    return (ceil(postTextHeight.height)) +10;
//}
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    CGSize postTextHeight=[AppManager frameForText:_strFullName sizeWithFont:[UIFont fontWithName:ssFontHelveticaNeue size:14] constrainedToSize:CGSizeMake(300, 1000)];
//
//    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(10, 0, 300, ceil(postTextHeight.height))];
//    UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, view.frame.size.height)];
//    [label setNumberOfLines:10];
//    [label setBackgroundColor:[UIColor clearColor]];
//    [label setTextColor:[UIColor blackColor]];
//    [label setText:_strFullName];
//    [view addSubview:label];
//    return view;
//    
//}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_arrContactDetail count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[[_arrContactDetail objectAtIndex:section] valueForKey:[[[_arrContactDetail objectAtIndex:section] allKeys] objectAtIndex:0]] isKindOfClass:[NSString class]]) {
        return 1;
    }else
        return [[[_arrContactDetail objectAtIndex:section] valueForKey:[[[_arrContactDetail objectAtIndex:section] allKeys] objectAtIndex:0]] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([[[_arrContactDetail objectAtIndex:indexPath.section] valueForKey:[[[_arrContactDetail objectAtIndex:indexPath.section] allKeys] objectAtIndex:0]] isKindOfClass:[NSString class]]) {
        return _notesHeight+30; // for Notes
    }else
        return 50;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"CellContactDetail";
    
    ContactDetailTableViewCell *contactCell=(ContactDetailTableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
        if (contactCell == nil)
        {
                contactCell =[[ContactDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier ];
                [contactCell setSelectionStyle:UITableViewCellSelectionStyleGray];
            [contactCell setBackgroundColor:[UIColor clearColor]];
        }
        contactCell.tag=indexPath.row;
        contactCell.delegate=self;
    

    if ([[[_arrContactDetail objectAtIndex:indexPath.section] valueForKey:[[[_arrContactDetail objectAtIndex:indexPath.section] allKeys] objectAtIndex:0]] isKindOfClass:[NSString class]]) {
//        NSLog(@"Yes");
        contactCell.notesHeight=_notesHeight;
        [contactCell UpdateCellContent:[_arrContactDetail objectAtIndex:indexPath.section]  ofContactType:[[[_arrContactDetail objectAtIndex:indexPath.section] allKeys] objectAtIndex:0] andIndexPath:indexPath];
        
        
    }else{
    
        contactCell.notesHeight=0;
        [contactCell UpdateCellContent:[[[_arrContactDetail objectAtIndex:indexPath.section] valueForKey:[[[_arrContactDetail objectAtIndex:indexPath.section] allKeys] objectAtIndex:0]] objectAtIndex:indexPath.row] ofContactType:[[[_arrContactDetail objectAtIndex:indexPath.section] allKeys] objectAtIndex:0] andIndexPath:indexPath];
        
    }
    
    return contactCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
#pragma mark - Cell Delegate
-(void)MessageButtonDelegate:(NSString*)chatUserName{
    [self InitiateChatting];
}
-(void)CallNumberButtonDelegate:(NSString*)number{
    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:number];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}
-(void)InviteToUmmAppDelegate{
    
    NSString *strMessage=NSLocalizedString(@"I want to add you as a friend on UmmApp.", @"I want to add you as a friend on UmmApp.") ;
    
    if ([[[[_arrContactDetail objectAtIndex:0] allKeys] objectAtIndex:0] isEqualToString:@"phone"]) {
        
        NSString *contact=[[[[_arrContactDetail objectAtIndex:0] valueForKey:@"phone"] objectAtIndex:0] valueForKey:@"number"];
        MFMessageComposeViewController *mfMessageComposer=[[MFMessageComposeViewController alloc] init];
        if([MFMessageComposeViewController canSendText])
        {
            mfMessageComposer.body = strMessage;
            mfMessageComposer.recipients = [NSArray arrayWithObjects:contact?contact:@"", nil];
            mfMessageComposer.messageComposeDelegate = self;
            [self presentViewController:mfMessageComposer animated:YES completion:nil];
        }
    }else{
         NSString *contact=[[[[_arrContactDetail objectAtIndex:0] valueForKey:@"email"] objectAtIndex:0] valueForKey:@"number"];
        [self actionEmailComposer:strMessage Subject:NSLocalizedString(@"UmmApp Invitation", @"UmmApp Invitation")  andRecipients:[NSArray arrayWithObjects:contact, nil]];
    }

   
}
-(void)ClearConversationDelegate:(NSString*)param{
    if([messages count]>0)
    {
//        [NSString stringWithFormat:NSLocalizedString(@"Yesterday you sold %@ apps", nil), numberString];
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Delete All Chat History with \"%@\"",nil),self.strFullName] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")  destructiveButtonTitle:NSLocalizedString(@"Clear Conversation", @"Clear Conversation")  otherButtonTitles:nil];
        [actionSheet showInView:self.view];

    }
    else
    {
        [Utils showAlertView:kAlertTitle message:NSLocalizedString(@"No conversation found." , @"No conversation found." ) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK")  otherButtonTitles:nil];
    }

 }
- (void) clearConversation
{
        [[gCXMPPController xmppMessageArchivingStorage] deleteHistory:[self.strChatUserName stringByAppendingString:[NSString stringWithFormat:@"@%@",STRChatServerURL]]];
        [Utils stopActivityIndicatorInView:self.view];
        if ([delegate respondsToSelector:@selector(DeletedAllHistory)]) [delegate DeletedAllHistory];
//        [self updateLastMessage];
        [messages removeAllObjects];
}
-(void)ViewAllMediaDelegate{
    if([arrPhotos count]==0)
        return;
    ShowLargePhotoViewController *showLargeImageView;
    if ([Utils isIPhone5]) {
        showLargeImageView = [[ShowLargePhotoViewController alloc] initWithNibName:@"ShowLargePhotoViewController" bundle:nil];
    }else{
        showLargeImageView = [[ShowLargePhotoViewController alloc] initWithNibName:@"ShowLargePhotoViewController_iPhone4" bundle:nil];
    }
    
//    [AppManager sharedManager].isFromChat= YES;
    
    showLargeImageView.arrayImages = [NSMutableArray arrayWithArray:arrPhotos];
    [AppManager sharedManager].arrImages=[NSMutableArray arrayWithArray:arrPhotos];
    showLargeImageView.strTotalPhotoCount=[NSString stringWithFormat:@"%lu",(unsigned long)arrPhotos.count];
    showLargeImageView.hidesBottomBarWhenPushed=YES;
//    showLargeImageView.navigateFrom=self.navigatefrom;
    //    NSInteger index = 0;
    showLargeImageView.intArrayIndex = 0;
    
    [showLargeImageView setInitializePageViewController];
    self.navigationController.navigationBarHidden = YES;
    [self.navigationController pushViewController:showLargeImageView animated:YES];
}
- (void)getDatabaseImages
{
    [arrPhotos removeAllObjects];
    NSManagedObjectContext *moc = [gCXMPPController messagesStoreMainThreadManagedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject"
                                                         inManagedObjectContext:moc];
    NSString *user = nil;
    if([[self.strChatUserName stringByAppendingString:[NSString stringWithFormat:@"@%@",STRChatServerURL]] rangeOfString:@"/"].length>0)
    {
        user = [[[self.strChatUserName stringByAppendingString:[NSString stringWithFormat:@"@%@",STRChatServerURL]] componentsSeparatedByString:@"/"] objectAtIndex:0];
    }
    else
    {
        user = [self.strChatUserName stringByAppendingString:[NSString stringWithFormat:@"@%@",STRChatServerURL]];
    }
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bareJidStr == %@ and streamBareJidStr == %@ and mediaType == \"image\" and konnect == 0",user, [[[NSString stringWithFormat:@"%@",[gCXMPPController.xmppStream myJID]]componentsSeparatedByString:@"/"] objectAtIndex:0]];
    
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    
    //14-5-14
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:YES];
    [request setSortDescriptors:[NSArray arrayWithObjects: sortDescriptor, nil]];
    //end
    
    [request setEntity:entityDescription];
    [request setPredicate:predicate];
    
    NSError *error;
    NSMutableArray *aMessage = [[NSMutableArray alloc] init ];
    [aMessage addObjectsFromArray: [moc executeFetchRequest:request error:&error]];
    for(XMPPMessageArchiving_Message_CoreDataObject *message in aMessage)
    {
        if([message.fileaction isEqualToString:@"downloaded"]||[message.fileaction isEqualToString:@"uploaded"])
        {
            if([message.outgoing boolValue]==YES)
            {
                NewPhoto *photo = [[NewPhoto alloc] init];
                photo.originalUrl = message.filelocalpath;
                [arrPhotos addObject:photo];
            }
            else
            {
                if([message.fileaction isEqualToString:@"downloaded"])
                {
                    NewPhoto *photo = [[NewPhoto alloc] init];
                    photo.originalUrl = message.filelocalpath;
                    [arrPhotos addObject:photo];
                }
            }
        }
    }
    [tableViewDetail reloadData];
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [Utils startActivityIndicatorInView:self.view withMessage:NSLocalizedString(@"This will take several seconds.", @"This will take several seconds.") ];
        [self performSelector:@selector(clearConversation) withObject:nil afterDelay:0.1];
    }
}
#pragma mark - Email Conversation Methods
-(void)EmailConversationDelegate:(NSString*)param{
    if([messages count]>0)
    {
        [Utils startActivityIndicatorInView:self.view withMessage:NSLocalizedString(@"This will take several seconds.", @"This will take several seconds.")];
        [self performSelector:@selector(createDoc) withObject:nil afterDelay:0.1];
    }
    else
    {
        [Utils showAlertView:kAlertTitle message:NSLocalizedString(@"No conversation found.", @"No conversation found.") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }

}

-(void)createDoc
{
    
    NSString *messageDocString = @"";
    for(NSDictionary *dict in messages)
    {
        if(![[dict allKeys]containsObject:@"media"])
        {
            if([[dict objectForKey:@"sender"]isEqualToString:@"you"])
            {
                messageDocString = [messageDocString stringByAppendingString:[NSString stringWithFormat:@"%@ %@: %@: %@\n",[Utils stringFromDateToEmail:[dict objectForKey:@"timestamp"]],[dict objectForKey:@"time"],[[NSUserDefaults standardUserDefaults] valueForKey:kFullName],[dict objectForKey:@"msg"]]];
            }
            else
            {
                messageDocString = [messageDocString stringByAppendingString:[NSString stringWithFormat:@"%@ %@: %@: %@\n",[Utils stringFromDateToEmail:[dict objectForKey:@"timestamp"]],[dict objectForKey:@"time"],self.strFullName,[dict objectForKey:@"msg"]]];
            }
        }
    }
//    NSLog(@"%@",messageDocString);
    NSString *logPath = [self filePath];
    [messageDocString writeToFile:logPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    [self showEmail:nil];
}
-(NSString *)filePath
{
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [documentPaths objectAtIndex:0];
    NSString *logPath = [[NSString alloc] initWithFormat:@"%@",[documentsDir stringByAppendingPathComponent:[NSString stringWithFormat: @"%@.txt",self.strFullName]]];
    return logPath;
}
- (void)showEmail:(NSString*)file {
    
    NSString *emailTitle = [NSString stringWithFormat:NSLocalizedString(@"UmmApp Chat with %@", nil) ,self.strFullName];
    NSString *messageBody = [NSString stringWithFormat:NSLocalizedString(@"Chat history is attached as: \"%@.txt\" file to this email", nil),self.strFullName];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    
    // Get the resource path and read the file using NSData
    NSString *filePath = [self filePath];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    
    // Determine the MIME type
    NSString *mimeType;
    mimeType = @"text/plain";
    [mc addAttachmentData:fileData mimeType:mimeType fileName:[NSString stringWithFormat:@"%@.txt",self.strFullName]];
    [Utils stopActivityIndicatorInView:self.view];
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
    
}
#pragma mark - Other Methods
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)InitiateChatting{
    AppDelegate *deleg = APP_DELEGATE;
    SMChatViewController *chatView = nil;
    chatView = [deleg createChatViewByChatUserNameIfNeeded: _strChatUserName];
    chatView.chatWithUser =[[_strChatUserName stringByAppendingString:[NSString stringWithFormat:@"@%@",STRChatServerURL]]lowercaseString];
    chatView.userName = _strFullName;
    chatView.imageString = _strProfilePic;
    chatView.isComingFromAddressBook = YES;
    chatView.friendNameId = [NSString stringWithFormat:@"%li",(long)_originalUserId];
    chatView.hidesBottomBarWhenPushed=YES;
    //    chatView.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:chatView animated:YES];
}
- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)actionEmailComposer:(NSString*) msg1 Subject:(NSString*)sub andRecipients:(NSArray*)arrRecipients
{
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setSubject:sub];
    [controller setMessageBody:msg1 isHTML:YES];
    [controller setToRecipients:arrRecipients];
    
    if (controller) [self presentViewController:controller animated:YES completion:nil];
    
    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            [self removeFileFromFilePath];
            //NSLog(@"Result: canceled");
            break;
        case MFMailComposeResultSaved:
            //NSLog(@"Result: saved");

            break;
        case MFMailComposeResultSent:
            //NSLog(@"Result: sent");
            [self removeFileFromFilePath];

            break;
        case MFMailComposeResultFailed:
            //NSLog(@"Result: failed");
            [self removeFileFromFilePath];

            break;
        default:
            //NSLog(@"Result: not sent");
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    NSLog(@"Messaged Successfully");
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)showProfile:(id)sender
{
    ShowFullProfileImageViewController *showFullImage =nil;
    showFullImage = [[ShowFullProfileImageViewController alloc] initWithNibName:nil bundle:nil];
    showFullImage.imgString =self.strProfilePic;
    showFullImage.isComingFromChat = NO;
    [self.navigationController pushViewController:showFullImage animated:YES];

//    PersonPageViewController *personVC= [[PersonPageViewController alloc] init];
//    
//    
//    Person *person = [[Person alloc] init];
//    person.userId = _originalUserId;
//    person.profilePic = _strProfilePic;
//    personVC.profilePageNavigatedFrom = PROFILE_PAGE_NAVIGATED_FROM_ADDRESS_BOOK;
//    personVC.person = person;
//    personVC.hidesBottomBarWhenPushed=YES;
//    [self.navigationController pushViewController:personVC animated:YES];
}
-(NSMutableArray *)fetchMsgByFetchingCursor
{
    NSMutableArray *aMessage = nil;
    NSMutableArray *tempArr = [[NSMutableArray alloc] init];
    
    @autoreleasepool {
        
        NSManagedObjectContext *moc = [gCXMPPController messagesStoreMainThreadManagedObjectContext];
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject"
                                                             inManagedObjectContext:moc];
        NSString *user = nil;
        if([[self.strChatUserName stringByAppendingString:STRChatServerURL] rangeOfString:@"/"].length>0)
        {
            user = [[[self.strChatUserName stringByAppendingString:[NSString stringWithFormat:@"@%@",STRChatServerURL]] componentsSeparatedByString:@"/"] objectAtIndex:0];
        }
        else
        {
            user = [self.strChatUserName stringByAppendingString:[NSString stringWithFormat:@"@%@",STRChatServerURL]];
        }
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bareJidStr == %@ and streamBareJidStr == %@ and blackMessage = 'no' and konnect == 0",user, [[[NSString stringWithFormat:@"%@",[gCXMPPController.xmppStream myJID]]componentsSeparatedByString:@"/"] objectAtIndex:0]];
        
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
        
        //14-5-14
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:YES];
        [request setSortDescriptors:[NSArray arrayWithObjects: sortDescriptor, nil]];
        //end
        
        [request setEntity:entityDescription];
        [request setPredicate:predicate];
        
        
        
        NSError *error;
        aMessage = [[NSMutableArray alloc] init ];
        [aMessage addObjectsFromArray: [moc executeFetchRequest:request error:&error]];
        //[moc executeFetchRequest:request error:&error];
        for (XMPPMessageArchiving_Message_CoreDataObject *message in aMessage) {
            
            NSString *m = message.body;
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            if(m !=NULL)
            {
                [dic setObject:m forKey:@"msg"];
            }
            else
            {
                [dic setObject:@" " forKey:@"msg"];
            }
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"hh:mm a"];
            NSString *dateString = [dateFormat stringFromDate: message.dispatchTimestamp];
            [dic setObject:dateString forKey:@"time"];
            
            NSDateFormatter *date_formater=[[NSDateFormatter alloc]init];
            [date_formater setDateFormat:@"yyyy-MM-dd"];
            
            NSString *aDateStr = [date_formater stringFromDate: message.timestamp];
            [dic setObject:[date_formater dateFromString:aDateStr ] forKey:@"timestamp"];
            
            //end
            if([message.outgoing boolValue]==YES)
            {
                [dic setObject:@"you" forKey:@"sender"];
                if(message.mediaType!=nil)
                {
                    [dic setObject:message.mediaType forKey:@"media"];
                }
            }
            else
            {
                [dic setObject:user forKey:@"sender"];
                if(message.mediaType!=nil)
                {
                    [dic setObject:message.mediaType forKey:@"media"];
                }
            }
            [tempArr addObject:dic];
        }
    }
    return tempArr;
}
- (void)removeFileFromFilePath
{
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success = [fileManager removeItemAtPath:[self filePath] error:&error];
    if (success) {
        
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}
-(void)updateLastMessage
{
//    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:self.strChatUserName,kchatUserName,@"",@"message", [NSDate date],@"sendDate",nil];
//    [[Database database]addMessage:dictionary withMessage:@"" fromPrivate:NO];
}
@end
