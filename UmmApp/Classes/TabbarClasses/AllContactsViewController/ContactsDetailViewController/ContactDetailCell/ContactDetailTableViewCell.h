//
//  ContactDetailTableViewCell.h
//  SocialParty
//
//  Created by pankaj on 02/09/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ContactDetailCellDelegate <NSObject>

-(void)CallNumberButtonDelegate:(NSString*)number;
-(void)MessageButtonDelegate:(NSString*)chatUserName;
-(void)InviteToUmmAppDelegate;
-(void)ClearConversationDelegate:(NSString*)param;
-(void)EmailConversationDelegate:(NSString*)param;
-(void)ViewAllMediaDelegate;
@end

@interface ContactDetailTableViewCell : UITableViewCell
{
    UILabel *lblType;
    UILabel *lblNumberOrEmail;
    UIButton *btnCall;
    UIButton *btnMessage;
    
    UIButton *btnViewAllMedia;
    UIButton *btnSendMessage;
    UIButton *btnEmailConversation;
    UIButton *btnClearConversation;
    UIButton *btnInviteToUMMAPP;
    UILabel *horLine;
    
}

-(void)UpdateCellContent:(NSDictionary*)aDict ofContactType:(NSString*)strContactType andIndexPath:(NSIndexPath*)indexPath;
@property (weak, nonatomic) id<ContactDetailCellDelegate> delegate;
@property (strong, nonatomic) NSDictionary *aDictDetails;
@property (strong, nonatomic) NSString *contactType;
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (nonatomic) NSInteger notesHeight;
@end
