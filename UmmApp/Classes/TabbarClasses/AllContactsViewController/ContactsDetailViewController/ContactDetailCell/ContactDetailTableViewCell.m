//
//  ContactDetailTableViewCell.m
//  SocialParty
//
//  Created by pankaj on 02/09/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "ContactDetailTableViewCell.h"

@implementation ContactDetailTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code

        lblType=[[UILabel alloc] initWithFrame:CGRectMake(40, 4, 200, 20)];
        [lblType setNumberOfLines:1];
        [lblType setBackgroundColor:[UIColor clearColor]];
        [lblType setTextColor:[UIColor colorWithRed:35/255.0f green:110/255.0f  blue:244/255.0f  alpha:1.0]];
        [lblType setFont:[UIFont systemFontOfSize:15]];
        [self addSubview:lblType];
        
        lblNumberOrEmail=[[UILabel alloc] initWithFrame:CGRectMake(40, 26, 230, 20)];
        [lblNumberOrEmail setNumberOfLines:1];
        [lblNumberOrEmail setBackgroundColor:[UIColor clearColor]];
        [lblNumberOrEmail setTextColor:[UIColor blackColor]];
        [lblNumberOrEmail setFont:[UIFont systemFontOfSize:17]];
        [self addSubview:lblNumberOrEmail];
        
        btnCall = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnCall setFrame:CGRectMake(276, 3, 44, 44)];
        [btnCall addTarget:self action:@selector(btnCallNumberClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btnCall setImage:[UIImage imageNamed:@"friendsContactIcon.png"] forState:UIControlStateNormal];
        [self addSubview:btnCall];
        
        btnMessage = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnMessage setFrame:CGRectMake(232, 3, 44, 44)];
        [btnMessage addTarget:self action:@selector(btnMessageClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btnMessage setImage:[UIImage imageNamed:@"friendsMsgIcon.png"] forState:UIControlStateNormal];
        [self addSubview:btnMessage];
//messIcon.png"
        
        horLine=[[UILabel alloc] initWithFrame:CGRectMake(40, 49, 280, 1)];
        [horLine setBackgroundColor:[UIColor colorWithRed:239/255.0f green:239/255.0f blue:239/255.0f alpha:1.0]];
        [self addSubview:horLine];
        
        
        btnViewAllMedia = [UIButton buttonWithType:UIButtonTypeSystem];
        [btnViewAllMedia setFrame:CGRectMake(40, 3, 200, 44)];
        [btnViewAllMedia addTarget:self action:@selector(btnViewAllMedia:) forControlEvents:UIControlEventTouchUpInside];
        [btnViewAllMedia setTitle:NSLocalizedString(@"View All Media", @"View All Media")  forState:UIControlStateNormal];
        [btnViewAllMedia setBackgroundColor:[UIColor clearColor]];
        btnViewAllMedia.titleLabel.font=[UIFont systemFontOfSize:17];
        btnViewAllMedia.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self addSubview:btnViewAllMedia];
        
        
        btnInviteToUMMAPP = [UIButton buttonWithType:UIButtonTypeSystem];
        [btnInviteToUMMAPP setFrame:CGRectMake(40, 3, 200, 44)];
        [btnInviteToUMMAPP addTarget:self action:@selector(btnInviteToUMMAPPClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btnInviteToUMMAPP setTitle:NSLocalizedString(@"Invite to UmmApp", @"Invite to UmmApp") forState:UIControlStateNormal];
        [btnInviteToUMMAPP setBackgroundColor:[UIColor clearColor]];
        btnInviteToUMMAPP.titleLabel.font=[UIFont systemFontOfSize:17];
        btnInviteToUMMAPP.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
        btnInviteToUMMAPP.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        [self addSubview:btnInviteToUMMAPP];
        
        btnSendMessage = [UIButton buttonWithType:UIButtonTypeSystem];
        [btnSendMessage setFrame:CGRectMake(40, 3, 200, 44)];
        [btnSendMessage addTarget:self action:@selector(btnMessageClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btnSendMessage setTitle:NSLocalizedString(@"Send Message", @"Send Message") forState:UIControlStateNormal];
        [btnSendMessage setBackgroundColor:[UIColor clearColor]];
        btnSendMessage.titleLabel.font=[UIFont systemFontOfSize:17];
         btnSendMessage.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self addSubview:btnSendMessage];
        
        btnEmailConversation = [UIButton buttonWithType:UIButtonTypeSystem];
        [btnEmailConversation setFrame:CGRectMake(40, 3, 200, 44)];
        [btnEmailConversation addTarget:self action:@selector(btnEmailConverstaionClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btnEmailConversation setTitle:NSLocalizedString(@"Email Conversation", @"Email Conversation") forState:UIControlStateNormal];
        [btnEmailConversation setBackgroundColor:[UIColor clearColor]];
         btnEmailConversation.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btnEmailConversation.titleLabel.font=[UIFont systemFontOfSize:17];
        [self addSubview:btnEmailConversation];
        
        btnClearConversation = [UIButton buttonWithType:UIButtonTypeSystem];
        [btnClearConversation setFrame:CGRectMake(40, 3, 200, 44)];
        [btnClearConversation addTarget:self action:@selector(btnClearConversationClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnClearConversation.titleLabel.font=[UIFont systemFontOfSize:17];
        [btnClearConversation setTitle:NSLocalizedString(@"Clear Conversation", @"Clear Conversation") forState:UIControlStateNormal];
        [btnClearConversation setBackgroundColor:[UIColor clearColor]];
        btnClearConversation.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self addSubview:btnClearConversation];
    }
    return self;
}

-(void)UpdateCellContent:(NSDictionary*)aDict ofContactType:(NSString*)strContactType andIndexPath:(NSIndexPath*)indexPath{
    
    _indexPath=indexPath;
    _aDictDetails=aDict;
    if ([strContactType isEqualToString:@"phone"]) {
        [btnCall setHidden:NO];
        
        [lblType setHidden:NO];
        [lblNumberOrEmail setHidden:NO];
        
        CGRect frame=lblNumberOrEmail.frame;
        frame.size.height=20;
        frame.size.width = 230;
        [lblNumberOrEmail setFrame:frame];
        
        lblType.text=NSLocalizedString(@"Mobile", @"Mobile");
        lblNumberOrEmail.text=[aDict valueForKey:@"number"];
        [lblNumberOrEmail setNumberOfLines:1];
        [btnInviteToUMMAPP setHidden:YES];
        [btnSendMessage setHidden:YES];
        [btnEmailConversation setHidden:YES];
        [btnClearConversation setHidden:YES];
        [btnViewAllMedia setHidden:YES];
        
        
        _contactType=@"phone";

    }else if ([strContactType isEqualToString:@"email"]){
        
        [lblType setHidden:NO];
        [lblNumberOrEmail setHidden:NO];
        [btnCall setHidden:YES];
        
        CGRect lineframe=CGRectMake(40, 49, 280, 1);
        [horLine setFrame:lineframe];

        CGRect frame=lblNumberOrEmail.frame;
        frame.size.height=20;
        frame.size.width = 300;
      
        [lblNumberOrEmail setFrame:frame];
        [lblNumberOrEmail setNumberOfLines:1];
        
        lblType.text=NSLocalizedString(@"Email", @"Email");
        lblNumberOrEmail.text=[aDict valueForKey:@"number"];
        
        [btnInviteToUMMAPP setHidden:YES];
        [btnSendMessage setHidden:YES];
        [btnEmailConversation setHidden:YES];
        [btnClearConversation setHidden:YES];
         [btnViewAllMedia setHidden:YES];
        _contactType=@"email";
        
    }else if ([strContactType isEqualToString:@"notes"]){
        
        [lblType setHidden:NO];
        [lblNumberOrEmail setHidden:NO];
        
        CGRect frame=lblNumberOrEmail.frame;
        frame.size.height=_notesHeight;
        [lblNumberOrEmail setFrame:frame];
        [lblNumberOrEmail setNumberOfLines:10];
        
        CGRect lineframe=horLine.frame;
        lineframe.origin.y= frame.origin.y+frame.size.height+5;
        [horLine setFrame:lineframe];
        
       
        [btnCall setHidden:YES];
        lblType.text=NSLocalizedString(strContactType, strContactType);
        lblNumberOrEmail.text=[aDict valueForKey:strContactType];
        
        [btnInviteToUMMAPP setHidden:YES];
        [btnSendMessage setHidden:YES];
        [btnEmailConversation setHidden:YES];
        [btnClearConversation setHidden:YES];
         [btnViewAllMedia setHidden:YES];
        
        _contactType=@"";
        
    }else if ([strContactType isEqualToString:@"button"]){
        
        [btnCall setHidden:YES];
        CGRect lineframe=CGRectMake(40, 49, 280, 1);
        [horLine setFrame:lineframe];
        
        [lblType setHidden:YES];
        [lblNumberOrEmail setHidden:YES];
        
        _contactType=@"";
        
        switch ([[aDict valueForKey:@"type"] integerValue]) {
            case ADDRESSBOOK_DETAIL_BUTTON_TYPE_INVITE_TO_UMMAPP:
                [btnInviteToUMMAPP setHidden:NO];
                [btnSendMessage setHidden:YES];
                [btnEmailConversation setHidden:YES];
                [btnClearConversation setHidden:YES];
                 [btnViewAllMedia setHidden:YES];
                break;
            case ADDRESSBOOK_DETAIL_BUTTON_TYPE_SEND_MESSAGE:
                [btnInviteToUMMAPP setHidden:YES];
                [btnSendMessage setHidden:NO];
                [btnEmailConversation setHidden:YES];
                [btnClearConversation setHidden:YES];
                 [btnViewAllMedia setHidden:YES];
                break;
            case ADDRESSBOOK_DETAIL_BUTTON_TYPE_EMAIL_CONVERSTAION:
                [btnInviteToUMMAPP setHidden:YES];
                [btnSendMessage setHidden:YES];
                [btnEmailConversation setHidden:NO];
                [btnClearConversation setHidden:YES];
                 [btnViewAllMedia setHidden:YES];
                break;
            case ADDRESSBOOK_DETAIL_BUTTON_TYPE_CLEAR_CONVERSATION:
                [btnInviteToUMMAPP setHidden:YES];
                [btnSendMessage setHidden:YES];
                [btnEmailConversation setHidden:YES];
                [btnClearConversation setHidden:NO];
                 [btnViewAllMedia setHidden:YES];
                break;
            case ADDRESSBOOK_DETAIL_BUTTON_TYPE_VIEW_ALL_MEDIA:
                [btnInviteToUMMAPP setHidden:YES];
                [btnSendMessage setHidden:YES];
                [btnEmailConversation setHidden:YES];
                [btnClearConversation setHidden:YES];
                 [btnViewAllMedia setHidden:NO];
                break;
            default:
                break;
        }
    }
    
    
    if ([[_aDictDetails valueForKey:@"chatUsername"] length]>0) {
        [btnMessage setHidden:[btnSendMessage isHidden]];
    }else
        [btnMessage setHidden:YES];
    
}
-(void)btnCallNumberClicked:(UIButton*)sender{
    if ([self.delegate respondsToSelector:@selector(CallNumberButtonDelegate:)]) [self.delegate CallNumberButtonDelegate:[_aDictDetails valueForKey:@"number"]];
}
-(void)btnMessageClicked:(UIButton*)sender{
    if ([self.delegate respondsToSelector:@selector(MessageButtonDelegate:)]) [self.delegate MessageButtonDelegate:nil];
}
-(void)btnInviteToUMMAPPClicked:(UIButton*)sender{
      if ([self.delegate respondsToSelector:@selector(InviteToUmmAppDelegate)]) [self.delegate InviteToUmmAppDelegate];
    
}
-(void)btnEmailConverstaionClicked:(UIButton*)sender{
    if ([self.delegate respondsToSelector:@selector(EmailConversationDelegate:)]) [self.delegate EmailConversationDelegate:nil];
}
-(void)btnClearConversationClicked:(UIButton*)sender{
     if ([self.delegate respondsToSelector:@selector(ClearConversationDelegate:)]) [self.delegate ClearConversationDelegate:nil];
}
-(void)btnViewAllMedia:(UIButton*)sender{
    if ([self.delegate respondsToSelector:@selector(ViewAllMediaDelegate)]) [self.delegate ViewAllMediaDelegate];
}
@end
