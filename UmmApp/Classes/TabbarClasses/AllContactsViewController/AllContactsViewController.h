//
//  AllContactsViewController.h
//  SocialParty
//
//  Created by pankaj on 18/08/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllContactsViewController : UIViewController<UIGestureRecognizerDelegate>{
    
    __weak IBOutlet UIButton *btnBackReference;
    __weak IBOutlet UITableView *contactsTableView;
    NSArray *arrAllContacts;
    NSMutableArray *sectionedListContent;
    NSArray *filteredResult;
    UIActivityIndicatorView *activityIndicatorView;
}
- (IBAction)btnBackClicked:(id)sender;
-(void)refreshArrayOfAllContacts;
@end
