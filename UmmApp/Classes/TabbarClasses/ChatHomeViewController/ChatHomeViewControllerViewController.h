//
//  ChatHomeViewControllerViewController.h
//  UmmApp
//
//  Created by Neha Saxena on 20/10/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressBookDB.h"
#import "PhoneBookViewController.h"
@interface ChatHomeViewControllerViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UISearchBarDelegate,PhoneBookDelegate>
{
    IBOutlet UISearchBar *searchBarChatFriend;
}
@property (nonatomic, retain) NSMutableArray *friends;
@property (strong, nonatomic) IBOutlet UITableView *tblViewMessages;
@property (nonatomic, retain)NSMutableArray *arrFriends;
@property (nonatomic, retain)NSMutableArray *arrSearchResults;
//@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
//@property (strong, nonatomic) IBOutlet UILabel *lblNoMatchFound;

@end
