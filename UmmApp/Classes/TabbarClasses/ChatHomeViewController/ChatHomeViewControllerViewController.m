//
//  ChatHomeViewControllerViewController.m
//  UmmApp
//
//  Created by Neha Saxena on 20/10/2014.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "ChatHomeViewControllerViewController.h"
#import "StatusViewController.h"
#import "AddressBookDB.h"

@interface ChatHomeViewControllerViewController ()
{
    AppDelegate *appDelegate;
    UIRefreshControl *refreshContactList;
    ViewStatus viewStatus;
    BOOL isLocalSearching;
}
@end

@implementation ChatHomeViewControllerViewController
@synthesize tblViewMessages;
@synthesize arrFriends;
@synthesize arrSearchResults;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        appDelegate = APP_DELEGATE;
        // Custom initialization
    }
    return self;
}

-(void)setNavigationBar
{
//    CGRect headerTitleSubtitleFrame = CGRectMake(20, 0, 150, 44);
//    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
//    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
//    _headerTitleSubtitleView.autoresizesSubviews = NO;
//    
//    CGRect titleFrame = CGRectMake(20,0, 150, 44);
//    UILabel *titleView = [[UILabel alloc] initWithFrame:titleFrame];
//    titleView.backgroundColor = [UIColor clearColor];
//    titleView.font = [UIFont fontWithName:ssFontBrushScriptStd size:20];
//    titleView.textAlignment = NSTextAlignmentCenter;
//    titleView.textColor = [UIColor whiteColor];
//    titleView.text = @"Chats";
//    titleView.adjustsFontSizeToFitWidth = YES;
//    [_headerTitleSubtitleView addSubview:titleView];
//    self.navigationItem.titleView = _headerTitleSubtitleView;
    self.navigationController.navigationItem.title = @"Chats";
    
    
    UIImage *statusImage = [UIImage imageNamed:@"chatMsgIcon"];
    UIButton *face = [UIButton buttonWithType:UIButtonTypeCustom];
    face.bounds = CGRectMake( 0, 0, 40, 30 );
    [face setImage:statusImage forState:UIControlStateNormal];
    [face addTarget:self action:@selector(changeStatus) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *statusBtn = [[UIBarButtonItem alloc] initWithCustomView:face];
    
    UIImage *addNewImage = [UIImage imageNamed:@"chatCompose"];
    UIButton *addNew = [UIButton buttonWithType:UIButtonTypeCustom];
    addNew.bounds = CGRectMake( 0, 0, 30, 30);
    [addNew setImage:addNewImage forState:UIControlStateNormal];
    [addNew addTarget:self action:@selector(addUser) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *addNewBtn = [[UIBarButtonItem alloc] initWithCustomView:addNew];


    
    NSArray *arrBtns = [[NSArray alloc]initWithObjects:addNewBtn,statusBtn, nil];
    self.navigationItem.rightBarButtonItems = arrBtns;
    

//    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
//    [backButton setImage:[UIImage imageNamed:@"chatMsgIcon"] forState:UIControlStateNormal];
//    [backButton addTarget:self action:@selector(changeStatus) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [self.editButtonItem setTintColor:[UIColor colorWithRed:16.0/255.0 green:170.0/255.0 blue:180.0/255.0 alpha:1.0]];
    self.navigationItem.rightBarButtonItem.enabled = YES;
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
}
- (void)viewSetup
{
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    [tableViewController setTableView: self.tblViewMessages];
    [searchBarChatFriend setDelegate:self];
    isLocalSearching = NO;
}
- (void)viewDidLoad {
    searchBarChatFriend.placeholder = NSLocalizedString(@"Search", @"Search");
    [super viewDidLoad];
    [gCXMPPController connect];
    [self setNavigationBar];
    [self viewSetup];

    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    gCXMPPController._messageDelegate = self;
    viewStatus=VIEW_STATUS_NOT_POPPED;
    if(!self.arrFriends)
        self.arrFriends = [[NSMutableArray alloc] init];
    [self reloadTableView];
}
- (void)viewWillDisappear:(BOOL)animated
{
    viewStatus = VIEW_STATUS_POPPED;
    gCXMPPController._messageDelegate = nil;
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - fetch details
- (void)fetchedResultsController
{

        NSManagedObjectContext *moc = [gCXMPPController messagesStoreMainThreadManagedObjectContext];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject"
                                                  inManagedObjectContext:moc];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entity];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"streamBareJidStr LIKE[c] %@",[[NSUserDefaults standardUserDefaults] valueForKey:kXMPPmyJID1]];
    [fetchRequest setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"mostRecentMessageTimestamp" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects: sortDescriptor, nil]];

    NSError *error;
    if(!self.friends)
        self.friends = [[NSMutableArray alloc] init];
    [self.friends removeAllObjects];

    [self.friends addObjectsFromArray: [moc executeFetchRequest:fetchRequest error:&error]];
    
    
    NSArray *arrAllContacts = [[Database database] fetchDataFromDatabaseForEntity:@"AddressBookDB"];
    for(int i = 0;i<[self.friends count];i++)
    {
        XMPPMessageArchiving_Contact_CoreDataObject *contact = [self.friends objectAtIndex:i];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.orgChatUsername contains[cd] %@",[[contact.bareJidStr componentsSeparatedByString:@"@"] firstObject]];
        NSArray *arr = [arrAllContacts filteredArrayUsingPredicate:predicate];
        
        if([arr count]>0)
        {
            [self.arrFriends addObjectsFromArray:arr];
        }
        else
        {
//            AddressBookDB *add = [[AddressBookDB alloc]init];
//            add.fullName = [[[contact bareJidStr] componentsSeparatedByString:@"@"] firstObject];
//            add.profilePic = @"";
//            add.userId = [NSNumber numberWithInt:-1];
//            add.orgChatUsername = [[[contact bareJidStr] componentsSeparatedByString:@"@"] firstObject];
            [self.arrFriends addObject:contact];
        }
    }
    
//    NSLog(@"Fetch Result Controller self.Array  : %@ ",self.friends);
//    NSLog(@"Fetch Result Controller self.ArrFriends  : %@ ",self.arrFriends);
}
#pragma mark - table view methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(isLocalSearching)
    {
        return self.arrSearchResults.count;
    }
    return self.arrFriends.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    
    if (cell == nil)
    {
        NSArray* nib;
        nib  = [[NSBundle mainBundle] loadNibNamed:@"CellChatHome" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        UIView *selectionColor = [[UIView alloc] init];
        selectionColor.backgroundColor = [UIColor colorWithRed:(224/255.0) green:(224/255.0) blue:(224/255.0) alpha:1];
        cell.selectedBackgroundView = selectionColor;
        //        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        [cell setEditing:YES animated:YES];
        cell.shouldIndentWhileEditing = YES;
    }
 
    AddressBookDB *addFriends = nil;
    XMPPMessageArchiving_Contact_CoreDataObject *cont = nil;
    if(isLocalSearching)
    {
        if([[self.arrSearchResults objectAtIndex:indexPath.row] isKindOfClass:[AddressBookDB class]])
        {
            addFriends = [self.arrSearchResults objectAtIndex:indexPath.row];
        }
        else
        {
            cont =[self.arrSearchResults objectAtIndex:indexPath.row];
        }
    }
    else
    {
        if([[self.arrFriends objectAtIndex:indexPath.row] isKindOfClass:[AddressBookDB class]])
        {
            addFriends = [self.arrFriends objectAtIndex:indexPath.row];
        }
        else
        {
            cont =[self.arrFriends objectAtIndex:indexPath.row];
        }
    }
    if(addFriends!=nil)
    {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.bareJidStr contains[cd] %@",addFriends.orgChatUsername];
    NSArray *arr = [self.friends filteredArrayUsingPredicate:predicate];
    cont = [arr objectAtIndex:0];
    

    UIImageView *imgUser = (UIImageView *)[cell.contentView viewWithTag:1];
    imgUser.layer.cornerRadius = imgUser.bounds.size.width/2;
//    imgUser.layer.masksToBounds = YES;
        imgUser.autoresizingMask = UIViewAutoresizingNone;
        imgUser.clipsToBounds=YES;
    
    
    //    NSDictionary *dictProfile = [AppManager createDifferentUrlFromUrl:friends.profilePic];
    
    [imgUser setImageWithURL:[NSURL URLWithString:addFriends.profilePic] placeholderImage:[UIImage imageNamed:@"chatDefaultPic"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
    }];
    UILabel *lblFriendName = (UILabel *)[cell.contentView viewWithTag:2];
    lblFriendName.text = addFriends.fullName;
    UILabel *lblDateTime = (UILabel *)[cell.contentView viewWithTag:3];
    if(cont.mostRecentMessageBody.length> 0)
    {
        lblDateTime.text = [Utils convertedDate:cont.mostRecentMessageTimestamp];
    }
//    //    //
    UILabel *lblComment = (UILabel *)[cell.contentView viewWithTag:4];
    lblComment.text = cont.mostRecentMessageBody;
    NSMutableArray *arrMsg = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:MESSAGE_COUNTER]];
    if([arrMsg count]>0)
    {
        NSPredicate *predicate = nil;
        if([addFriends.orgChatUsername rangeOfString:STRChatServerURL].length>0)
        {
            predicate = [NSPredicate predicateWithFormat:@"self = %@",addFriends.orgChatUsername];
        }
        else
        {
            predicate = [NSPredicate predicateWithFormat:@"self = %@",[[NSString stringWithFormat:@"%@@%@",addFriends.orgChatUsername,STRChatServerURL] lowercaseString]];
        }
        
        NSArray *aRecentArr = [arrMsg filteredArrayUsingPredicate:predicate];
        if([aRecentArr count]>0)
        {
            UIImageView *counterImage = (UIImageView *)[cell.contentView viewWithTag:12];
            counterImage.hidden = NO;
            UILabel *lblCounter = (UILabel *)[cell.contentView viewWithTag:13];
            if([aRecentArr count]>99)
            {
                lblCounter.text = [NSString stringWithFormat:@"99+"];
            }
            else
            {
                lblCounter.text = [NSString stringWithFormat:@"%lu",(unsigned long)[aRecentArr count]];
            }
            lblCounter.hidden = NO;
        }
        
    }
    }
    else
    {
        UIImageView *imgUser = (UIImageView *)[cell.contentView viewWithTag:1];
        imgUser.layer.cornerRadius = imgUser.bounds.size.width/2;
//        imgUser.layer.masksToBounds = YES;
        imgUser.autoresizingMask = UIViewAutoresizingNone;
        imgUser.clipsToBounds=YES;
        
        //    NSDictionary *dictProfile = [AppManager createDifferentUrlFromUrl:friends.profilePic];
        [imgUser setImage:[UIImage imageNamed:@"chatDefaultPic"]];
        UILabel *lblFriendName = (UILabel *)[cell.contentView viewWithTag:2];
        lblFriendName.text = [[cont.bareJidStr componentsSeparatedByString:@"@"] firstObject];
        UILabel *lblDateTime = (UILabel *)[cell.contentView viewWithTag:3];
        if(cont.mostRecentMessageBody.length> 0)
        {
            lblDateTime.text = [Utils convertedDate:cont.mostRecentMessageTimestamp];
        }
        //    //    //
        UILabel *lblComment = (UILabel *)[cell.contentView viewWithTag:4];
        lblComment.text = cont.mostRecentMessageBody;
        NSMutableArray *arrMsg = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:MESSAGE_COUNTER]];
        if([arrMsg count]>0)
        {
            NSPredicate *predicate = nil;
            if([cont.bareJidStr rangeOfString:STRChatServerURL].length>0)
            {
                predicate = [NSPredicate predicateWithFormat:@"self = %@",cont.bareJidStr];
            }
            else
            {
                predicate = [NSPredicate predicateWithFormat:@"self = %@",[[NSString stringWithFormat:@"%@@%@",addFriends.orgChatUsername,STRChatServerURL] lowercaseString]];
            }
            
            NSArray *aRecentArr = [arrMsg filteredArrayUsingPredicate:predicate];
            if([aRecentArr count]>0)
            {
                UIImageView *counterImage = (UIImageView *)[cell.contentView viewWithTag:12];
                counterImage.hidden = NO;
                UILabel *lblCounter = (UILabel *)[cell.contentView viewWithTag:13];
                if([aRecentArr count]>99)
                {
                    lblCounter.text = [NSString stringWithFormat:@"99+"];
                }
                else
                {
                    lblCounter.text = [NSString stringWithFormat:@"%lu",(unsigned long)[aRecentArr count]];
                }
                lblCounter.hidden = NO;
            }
            
        }

    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    UIButton *btn = (UIButton *)sender;
    AppDelegate *deleg = APP_DELEGATE;
    AddressBookDB *addfriends = nil;
    XMPPMessageArchiving_Contact_CoreDataObject *cont = nil;
    if(isLocalSearching)
    {
        if([[self.arrSearchResults objectAtIndex:indexPath.row] isKindOfClass:[AddressBookDB class]])
        {
            addfriends = [self.arrSearchResults objectAtIndex:indexPath.row];
        }
        else
        {
            cont =[self.arrSearchResults objectAtIndex:indexPath.row];
        }
    }
    else
    {
        if([[self.arrFriends objectAtIndex:indexPath.row] isKindOfClass:[AddressBookDB class]])
        {
            addfriends = [self.arrFriends objectAtIndex:indexPath.row];
        }
        else
        {
            cont =[self.arrFriends objectAtIndex:indexPath.row];
        }

    }
    SMChatViewController *chatView = nil;//[[SMChatViewController alloc] initWithNibName:@"SMChatViewController" bundle:nil];

    if(addfriends!=nil)
    {
    
    chatView = [deleg createChatViewByChatUserNameIfNeeded:addfriends.orgChatUsername];
    chatView.chatWithUser =[NSString stringWithFormat:@"%@@%@",addfriends.orgChatUsername,STRChatServerURL];
    chatView.userName = addfriends.fullName;
//    NSArray *array = [friends.profilePic componentsSeparatedByString:@";"];
    chatView.imageString = addfriends.profilePic;
    chatView.friendNameId = [NSString stringWithFormat:@"%@", addfriends.userId];
    [searchBarChatFriend resignFirstResponder];
    chatView.isComingFrom = YES;
    chatView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatView animated:YES];
    }
    else
    {
        chatView =[[SMChatViewController alloc] initWithNibName:@"SMChatViewController" bundle:nil];
//        chatView = [deleg createChatViewByChatUserNameIfNeeded:[[cont.bareJidStr componentsSeparatedByString:@"@"] firstObject]];
        chatView.chatWithUser =cont.bareJidStr;
        chatView.userName = [[cont.bareJidStr componentsSeparatedByString:@"@"] firstObject];
        //    NSArray *array = [friends.profilePic componentsSeparatedByString:@";"];
//        chatView.imageString = ;
//        chatView.friendNameId = [NSString stringWithFormat:@"%@", addfriends.userId];
        [searchBarChatFriend resignFirstResponder];
        chatView.isComingFrom = YES;
        chatView.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:chatView animated:YES];

    }
}

-(void)setEditing:(BOOL)editing animated:(BOOL) animated {
    if( editing != self.editing ) {
        [super setEditing:editing animated:animated];
        [self.tblViewMessages setEditing:editing animated:animated];
    }
    if(editing)
    {
        searchBarChatFriend.userInteractionEnabled = NO;
        searchBarChatFriend.alpha = 0.5;
    }
    else
    {
        searchBarChatFriend.userInteractionEnabled = YES;
        searchBarChatFriend.alpha = 1.0;
    }
}
- (void) tableView:(UITableView *)tv commitEditingStyle:(UITableViewCellEditingStyle) editing
 forRowAtIndexPath:(NSIndexPath *)indexPath {
    if( editing == UITableViewCellEditingStyleDelete ) {
        NSString *strchatusername = nil;
        if([[self.arrFriends objectAtIndex:indexPath.row] isKindOfClass:[AddressBookDB class]])
        {
            AddressBookDB *addressb = [self.arrFriends objectAtIndex:indexPath.row];
            strchatusername = addressb.orgChatUsername;
        }
        else
        {
            XMPPMessageArchiving_Contact_CoreDataObject *cd = [self.arrFriends objectAtIndex:indexPath.row];
            strchatusername = [[cd.bareJidStr componentsSeparatedByString:@"@"] firstObject];
        }

        [self.arrFriends removeObjectAtIndex:indexPath.row];
        [self deleteChat:strchatusername];
        [tv deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                  withRowAnimation:UITableViewRowAnimationLeft];
    }
}
- (void)deleteChat:(NSString *)strChatUserName
{
    [[gCXMPPController xmppMessageArchivingStorage] deleteHistory:[strChatUserName stringByAppendingString:[NSString stringWithFormat:@"@%@",STRChatServerURL]]];
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tv editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}
#pragma mark searchbar delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self.arrSearchResults removeAllObjects];
    
    NSPredicate *resultPredicateUserName = [NSPredicate
                                            predicateWithFormat:@"SELF.orgChatUsername contains[cd] %@ or SELF.fullName contains[cd] %@",
                                            searchBarChatFriend.text,searchBarChatFriend.text];

    NSArray *arrPredicate = [NSArray arrayWithObjects:resultPredicateUserName, nil];
    
    NSPredicate *compoundPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:arrPredicate];
    
    
    NSArray *array = [self.arrFriends filteredArrayUsingPredicate:compoundPredicate];
    self.arrSearchResults = [NSMutableArray arrayWithArray:array];
    
    
    if(searchBarChatFriend.text.length>0)
        isLocalSearching=YES;
    
    else
        isLocalSearching=NO;
    
    [tblViewMessages reloadData];
    
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)_searchBar{
    
    [searchBarChatFriend resignFirstResponder];
    
    NSPredicate *resultPredicateUserName = [NSPredicate
                                            predicateWithFormat:@"SELF.orgChatUsername contains[cd] %@ or SELF.fullName contains[cd] %@",
                                            searchBarChatFriend.text,searchBarChatFriend.text];
    
    NSArray *arrPredicate = [NSArray arrayWithObjects:resultPredicateUserName, nil];
    
    NSPredicate *compoundPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:arrPredicate];
    
    [self.arrSearchResults removeAllObjects];
    //    isLocalSearching=YES;
    NSArray *arr = [self.arrFriends filteredArrayUsingPredicate:compoundPredicate];
    
    self.arrSearchResults = [NSMutableArray arrayWithArray:arr];
    
    if(searchBarChatFriend.text.length>0)
        isLocalSearching=YES;
    else
        isLocalSearching=NO;
    [tblViewMessages reloadData];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    self.navigationItem.leftBarButtonItem.enabled = NO;
    //    isLocalSearching = YES;
    //    isKeyboardVisible = YES;
    [searchBarChatFriend setShowsCancelButton:YES animated:YES];
    
    if (!self.arrSearchResults) {
        self.arrSearchResults = [NSMutableArray array];
    }
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBa
{
    self.navigationItem.leftBarButtonItem.enabled= YES;
    [searchBarChatFriend setShowsCancelButton:NO animated:YES];
    //    isKeyboardVisible = NO;
    isLocalSearching = NO;
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)_searchBar
{
    
    [searchBarChatFriend resignFirstResponder];
    //    isKeyboardVisible = NO;
    //    if(searchBarFriends.text.length>0)
    //        isLocalSearching=YES;
    //
    //    //    else
    //        isLocalSearching=NO;
    searchBarChatFriend.text = @"";
    [tblViewMessages reloadData];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)reloadTableView
{
    [self.arrFriends removeAllObjects];
    [self fetchedResultsController];
    if([self.arrFriends count]>0)
    {
        [tblViewMessages setHidden:NO];
    }
    else
    {
        //        [self.lblNoMatchFound setHidden:NO];
        [self.tblViewMessages setHidden:YES];
    }
    [self.tblViewMessages reloadData];

}
- (void)newMessageReceived:(NSDictionary *)messageContent
{
    [self reloadTableView];
}
- (void)newGroupMessageReceived:(NSDictionary *)messageContent
{
    
}
- (void)newPrivateMessageReceived:(NSDictionary *)messageContent{
    
}
- (void)isDelivered:(NSDictionary *)messageContent
{
    
}
- (void)userPresence:(NSDictionary *)presence
{
    [self reloadTableView];
}
- (void)ReceiveIQ:(XMPPIQ *)IQ
{
    
}
- (void)StopAudioRecorder
{
    
}
//27-1-14 displayed
- (void)isDisplayed:(NSDictionary *)messageContent
{
    
}
#pragma mark - button clicked methods
- (void)changeStatus
{
    StatusViewController *statusViewController=[[StatusViewController alloc] initWithNibName:@"StatusViewController" bundle:nil];
    statusViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:statusViewController animated:YES];
    
}
- (void)addUser
{
    PhoneBookViewController *phoneBook=[[PhoneBookViewController alloc] initWithNibName:@"PhoneBookViewController" bundle:nil];
    phoneBook.delegate = self;
    phoneBook.isFromTabBar = NO;
    [self presentViewController:phoneBook animated:YES completion:nil];
}
#pragma mark - phone book delegate
-(void)selectedUser:(AddressBookDB*)contact{

    SMChatViewController *chatView = nil;//[[SMChatViewController alloc] initWithNibName:@"SMChatViewController" bundle:nil];
    chatView = [appDelegate createChatViewByChatUserNameIfNeeded:contact.orgChatUsername];
    chatView.chatWithUser =[NSString stringWithFormat:@"%@@%@",contact.orgChatUsername,STRChatServerURL];
    chatView.userName = contact.fullName;
    //    NSArray *array = [friends.profilePic componentsSeparatedByString:@";"];
    chatView.imageString = contact.profilePic;
    chatView.friendNameId = [NSString stringWithFormat:@"%@", contact.userId];
    [searchBarChatFriend resignFirstResponder];
    chatView.isComingFrom = YES;
    chatView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatView animated:YES];
}
@end
