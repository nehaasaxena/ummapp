//
//  PhoneBookViewController.m
//  SocialParty
//
//  Created by pankaj on 14/08/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "PhoneBookViewController.h"
#import "PhoneBookTableViewCell.h"
#import "ContactsDetailViewController.h"
#import "AddressBookDB.h"
#import "SMChatViewController.h"
#define kBadgeViewTag 121223
@interface PhoneBookViewController ()

@end

@implementation PhoneBookViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.isFromTabBar = YES;
        // Custom initialization
    }
    return self;
}
-(void)setNavigationBar
{
    self.navigationController.navigationItem.title = NSLocalizedString(@"Accounts", @"navigation Title") ;
    self.navigationItem.rightBarButtonItem.enabled = YES;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNavigationBar];
//    arrPhoneBook = (NSMutableArray*)[[Database database] fetchDataFromAddressBookDBOfType:ADDRESSBOOK_CONTACT_REGISTERED_ON_MEKS];
    if(self.isFromTabBar)
    {
        [phoneBookTableview setFrame:CGRectMake(0, 108, 320, [UIScreen mainScreen].bounds.size.height-157)];
    }
    else
    {
        [phoneBookTableview setFrame:CGRectMake(0, 108, 320, [UIScreen mainScreen].bounds.size.height-108)];
    }
    
    activityIndicatorView =[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [activityIndicatorView setFrame:CGRectMake(0, 0, 40, 40)];
    [activityIndicatorView setCenter:phoneBookTableview.center];
    [activityIndicatorView setHidesWhenStopped:YES];
    [self.view addSubview:activityIndicatorView];

   
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];    
   

    [self refreshArrayOfAllContacts];
    self.tabBarController.tabBar.hidden = NO;

}



-(void)refreshArrayOfAllContacts{
    arrPhoneBook=nil;
    arrPhoneBook = [NSMutableArray arrayWithArray:[[Database database] fetchDataFromAddressBookDBOfType:ADDRESSBOOK_CONTACT_REGISTERED_ON_UMMAPP]];
    if ([arrPhoneBook count]==0 && [AppManager sharedManager].isFetchingContacts) {
        [activityIndicatorView startAnimating];
        [phoneBookTableview setHidden:YES];
    }else{
        [activityIndicatorView stopAnimating];
         [phoneBookTableview setHidden:NO];
    }
   
    [phoneBookTableview reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//-(void)sendAddressBookContactsOnMEKSServer{
//    NSArray *contactList=[AppManager fetchAddressBook];
//    
////    [arrContactsToBeInvited removeAllObjects];
//    
//    [self callWebServiceFor:kCheckContactList withObject:contactList];
//    
//    
//    
//}

#pragma mark - UITableView delegate & Datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == [[self searchDisplayController] searchResultsTableView])
    {
        return [filteredResult count];
    }else
        return [arrPhoneBook count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    cellIdentifier = @"cell";
    PhoneBookTableViewCell *cell = (PhoneBookTableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil) {
        cell=[[PhoneBookTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.accessoryType=UITableViewCellAccessoryDetailButton;
    }
    
    if(tableView == [[self searchDisplayController] searchResultsTableView])
    {
        [cell updateCell:[filteredResult objectAtIndex:indexPath.row]];
    }
    else
    {
        [cell updateCell:[arrPhoneBook objectAtIndex:indexPath.row]];
    }
//    [cell updateCell:[arrPhoneBook objectAtIndex:indexPath.row]];
    
   // cell.textLabel.attributedText=contact.fullName;
    
    return cell;
}
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"reaching accessoryButtonTappedForRowWithIndexPath:");
    ContactsDetailViewController *contactDetail=[[ContactsDetailViewController alloc] initWithNibName:@"ContactsDetailViewController" bundle:nil];
    
    AddressBookDB *address;
        if(tableView == [[self searchDisplayController] searchResultsTableView])
        {
            address=(AddressBookDB*)[filteredResult objectAtIndex:indexPath.row];
        }
        else
        {
            address=(AddressBookDB*)[arrPhoneBook objectAtIndex:indexPath.row];
        }
    
    
    NSMutableArray *arrContacDetail=[[NSMutableArray alloc] init];
    
    NSArray *valuesPhone = nil;
    if (address.phone) {
        NSData* data = [address.phone dataUsingEncoding:NSUTF8StringEncoding];
        valuesPhone = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        
    }
    
    NSArray *valuesEmail = nil;
    if (address.email) {
        NSData* data = [address.email dataUsingEncoding:NSUTF8StringEncoding];
        valuesEmail = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        
    }
//     contactDetail.navigatefrom=PROFILE_PAGE_NAVIGATED_FROM_ADDRESS_BOOK;
    //    NSMutableDictionary *aDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:address.fullName,kfullName, nil];
    
    if (address.fullName) {
        contactDetail.strFullName=address.fullName;
    }
    if (valuesPhone) {
        NSMutableDictionary *aTemp=[NSMutableDictionary dictionaryWithObjectsAndKeys:valuesPhone,kPhoneNumber, nil];
        [arrContacDetail addObject:aTemp];
        
    }
    if (valuesEmail) {
        NSMutableDictionary *aTemp=[NSMutableDictionary dictionaryWithObjectsAndKeys:valuesEmail,kemail, nil];
        [arrContacDetail addObject:aTemp];
        
    }
    if (address.notes) {
        NSMutableDictionary *aTemp=[NSMutableDictionary dictionaryWithObjectsAndKeys:address.notes,kNotes, nil];
        [arrContacDetail addObject:aTemp];
        CGSize notesHeight=[AppManager frameForText:address.notes sizeWithFont:[UIFont fontWithName:ssFontHelveticaNeue size:17] constrainedToSize:CGSizeMake(250, 1000)];
        contactDetail.notesHeight=ceil(notesHeight.height);
    }
    
    if ([address.originalUserId integerValue] == 0) {
        
        
        NSMutableDictionary *aTemp=[NSMutableDictionary dictionaryWithObjectsAndKeys:@"Invite to UmmApp",@"button",[NSNumber numberWithInt:ADDRESSBOOK_DETAIL_BUTTON_TYPE_INVITE_TO_UMMAPP],@"type", nil];
        
        NSArray *arrtemp=[NSArray arrayWithObjects:aTemp, nil];
        NSDictionary *aTempOuter=[NSDictionary dictionaryWithObjectsAndKeys:arrtemp,@"button", nil];
        
        
        [arrContacDetail addObject:aTempOuter];
        
    }else{
         NSMutableDictionary *aTemp3=[NSMutableDictionary dictionaryWithObjectsAndKeys:@"View All Media",@"button",[NSNumber numberWithInt:ADDRESSBOOK_DETAIL_BUTTON_TYPE_VIEW_ALL_MEDIA],@"type", nil];
        
        NSMutableDictionary *aTemp=[NSMutableDictionary dictionaryWithObjectsAndKeys:@"Send Message",@"button",[NSNumber numberWithInt:ADDRESSBOOK_DETAIL_BUTTON_TYPE_SEND_MESSAGE],@"type", nil];
        
        NSMutableDictionary *aTemp1=[NSMutableDictionary dictionaryWithObjectsAndKeys:@"Email Conversation",@"button",[NSNumber numberWithInt:ADDRESSBOOK_DETAIL_BUTTON_TYPE_EMAIL_CONVERSTAION],@"type", nil];
        
        NSMutableDictionary *aTemp2=[NSMutableDictionary dictionaryWithObjectsAndKeys:@"Clear Message",@"button",[NSNumber numberWithInt:ADDRESSBOOK_DETAIL_BUTTON_TYPE_CLEAR_CONVERSATION],@"type", nil];
        
        NSArray *arrtemp=[NSArray arrayWithObjects:aTemp3,aTemp,aTemp1,aTemp2, nil];
        NSDictionary *aTempOuter=[NSDictionary dictionaryWithObjectsAndKeys:arrtemp,@"button", nil];
        
        
        [arrContacDetail addObject:aTempOuter];
    }
    
    contactDetail.strJobTitle=address.jobTitle;
    contactDetail.strChatUserName = address.orgChatUsername;
    contactDetail.arrContactDetail=arrContacDetail;
    contactDetail.strProfilePic=address.profilePic;
    contactDetail.originalUserId=[address.originalUserId integerValue];
    contactDetail.hidesBottomBarWhenPushed=YES;
    //    contactDetail.aDictContactDetail=aDict;
    
    [self.navigationController pushViewController:contactDetail animated:YES];

   
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    AddressBookDB *address;
    if(tableView == [[self searchDisplayController] searchResultsTableView])
    {
        address=(AddressBookDB*)[filteredResult objectAtIndex:indexPath.row];
    }
    else
    {
        address=(AddressBookDB*)[arrPhoneBook objectAtIndex:indexPath.row];
    }
    
    
    if (_delegate) {
        if ([_delegate respondsToSelector:@selector(selectedUser:)]) {
            [_delegate selectedUser:address];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
    
    AppDelegate *deleg = APP_DELEGATE;
    SMChatViewController *chatView = nil;
    chatView = [deleg createChatViewByChatUserNameIfNeeded: address.orgChatUsername];
    chatView.chatWithUser =[[address.orgChatUsername stringByAppendingString:[NSString stringWithFormat:@"@%@",STRChatServerURL]]lowercaseString];
    chatView.userName = address.fullName;
    chatView.imageString = address.profilePic;
    chatView.isComingFromAddressBook = YES;
    chatView.friendNameId = [NSString stringWithFormat:@"%@",address.originalUserId];
    //    chatView.navigatedFrom=PROFILE_PAGE_NAVIGATED_FROM_ADDRESS_BOOK;
    chatView.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:chatView animated:YES];
    }
//    PersonPageViewController *personPageViewController = [[PersonPageViewController alloc] initWithNibName:@"PersonPageViewController" bundle:nil];
//    
//    Person *person = [[Person alloc] init];
//    person.userId = [address.originalUserId integerValue];
//    person.userName = address.orgAppUsername;
//    person.profilePic = address.profilePic;
//    person.fullName = address.fullName;
//    
//    person.chatUserName = address.orgChatUsername;
//    personPageViewController.person = person;
//    personPageViewController.hidesBottomBarWhenPushed=YES;
//    personPageViewController.isComingFromAddressBook=YES;
//    personPageViewController.profilePageNavigatedFrom=PROFILE_PAGE_NAVIGATED_FROM_ADDRESS_BOOK;
//    [self.navigationController pushViewController:personPageViewController animated:YES];
    
    
}
-(void)pushToChatView:(NSIndexPath*)indexPath andtableView:(UITableView*)tableView{
    AddressBookDB *address;
    if(tableView == [[self searchDisplayController] searchResultsTableView])
    {
        address=(AddressBookDB*)[filteredResult objectAtIndex:indexPath.row];
    }
    else
    {
        address=(AddressBookDB*)[arrPhoneBook objectAtIndex:indexPath.row];
    }
    
    AppDelegate *deleg = APP_DELEGATE;
    SMChatViewController *chatView = nil;
    chatView = [deleg createChatViewByChatUserNameIfNeeded: address.orgChatUsername];
    chatView.chatWithUser =[[address.orgChatUsername stringByAppendingString:[NSString stringWithFormat:@"@%@",STRChatServerURL]]lowercaseString];
    chatView.userName = address.fullName;
    chatView.imageString = address.profilePic;
    chatView.isComingFromAddressBook = YES;
    chatView.friendNameId = [NSString stringWithFormat:@"%@",address.originalUserId];
//    chatView.navigatedFrom=PROFILE_PAGE_NAVIGATED_FROM_ADDRESS_BOOK;
    chatView.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:chatView animated:YES];

}



-(void)responseHandler :(id)inResponseDic andRequestIdentifier:(NSString *)inReqIdentifier
{
    
//    if (viewStatus == VIEW_STATUS_POPPED) {
//        return;
//    }
    
//    [activityIndicator stopAnimating];
    [AppManager stopStatusbarActivityIndicator];
//    [refrestAddressBookContacts endRefreshing];
    
    
    if ([[inResponseDic valueForKey:@"status"] integerValue] == RESPONSE_STATUS_SUCCESS) {
        
        NSString *responseOption = [inResponseDic valueForKey:@"option"];
        
       
        
    }else if ([[inResponseDic valueForKey:kStatus] integerValue] == RESPONSE_STATUS_AUTHENTICATION_FAIL){
        
        [[AppManager sharedManager] showAuthenticationFailedAlertView];
    }
}

-(void)requestErrorHandler :(NSError *)inError andRequestIdentifier :(NSString *)inReqIdentifier
{
    
//    if (viewStatus == VIEW_STATUS_POPPED) {
//        return;
//    }
    
//    [activityIndicator stopAnimating];
//    //    [Utils stopActivityIndicatorInView:self.view];
//    [AppManager stopStatusbarActivityIndicator];
//    [refrestAddressBookContacts endRefreshing];
//    
//    //    NSLog(@"*** requestErrorHandler Error : %@ and Request Udesntifier : %@ ****",[inError debugDescription],inReqIdentifier);
//    
//    //  Log in
//    if ([inReqIdentifier isEqualToString:kCheckContactList])
//    {
//        [refrestAddressBookContacts endRefreshing];
//    }
    
}

#pragma mark- FriendRequestReceivedViewControllerDelegate Method

-(void)hideBadgeView:(BOOL)status andCount:(NSInteger)count{
    
//    [btnFrndRequestReceived setHidden:status];
    
    [[viewMenuContainer viewWithTag:kBadgeViewTag] removeFromSuperview];
    
//    if (!status) {
//        UIView *badgeView = [self createBadgeOnIconsFor:btnPendingRequest andCountIs:count];
//        [badgeView setTag:kBadgeViewTag];
//        
//        [viewMenuContainer addSubview:badgeView];
//        [viewMenuContainer bringSubviewToFront:btnPendingRequest];
//      
//    }else{
//        [[viewMenuContainer viewWithTag:kBadgeViewTag] removeFromSuperview];
//    }
    
}


- (IBAction)btnPendingRequestAction:(id)sender {
    
}
#pragma mark - Search Display Controller Delegates
-(void) filterForSearchText:(NSString *) text scope:(NSString *) scope
{
    filteredResult = nil; // clearing filter array
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"SELF.fullName contains[c] %@",text]; // Creating filter condition
    filteredResult = [NSMutableArray arrayWithArray:[arrPhoneBook filteredArrayUsingPredicate:filterPredicate]]; // filtering result
}
-(BOOL) searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterForSearchText:searchString scope:[[[[self searchDisplayController] searchBar] scopeButtonTitles] objectAtIndex:[[[self searchDisplayController] searchBar] selectedScopeButtonIndex] ]];
    
    return YES;
}

-(BOOL) searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    [self filterForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    
    return YES;
}
- (IBAction)btnBack:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
