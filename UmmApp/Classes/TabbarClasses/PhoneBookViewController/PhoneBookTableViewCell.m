//
//  PhoneBookTableViewCell.m
//  SocialParty
//
//  Created by pankaj on 19/08/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "PhoneBookTableViewCell.h"

@implementation PhoneBookTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        imgProfilePic = [[UIImageView alloc] initWithFrame:CGRectMake(10, 4, 35, 35)];
        [imgProfilePic.layer setCornerRadius:imgProfilePic.frame.size.width/2];
        [imgProfilePic setClipsToBounds:YES];
        [self addSubview:imgProfilePic];
        
        lblFullName = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, 200, 25)];
        [lblFullName setBackgroundColor:[UIColor clearColor]];
        [lblFullName setTextColor:[UIColor blackColor]];
        [self addSubview:lblFullName];
        
        lblStatus = [[UILabel alloc] initWithFrame:CGRectMake(60, 24, 200, 15)];
        [lblStatus setBackgroundColor:[UIColor clearColor]];
        [lblStatus setTextColor:[UIColor lightGrayColor]];
        [lblStatus setFont:[UIFont systemFontOfSize:13]];
        [self addSubview:lblStatus];
        
        
        
    }
    return self;
}

-(void)updateCell:(AddressBookDB*)aDict{
    [lblFullName setText:[aDict valueForKey:kFullName]];
    AddressBookDB *ab = aDict;
//    NSDictionary *dictProfile = [AppManager createDifferentUrlFromUrl:[aDict valueForKey:@"profilePic"]];
    lblStatus.text = ab.userStatus;
    [imgProfilePic setImageWithURL:[NSURL URLWithString:ab.profilePic] placeholderImage:[UIImage imageNamed:@"chatDefaultPic"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        
    }];
    
   }

@end
