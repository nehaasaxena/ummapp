//
//  PhoneBookViewController.h
//  SocialParty
//
//  Created by pankaj on 14/08/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressBookDB.h"
@protocol  PhoneBookDelegate <NSObject>

-(void)selectedUser:(AddressBookDB*)contact;

@end

@interface PhoneBookViewController : UIViewController<UIGestureRecognizerDelegate>{
    NSMutableArray *arrPhoneBook;
    __weak IBOutlet UITableView *phoneBookTableview;
    NSMutableArray *sectionedListContent;
    NSMutableArray *filteredResult;
    UIActivityIndicatorView *activityIndicatorView;
    __weak IBOutlet UIButton *btnBackReference;
    __weak IBOutlet UIButton *btnPendingRequest;
    __weak IBOutlet UIView *viewMenuContainer;
    
    
}
-(void)refreshArrayOfAllContacts;
- (IBAction)btnBack:(id)sender;
- (IBAction)btnPendingRequestAction:(id)sender;
@property (nonatomic, assign) BOOL isFromTabBar;
@property (nonatomic, weak) id<PhoneBookDelegate> delegate;

@end
