//
//  PhoneBookTableViewCell.h
//  SocialParty
//
//  Created by pankaj on 19/08/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneBookTableViewCell : UITableViewCell
{
    UIImageView *imgProfilePic;
    UILabel *lblFullName;
    UILabel *lblStatus;
//    UIButton *btnPhone;
}
-(void)updateCell:(AddressBookDB*)aDict;
@end
