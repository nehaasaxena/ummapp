//
//  StatusTableViewCell.h
//  SocialParty
//
//  Created by pankaj on 09/09/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StatusCellDelegate <NSObject>
@optional

-(void)btnDeleteClickedOnCellWithStatusId:(NSString*)statusId;

-(void)didCellScrolled:(id)cellScrolled;
-(void)didCellClicked:(NSString *)statusId andObjectAtIndexPath:(NSIndexPath*)indexPath;

@end
@interface StatusTableViewCell : UITableViewCell<UIScrollViewDelegate>
{
   // UIScrollView *sscrollView;
    UIView *viewBackground;
    UILabel *lblStatus;
    UIImageView *imgTickMark;
    UIButton *btnDelete;
    UIButton *btnDeleteActual;
    UIButton *btnCellClick;
}
-(void)updateCellView:(NSMutableDictionary *)aDict andIndexPath:(NSIndexPath*)indexPath;
@property (nonatomic, weak) id<StatusCellDelegate> delegate;
@property (nonatomic, strong) UIScrollView *sscrollView;
@property (nonatomic, strong) NSString *statusId;
@property (nonatomic, strong) NSIndexPath *indexpath;
@end
