//
//  AddStatusViewController.m
//  SocialParty
//
//  Created by pankaj on 09/09/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "AddStatusViewController.h"
#define maxStatusLength   140
@interface AddStatusViewController ()

@end

@implementation AddStatusViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [txtStatus becomeFirstResponder];
    if ([_strStatus length] != 0) {
        [txtStatus setText:_strStatus];
//        [lblStatusHeading setText:NSLocalizedString([NSString stringWithFormat:@"Your Status (%d)",140-[_strStatus length]], @"Your Status")];
        lblStatusHeading.text = [NSString stringWithFormat:NSLocalizedString(@"Your Status (%d)", nil), 140-[_strStatus length]];
    }
    else{
        [lblStatusHeading setText:[NSString stringWithFormat:NSLocalizedString(@"Your Status (%d)", nil), 140]];
        [btnSaveOutlet setEnabled:NO];
    }
    [self.btnCancel setTitle:NSLocalizedString(@"Cancel", @"Cancel") forState:UIControlStateNormal];
    [self.btnSave setTitle:NSLocalizedString(@"Save", @"Save") forState:UIControlStateNormal];
    
    // Do any additional setup after loading the view from its nib.
}
#pragma mark- Web Service Methods



-(void)responseHandler :(id)inResponseDic andRequestIdentifier:(NSString *)inReqIdentifier
{

    [AppManager stopStatusbarActivityIndicator];
    
    if ([[inResponseDic valueForKey:@"status"] integerValue] == RESPONSE_STATUS_SUCCESS) {
        
       
        
    }else if ([[inResponseDic valueForKey:kStatus] integerValue] == RESPONSE_STATUS_AUTHENTICATION_FAIL){
        
        [[AppManager sharedManager] showAuthenticationFailedAlertView];
    }else{
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kAlertTitle message:[inResponseDic valueForKey:kMessage] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)requestErrorHandler :(NSError *)inError andRequestIdentifier :(NSString *)inReqIdentifier
{

    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - TextView Delegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
     NSInteger check=maxStatusLength-(textView.text.length + text.length - range.length);
    if (check < 0) {
        return NO;
//        [btnSaveOutlet setEnabled:NO];
    }else if (check >140){
        [lblStatusHeading setText:[NSString stringWithFormat:NSLocalizedString(@"Your Status (%d)", nil), 140]];
        return NO;
    }else if (check == 140){
//        [btnSaveOutlet setEnabled:NO];
       
    }
    else{
//         [btnSaveOutlet setEnabled:YES];
    }
    
    [lblStatusHeading setText:[NSString stringWithFormat:NSLocalizedString(@"Your Status (%d)", nil), check]];
    return YES;
}
- (void)textViewDidChange:(UITextView *)textView{
    BOOL isStringEmpty=[self checkWhetherStringIsNotEmpty:textView.text];
    if (isStringEmpty) {
        [btnSaveOutlet setEnabled:NO];
    }else
        [btnSaveOutlet setEnabled:YES];
}


- (IBAction)btnCancel:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(BOOL)checkWhetherStringIsNotEmpty:(NSString*)strStatus{
    
    NSString *trimmedString = [strStatus stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (trimmedString.length>0) {
        return NO;
    }else
        return YES;
}
- (IBAction)btnSave:(UIButton *)sender {
    
    BOOL shouldUpdate=NO;
    if (![_strStatus isEqualToString:txtStatus.text]) {
        shouldUpdate = YES;
    }
    if ([self.delegate respondsToSelector:@selector(AddStatusWithStatusText:andShouldUpdate:)]) [self.delegate AddStatusWithStatusText:txtStatus.text andShouldUpdate:shouldUpdate];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    }
@end
