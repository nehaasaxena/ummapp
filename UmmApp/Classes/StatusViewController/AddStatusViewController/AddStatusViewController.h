//
//  AddStatusViewController.h
//  SocialParty
//
//  Created by pankaj on 09/09/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddStatusDelegate <NSObject>

-(void)AddStatusWithStatusText:(NSString*)strStatusText andShouldUpdate:(BOOL)shouldUpdate;

@end

@interface AddStatusViewController : UIViewController
{
   __weak IBOutlet UILabel *lblStatusHeading;
    __weak IBOutlet UIView *viewStatus;
    __weak IBOutlet UIButton *btnSaveOutlet;
    __weak IBOutlet UITextView *txtStatus;
}
@property (nonatomic, strong) NSString *strStatus;
@property (nonatomic, weak) id<AddStatusDelegate> delegate;
- (IBAction)btnCancel:(UIButton *)sender;
- (IBAction)btnSave:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong, nonatomic) IBOutlet UIButton *btnSave;

@end
