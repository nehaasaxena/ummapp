//
//  StatusViewController.m
//  SocialParty
//
//  Created by pankaj on 09/09/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "StatusViewController.h"


@interface StatusViewController ()

@end

@implementation StatusViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [statusTableView setFrame:CGRectMake(0, 64, 320, [UIScreen mainScreen].bounds.size.height-64)];
    
    [self setNavigationBar];
    [self callWebService];
    // Do any additional setup after loading the view from its nib.
}

-(void)setNavigationBar
{
//    CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 200, 44);
//    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
//    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
//    _headerTitleSubtitleView.autoresizesSubviews = NO;
//    
//    CGRect titleFrame = CGRectMake(0,0, 200, 44);
//    UILabel *titleView = [[UILabel alloc] initWithFrame:titleFrame];
//    titleView.backgroundColor = [UIColor clearColor];
//    titleView.font = [UIFont fontWithName:ssFontBrushScriptStd size:20];
//    titleView.textAlignment = NSTextAlignmentCenter;
//    titleView.textColor = [UIColor whiteColor];
//    titleView.text = @"Status";
//    titleView.adjustsFontSizeToFitWidth = YES;
//    [_headerTitleSubtitleView addSubview:titleView];
//    self.navigationItem.titleView = _headerTitleSubtitleView;
    self.title = NSLocalizedString(@"Status", @"Status") ;
    
    
    
    UIImage *addNewImage = [UIImage imageNamed:@"chatAdd"];
    UIButton *addNew = [UIButton buttonWithType:UIButtonTypeCustom];
    addNew.bounds = CGRectMake( 0, 0, 30, 30 );
    [addNew setImage:addNewImage forState:UIControlStateNormal];
    [addNew addTarget:self action:@selector(btnAddStatus:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *addNewBtn = [[UIBarButtonItem alloc] initWithCustomView:addNew];
    
    
    
    NSArray *arrBtns = [[NSArray alloc]initWithObjects:addNewBtn, nil];
    self.navigationItem.rightBarButtonItems = arrBtns;
    self.navigationItem.hidesBackButton = YES;

    
    self.navigationItem.rightBarButtonItem.enabled = YES;
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 11, 20)];
    [backButton setImage:[UIImage imageNamed:@"arrow1"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(btnBackClicked) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];

    
}

#pragma mark - UITableView delegate & Datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [arrStatus count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[arrStatus objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    cellIdentifier = @"cell";
    StatusTableViewCell *cell = (StatusTableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil) {
        cell=[[StatusTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.accessoryType=UITableViewCellAccessoryNone;
        cell.delegate=self;
    }
    
    [cell updateCellView:[[arrStatus objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] andIndexPath:indexPath];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    switch (indexPath.section) {
//        case 0:
//            if (indexPath.row==0) {
//                [self openAddStatusView:[[[arrStatus objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] valueForKey:@"userStatus"]];
//            }
//            break;
//            
//        default:
//            [self callWebServiceAddStatus:@"" andStatusId:[[[arrStatus objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] valueForKey:@"userStatusId"]];
//            break;
//    }
    
}


#pragma mark- Web Service Methods

-(void)callWebService{
    
    if(![Utils isInternetAvailable])
    {
        //        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:kAlertTitle message:NSLocalizedString( @"Check your internet connection.",  @"Check your internet connection.") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
        return;
    }
    else
    {

        [self.view addSubview:activityView];
        [activityView setCenter:self.view.center];
        
        NetworkService *obj = [NetworkService sharedInstance];
        
        NSString *deviceId = [[NSUserDefaults standardUserDefaults] valueForKey:kDeviceToken];
        NSString *deviceType = kDevice;
        NSString *sessionToken = [[NSUserDefaults standardUserDefaults] valueForKey:kSessionToken];
        NSString *userId = [[NSUserDefaults standardUserDefaults] valueForKey:kUserId];
        NSString *option = @"getUserStatus";
        
        NSString *url = kUsers;
        
        NSMutableDictionary *aDict = [NSMutableDictionary dictionary];
        [aDict setObject:deviceId forKey:kDeviceToken];
        [aDict setObject:deviceType forKey:kDeviceType];
        [aDict setObject:sessionToken forKey:kSessionToken];
        [aDict setObject:userId forKey:kUserId];
        
        
        [aDict setObject:option forKey:@"option"];
        
        [obj sendAsynchRequestByPostToServer:url dataToSend:aDict  delegate: self contentType:eAppJsonType andReqParaType:eJson header:NO];
    }
}
-(void)callWebServiceAddStatus:(NSString *)strStatusText andStatusId:(NSString *)statusId{
    
    if(![Utils isInternetAvailable])
    {
        //        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:kAlertTitle message:@"Check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    else
    {

        [self.view addSubview:activityView];
         [activityView setCenter:self.view.center];
        
        NetworkService *obj = [NetworkService sharedInstance];
        NSString *deviceId = [[NSUserDefaults standardUserDefaults] valueForKey:kDeviceToken];
        NSString *deviceType = kDevice;
        NSString *sessionToken = [[NSUserDefaults standardUserDefaults] valueForKey:kSessionToken];
        NSString *userId = [[NSUserDefaults standardUserDefaults] valueForKey:kUserId];
        NSString *option = @"addUpdateUserStatus";
        
        NSString *url = kUsers;
        
        NSMutableDictionary *aDict = [NSMutableDictionary dictionary];
        [aDict setObject:deviceId forKey:kDeviceToken];
        [aDict setObject:deviceType forKey:kDeviceType];
        [aDict setObject:sessionToken forKey:kSessionToken];
        [aDict setObject:userId forKey:kUserId];
        
        if ([statusId integerValue] !=0) {
            [aDict setObject:statusId forKey:@"userStatusId"];
        }else
        [aDict setObject:strStatusText forKey:@"userStatus"];
        
        [aDict setObject:option forKey:@"option"];
        
        [obj sendAsynchRequestByPostToServer:url dataToSend:aDict  delegate: self contentType:eAppJsonType andReqParaType:eJson header:NO];
    }
}
-(void)callWebServiceDeleteStatusId:(NSString *)statusId{
    
    if(![Utils isInternetAvailable])
    {
        //        [Utils stopActivityIndicatorInView:self.view];
        [Utils showAlertView:kAlertTitle message:@"Check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    else
    {
        
        [self.view addSubview:activityView];
        [activityView setCenter:self.view.center];
        
        NetworkService *obj = [NetworkService sharedInstance];
        NSString *deviceId = [[NSUserDefaults standardUserDefaults] valueForKey:kDeviceToken];
        NSString *deviceType = kDevice;
        NSString *sessionToken = [[NSUserDefaults standardUserDefaults] valueForKey:kSessionToken];
        NSString *userId = [[NSUserDefaults standardUserDefaults] valueForKey:kUserId];
        NSString *option = @"deleteUserStatus";
        
        NSString *url = kUsers;
        
        NSMutableDictionary *aDict = [NSMutableDictionary dictionary];
        [aDict setObject:deviceId forKey:kDeviceToken];
        [aDict setObject:deviceType forKey:kDeviceType];
        [aDict setObject:sessionToken forKey:kSessionToken];
        [aDict setObject:userId forKey:kUserId];
        
   
        [aDict setObject:statusId forKey:@"userStatusId"];
        
        
        [aDict setObject:option forKey:@"option"];
        
        [obj sendAsynchRequestByPostToServer:url dataToSend:aDict  delegate: self contentType:eAppJsonType andReqParaType:eJson header:NO];
    }
}
-(void)responseHandler :(id)inResponseDic andRequestIdentifier:(NSString *)inReqIdentifier
{
    
    //    if (viewStatus == VIEW_STATUS_POPPED) {
    //        return;
    //    }

    [AppManager stopStatusbarActivityIndicator];

    
    
    
    
    if ([[inResponseDic valueForKey:@"status"] integerValue] == RESPONSE_STATUS_SUCCESS) {
        
        if ([[inResponseDic valueForKey:kOption] isEqualToString:@"getUserStatus"]) {
            
            NSMutableArray *arrTemp=[inResponseDic valueForKey:@"userStatus"];
            if ([arrTemp count]>0) {
          
            arrStatus=[[NSMutableArray alloc] init];
            NSInteger filteredIndexes = [arrTemp indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop){
                NSString *ID =[NSString stringWithFormat:@"%@",[obj valueForKey:@"isDefault"]];
                return [ID isEqualToString:@"1"];
            }];
            if (filteredIndexes != NSNotFound) {
                NSMutableDictionary *aTempDict=[arrTemp objectAtIndex:filteredIndexes];
                [arrStatus addObject:[NSMutableArray arrayWithObjects:aTempDict, nil]];
                
                [[NSUserDefaults standardUserDefaults] setValue:[aTempDict valueForKey:kUserStatus] forKey:kUserStatus];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            [arrStatus addObject:arrTemp];
                
            }
        }else if ([[inResponseDic valueForKey:kOption] isEqualToString:@"addUpdateUserStatus"]){
            NSMutableArray *arrStatusList;
            if (arrStatus) {
                
          
            arrStatusList=[[NSMutableArray alloc] initWithArray:[arrStatus objectAtIndex:1]];
            NSInteger filteredIndexes = [arrStatusList indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop){
                NSString *ID =[NSString stringWithFormat:@"%@",[obj valueForKey:@"isDefault"]];
                return [ID isEqualToString:@"1"];
            }];
            if (filteredIndexes != NSNotFound) {
                NSMutableDictionary *aTempDict=[[NSMutableDictionary alloc] initWithDictionary:[arrStatusList objectAtIndex:filteredIndexes]];
                [aTempDict setObject:@"0" forKey:@"isDefault"];
                [arrStatusList replaceObjectAtIndex:filteredIndexes withObject:aTempDict];
            }
            }
            if ([inResponseDic valueForKey:@"userStatus"]) {
                NSMutableDictionary *aTemp=[[NSMutableDictionary alloc] init];
               [aTemp setObject:[inResponseDic valueForKey:@"userStatus"]  forKey:@"userStatus"];
                [aTemp setObject:[inResponseDic valueForKey:@"userStatusId"] forKey:@"userStatusId"];
                [aTemp setObject:[[NSUserDefaults standardUserDefaults] valueForKey:kUserId] forKey:@"userId"];
                [aTemp setObject:@"1" forKey:@"isDefault"];
                
                
//                NSMutableArray *arrStatusList=[[NSMutableArray alloc] initWithArray:[arrStatus objectAtIndex:1]];
                if (!arrStatusList) {
                    arrStatusList = [[NSMutableArray alloc] init];
                    [arrStatusList addObject:aTemp];
                }
                else
                [arrStatusList insertObject:aTemp atIndex:0];
                
                if ([arrStatus count]==0) {
                    arrStatus= [[NSMutableArray alloc] init];
                    [arrStatus addObject:[NSMutableArray arrayWithObjects:aTemp,nil]];
                    [arrStatus addObject:arrStatusList];
                }else{
                    [arrStatus replaceObjectAtIndex:0 withObject:[NSMutableArray arrayWithObjects:aTemp, nil]];
                    [arrStatus replaceObjectAtIndex:1 withObject:arrStatusList];
                }
                
               
                [[NSUserDefaults standardUserDefaults] setValue:[aTemp valueForKey:@"userStatus"] forKey:kUserStatus];
                [arrStatus replaceObjectAtIndex:1 withObject:arrStatusList];
                
            }else{
                
                
                
                
                
               
                NSInteger filteredIndexesNew = [arrStatusList indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop){
                    NSString *ID =[NSString stringWithFormat:@"%@",[obj valueForKey:@"userStatusId"]];
                    return [ID isEqualToString:[inResponseDic valueForKey:@"userStatusId"]];
                }];
                if (filteredIndexesNew != NSNotFound) {
                     NSMutableDictionary *aTempDict=[[NSMutableDictionary alloc] initWithDictionary:[arrStatusList objectAtIndex:filteredIndexesNew]];
                    [aTempDict setObject:@"1" forKey:@"isDefault"];
                    [arrStatusList replaceObjectAtIndex:filteredIndexesNew withObject:aTempDict];
                    
                 [arrStatus replaceObjectAtIndex:0 withObject:[NSMutableArray arrayWithObjects:aTempDict, nil]];
                    
                    [[NSUserDefaults standardUserDefaults] setValue:[aTempDict valueForKey:@"userStatus"] forKey:kUserStatus];
                    
                }
                
                
                [arrStatus replaceObjectAtIndex:1 withObject:arrStatusList];
            }
           
           
            
            
        }else{
            //Delete Cell
            NSMutableArray *arrStatusList=[[NSMutableArray alloc] initWithArray:[arrStatus objectAtIndex:1]];
            NSInteger filteredIndexesNew = [arrStatusList indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop){
                NSString *ID =[NSString stringWithFormat:@"%@",[obj valueForKey:@"userStatusId"]];
                return [ID isEqualToString:[inResponseDic valueForKey:@"userStatusId"]];
            }];
            if (filteredIndexesNew != NSNotFound) {
                
                
                
                [arrStatusList removeObjectAtIndex:filteredIndexesNew];
                
                [arrStatus replaceObjectAtIndex:1 withObject:arrStatusList];
                
                [statusTableView beginUpdates];
                [statusTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:filteredIndexesNew inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
                [statusTableView endUpdates];
            }
            
            
            
        }
        
        
        [statusTableView reloadData];
        
    }else if ([[inResponseDic valueForKey:kStatus] integerValue] == RESPONSE_STATUS_AUTHENTICATION_FAIL){
        
        [[AppManager sharedManager] showAuthenticationFailedAlertView];
    }
     [activityView removeFromSuperview];
}

-(void)requestErrorHandler :(NSError *)inError andRequestIdentifier :(NSString *)inReqIdentifier
{
    [activityView removeFromSuperview];

    //    if (viewStatus == VIEW_STATUS_POPPED) {
    //        return;
    //    }
    
    //    [activityIndicator stopAnimating];
    //    //    [Utils stopActivityIndicatorInView:self.view];
    //    [AppManager stopStatusbarActivityIndicator];
    //    [refrestAddressBookContacts endRefreshing];
    //
    //    //    NSLog(@"*** requestErrorHandler Error : %@ and Request Udesntifier : %@ ****",[inError debugDescription],inReqIdentifier);
    //
    //    //  Log in
    //    if ([inReqIdentifier isEqualToString:kCheckContactList])
    //    {
    //        [refrestAddressBookContacts endRefreshing];
    //    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)btnBackClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnAddStatus:(id)sender {
    
    [self openAddStatusView:nil];
}
-(void)openAddStatusView:(NSString*)strStatus{
    AddStatusViewController *addStatus=[[AddStatusViewController alloc] initWithNibName:@"AddStatusViewController" bundle:nil];
    addStatus.delegate=self;
    addStatus.strStatus=strStatus;
    [self presentViewController:addStatus animated:YES completion:nil];
}
#pragma mark - Add Status Delegate
-(void)AddStatusWithStatusText:(NSString*)strStatusText andShouldUpdate:(BOOL)shouldUpdate{
    if (shouldUpdate) {
       
        
      
        [self callWebServiceAddStatus:strStatusText andStatusId:@"0"];
    }
}
#pragma mark - Status Cell Delegate
-(void)didCellScrolled:(id)cellScrolled{
    if (!isTableViewCellScrolled) {
        isTableViewCellScrolled = YES;
        
    }else{
        isTableViewCellScrolled = NO;
    }
    
    if (![commentCellScrolled isEqual:(StatusTableViewCell *)cellScrolled]) {
        [commentCellScrolled.sscrollView setContentOffset:CGPointZero animated:YES];
    }
    
    commentCellScrolled = (StatusTableViewCell *)cellScrolled;
}
-(void)btnDeleteClickedOnCellWithStatusId:(NSString*)statusId{
    [self callWebServiceDeleteStatusId:statusId];
}
-(void)didCellClicked:(NSString *)statusId andObjectAtIndexPath:(NSIndexPath*)indexPath{
    
    switch (indexPath.section) {
        case 0:
            if (indexPath.row==0) {
                [self openAddStatusView:[[[arrStatus objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] valueForKey:@"userStatus"]];
            }
            break;
            
        default:
            [self callWebServiceAddStatus:@"" andStatusId:[[[arrStatus objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] valueForKey:@"userStatusId"]];
            break;
    }
}
@end
