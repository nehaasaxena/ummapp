//
//  StatusTableViewCell.m
//  SocialParty
//
//  Created by pankaj on 09/09/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import "StatusTableViewCell.h"

@implementation StatusTableViewCell
@synthesize sscrollView;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        sscrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, self.frame.size.height)];
        [sscrollView setBackgroundColor:[UIColor clearColor]];
        [sscrollView setPagingEnabled:YES];
        sscrollView.showsHorizontalScrollIndicator=NO;
        [sscrollView setDelegate:self];
        
        viewBackground = [[UIView alloc] initWithFrame:sscrollView.frame];
        [viewBackground setBackgroundColor:[UIColor whiteColor]];
        
        
        btnCellClick= [UIButton buttonWithType:UIButtonTypeCustom];
        [btnCellClick setFrame:viewBackground.frame];
        [btnCellClick setBackgroundColor:[UIColor clearColor]];
        [btnCellClick addTarget:self action:@selector(btnCellClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        lblStatus = [[UILabel alloc] initWithFrame:CGRectMake(20, 2, 260, 40)];
        [lblStatus setBackgroundColor:[UIColor clearColor]];
        [lblStatus setTextColor:[UIColor blackColor]];
        [viewBackground addSubview:lblStatus];
        
        imgTickMark = [[UIImageView alloc] initWithFrame:CGRectMake(280, 4, 40, 40)];
        [imgTickMark setContentMode:UIViewContentModeCenter];
        [viewBackground addSubview:imgTickMark];
        [viewBackground addSubview:btnCellClick];
        
        
        
        btnDelete = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnDelete setFrame:CGRectMake(250, 0, 70, self.frame.size.height)];
        [btnDelete setBackgroundColor:[UIColor redColor]];
        [btnDelete setTitle:NSLocalizedString(@"Delete", @"Delete")  forState:UIControlStateNormal];
        [btnDelete setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnDelete.titleLabel.font=[UIFont systemFontOfSize:16];
        
        
        
        btnDeleteActual = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnDeleteActual setFrame:CGRectMake(250, 0, 70, self.frame.size.height)];
        [btnDeleteActual setBackgroundColor:[UIColor redColor]];
        [btnDeleteActual setTitle:NSLocalizedString(@"Delete", @"Delete") forState:UIControlStateNormal];
        [btnDeleteActual setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnDeleteActual.titleLabel.font=[UIFont systemFontOfSize:16];
        [btnDeleteActual addTarget:self action:@selector(btnDeleteClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btnDeleteActual setHidden:YES];
        
        [self addSubview:btnDelete];
        
        [sscrollView addSubview:viewBackground];
        
        [self addSubview:sscrollView];
         [self addSubview:btnDeleteActual];
        
        
        
        
        [sscrollView setContentSize:CGSizeMake(390, sscrollView.frame.size.height)];
    }
    return self;
}
-(void)updateCellView:(NSMutableDictionary *)aDict andIndexPath:(NSIndexPath*)indexPath{
    
    
    _statusId = [NSString stringWithFormat:@"%@",[aDict valueForKey:@"userStatusId"]];
    _indexpath=indexPath;
    
    if (_indexpath.section==0 && _indexpath.row==0) {
        [sscrollView setScrollEnabled:NO];
    }else
        [sscrollView setScrollEnabled:YES];
    
    [lblStatus setText:[aDict valueForKey:@"userStatus"]];
    
    if (indexPath.section==0&&indexPath.row==0) {
        [imgTickMark setImage:[UIImage imageNamed:@"arrowPreview"]];
    }else{
        if ([[aDict valueForKey:@"isDefault"] integerValue] == 1) {
            [imgTickMark setImage:[UIImage imageNamed:@"settingsTick"]];
        }else
            [imgTickMark setImage:nil];
    }
}
- (void)awakeFromNib
{
    // Initialization code
}
#pragma mark- UIScrollView Delegate

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
    
    if ([self.delegate respondsToSelector:@selector(didCellScrolled:)]) {
        [self.delegate didCellScrolled:self];
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
   
    
    
    NSLog(@"%f",scrollView.contentOffset.x);
    if (scrollView.contentOffset.x<0) {
        [scrollView setContentOffset:CGPointMake(0, 0)];
    }else if (scrollView.contentOffset.x>=70){
        
        [btnDeleteActual setHidden:NO];

        
    }else{

        [btnDeleteActual setHidden:YES];
    }
    //    NSLog(@"%f",scrollView.contentOffset.x);
    
}
-(void)btnDeleteClicked:(UIButton*)sender{
    NSLog(@"btn hit");
    
    if ([self.delegate respondsToSelector:@selector(btnDeleteClickedOnCellWithStatusId:)])
        [self.delegate btnDeleteClickedOnCellWithStatusId:_statusId];
    
}
-(void)btnCellClicked:(UIButton*)sender{
    if ([self.delegate respondsToSelector:@selector(didCellScrolled:)]) {
        [self.delegate didCellScrolled:self];
    }
    if ([self.delegate respondsToSelector:@selector(didCellClicked:andObjectAtIndexPath:)])
        [self.delegate didCellClicked:_statusId andObjectAtIndexPath:_indexpath];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
