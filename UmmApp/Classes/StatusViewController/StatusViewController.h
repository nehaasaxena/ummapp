//
//  StatusViewController.h
//  SocialParty
//
//  Created by pankaj on 09/09/14.
//  Copyright (c) 2014 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddStatusViewController.h"
#import "StatusTableViewCell.h"
@interface StatusViewController : UIViewController<AddStatusDelegate,StatusCellDelegate>{
    
    IBOutlet UIView *activityView;
    __weak IBOutlet UITableView *statusTableView;
    NSMutableArray *arrStatus;
    BOOL isTableViewCellScrolled;
    StatusTableViewCell *commentCellScrolled;

}
- (IBAction)btnAddStatus:(id)sender;

@end
