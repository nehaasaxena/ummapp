
//#define PrintFrame(frame) NSLog(@"F X:%f Y:%f W:%f H:%f", frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
#define PrintFrame(frame) NSLog(@"%@", NSStringFromCGRect(frame));


#define kAlertTitle             @"UmmApp"

#define kIsLoggedIn             @"isLoggedIn"
#define kprofileImage @"profileImage"

// fonts name
#define ssFontBrushScriptStd  @"BrushScriptStd"

#define kDeviceToken                   @"deviceId"
#define kBaseURL  @"http://devapi.ummapp.me/" // live for version 1.1
#define SUCCESS                @"status"
#define DATA                    @"data"
#define KPost                 @"POST"
#define KGet                  @"GET"
#define KPut                  @"PUT"
#define kOption                       @"option"
#define kMyUpload     @"myUpload"
#define kGetChatList            @"getChatList"


#define kParameter @"Parameter"
#define kDataDic @"DataDic"
#define kBodyStr @"BodyStr"
#define kbgImage   @"bgImage"
#define kFileType                         @"fileType"
#define kUploadProfilePic           @"uploadProfilePic"

// Register api
#define kSignup                         @"signup"
#define kMobile                         @"mobile"
#define kCountryCode               @"countryCode"
#define kCountryName              @"countryName"
#define kDeviceType                 @"deviceType"
#define kDeviceTypeiPhone       @"1"
#define kUserState                      @"userState"



#define kSessionToken               @"sessionToken"
#define kOption                         @"option"
#define kUserId                         @"userId"
#define kShowVerification           @"showVerification"
#define kServerUrl                     @"serverUrl"
#define kFullName                   @"fullName"
#define kProfileImage               @"profileImage"


// send sms for Verification
#define kUsers                      @"users"
#define kSendOAuthSMS      @"sendOAuthSMS"
#define kItemValue              @"itemValue"

// Phone verification
#define kCheckCode          @"checkCode"
#define kCode                   @"code"
#define kVerified               @"verified"


// settings api

#define kSettings                                @"settings"
#define kUpdateUserSettings             @"updateUserSettings"
#define kUserSettings                           @"userSettings"
#define kMESSAGE_NOTIFICATION   @"MESSAGE_NOTIFICATION"
#define kSOUNDNAME                         @"SOUNDNAME"
#define kALERT                                       @"ALERT"
#define kSHOW_PREVIEW                   @"SHOW_PREVIEW"


// Reset app settings
#define kResetUserSettings              @"resetUserSettings"



#define kAPP_SETTINGS                                                     @"appSettings"

#define kCHAT_SETTINGS                                                 @"chatSettings"
#define kCHAT_SETTINGS_CHAT_WALLPAPER            @"chatWallpaper"
#define kCHAT_SETTINGS_AUTODOWNLOAD             @"autoDownload"
#define kCHAT_SETTINGS_SAVE_INCOMING_MEDIA  @"saveIncomingMedia"

// Update Profile

#define kUserProfile        @"userProfile"
#define kUpdateProfile    @"updateProfile"

#define kDevice    @"1"
#define kUserStatus            @"userStatus"
#define kStatus          @"status"
#define kMessage                       @"message"

#pragma - mark AddressBook
#define kContactsAccessPermission @"ContactsAccessPermission"
#define kType  @"type"
#define kNotes   @"notes"
#define kJobTitle   @"jobTitle"
#define kPhoneAddress             @"phoneAddress"
#define kGetPhoneBookFriends      @"getPhoneBookFriendsNew"
#define kPhoneNumber        @"phone"
#define kemail                   @"email"


// Delete Account
#define kDeleteAccount  @"deleteAccount"



//========Profile Pic Constants============//

#define kProfilePic                     @"profilePic"
#define kprofile105                     @"profile105"
#define kprofile210                     @"profile210"
#define kprofilePicOriginalCompress     @"profilePicOriginalCompress"
#define kprofilePicBlur                 @"profilePicBlur"



#define ssFontHelveticaNeue_BoldItalic  @"HelveticaNeue-BoldItalic"
#define ssFontHelveticaNeue_Light @"HelveticaNeue-Light"
#define ssFontHelveticaNeue_Italic @"HelveticaNeue-Italic"
#define ssFontHelveticaNeue_UltraLightItalic @"HelveticaNeue-UltraLightItalic"
#define ssFontHelveticaNeue_CondensedBold @"HelveticaNeue-CondensedBold"
#define ssFontHelveticaNeue_MediumItalic @"HelveticaNeue-MediumItalic"
#define ssFontHelveticaNeue_Thin @"HelveticaNeue-Thin"
#define ssFontHelveticaNeue_Medium @"HelveticaNeue-Medium"
#define ssFontHelveticaNeue_Thin_Italic @"HelveticaNeue-Thin_Italic"
#define ssFontHelveticaNeue_LightItalic @"HelveticaNeue-LightItalic"
#define ssFontHelveticaNeue_UltraLight @"HelveticaNeue-UltraLight"
#define ssFontHelveticaNeue_Bold @"HelveticaNeue-Bold"
#define ssFontHelveticaNeue @"HelveticaNeue"
#define ssFontHelveticaNeue_CondensedBlack @"HelveticaNeue-CondensedBlack"




//=================== FourSquare =================
#define kFourSquareClientId @"MIVI4T1NI3MFY13UQ1O1BOMZ1MD1ZUPTK2XZVO5O5OE5SW2R"
#define kFourSquareSecretId @"TBAYO1CDWOTFPEAIS5HDTYLUIIOUOQXKW2PHH3DWP533XT3R"


